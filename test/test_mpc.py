from mpc.mpc import MPCController
from mpc.use_cases import get_case_data


def test_mpc():
    robots, obstacles, sim_steps, r_model = get_case_data(14)
    mpc = MPCController(r_model)
    mpc.robots = robots
    mpc.obstacles = obstacles


    for i in range(400):
        mpc.tick()
        print(mpc.robots[0]["State"])

    assert(False)
