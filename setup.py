import setuptools
from setuptools import setup
from glob import glob
import os

package_name = "mpc_trajectory_control"

setup(
    name=package_name,
    version="0.0.1",
    packages=setuptools.find_packages(),
    include_package_data=True,
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        (os.path.join("share", package_name, "configs"), glob("configs/*.yaml")),
        (os.path.join("share", package_name, "launch"), glob("launch/*.launch.py")),
        (os.path.join("share", package_name, "launch"), glob("launch/*.launch.py")),
        (os.path.join("lib", package_name, "launch"), glob("launch/*.launch.py")),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="erik",
    maintainer_email="erik.brorsson@consultant.volvo.com",
    author="Erik",
    keywords=["ROS"],
    classifiers=[
        "Intended Audience :: Developers",
        "License :: BSD",
        "Programming Language :: Python",
        "Topic :: Software Development",
    ],
    description="This package provides a simple implementation of an ARuco Marker detector.",
    license="BSD",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "mpc_trajectory_control = mpc.mpc_node:main",
        ],
    },
)
