# \file
#
#  \author Emmanuel Dean
#
#  \version 0.1
#  \date 15.03.2021
#
#  \copyright Copyright 2021 Chalmers
#
#  #### License
# All rights reserved.

# Software License Agreement (BSD License 2.0)

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# /


import os
import yaml

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

# def generate_launch_description():
#     # Get the launch directory
#     mpc_trajectory_control_dir = get_package_share_directory("mpc_trajectory_control")

#     # Launch configuration variables specific to simulation

#     # RQT
#     rqt_graph_node = Node(package="rqt_graph", executable="rqt_graph", output="screen")

#     # -------- NODES

#     # Turtle Visualization
#     params = os.path.join(get_package_share_directory("mpc_trajectory_control"), "configs", "params.yaml")
#     mpc_trajectory_control = Node(
#         package="mpc_trajectory_control",
#         name="my_mpc_trajectory_control_1",
#         executable="tag_publisher_node",
#         parameters=[params],
#         parameters=[],
#         output="screen",
#     )

#     # Create the launch description and populate
#     ld = LaunchDescription()

#     # Load nodes (actions)
#     # ld.add_action(rqt_graph_node)
#     ld.add_action(mpc_trajectory_control)


#     return ld


def generate_launch_description():
    # Get the launch directory
    mpc_trajectory_control_dir = get_package_share_directory("mpc_trajectory_control")

    # Launch configuration variables specific to simulation

    # RQT
    rqt_graph_node = Node(package="rqt_graph", executable="rqt_graph", output="screen")

    # -------- NODES
    cuda_yaml = os.path.join(
        get_package_share_directory("mpc_trajectory_control"), "configs", "launch.yaml"
    )

    with open(cuda_yaml, "r") as stream:
        try:
            config_parameters = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    ld = LaunchDescription()

    cam_ids = config_parameters["mpc_trajectory_control"]["cam_ids"]
    for i, cam_id in enumerate(cam_ids):
        mpc_trajectory_control = Node(
            package="mpc_trajectory_control",
            name=f"mpc_trajectory_control_{cam_id}",
            executable="tag_publisher_node",
            # parameters=[turtle_vis_yaml],
            parameters=[
                {
                    "cam_id": str(cam_id),
                    "pub_topic_name": f"cam_{cam_id}/ar_tags",
                    "timer_period": config_parameters["mpc_trajectory_control"]["timer_period"],
                    "port": config_parameters["mpc_trajectory_control"]["ports"][i],
                }
            ],
            output="screen",
            emulate_tty=True,
        )

        # Load nodes (actions)
        # ld.add_action(rqt_graph_node)
        ld.add_action(mpc_trajectory_control)

    return ld
