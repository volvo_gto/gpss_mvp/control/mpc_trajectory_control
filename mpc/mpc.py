"""This module includes the mpc trajectory controller that will control each ATR"""

import sys, os
from typing import Dict, Tuple, Optional, List

from mpc.RobotModelData import RobotModelData, RunConfig, Constants
from mpc.function_lib import (
    predict, 
    polygon_to_eqs, 
    extract_xy,
    get_closest_obstacles, 
    model, 
    check_collision, 
    closest_ref, 
    dist_to_goal)


class MPCController:
    """The MPC controller class that keeps track of the ATR and plans the trajectory for it"""

    robot_id = 0
    robots: Dict = {}
    obstacles: Dict = {}
    predicted_states: Dict = {}
    possible_collisions = []
    
    
    def __init__(self, r_model:RobotModelData) -> None:
        from mpc.solvers.distributed_20 import distributed_20
        self.solver = distributed_20.solver()

        self.r_model = r_model
        self.r_config = RunConfig()
        self.r_consts = Constants()

        self.w = .65
        self.N = r_model.N
        self.nu = r_model.nu
        self.nx = r_model.nx
        self.ts = r_model.ts 
        self.weights = r_model.get_weights()
        self.max_nr_of_robots = r_model.max_nr_of_robots
        self.nr_of_robots = r_model.nr_of_robots

        self.bad_exit = 0

        self.dist_to_ref = []
        self.ustar = []    


    def tick(self): 
        #run the distributed algorithm
        self.distributed_algorithm()
        
        #update the robots state
        self.update_state(self.robots[self.robot_id])

        #update so that reference starts closest to the robot
        self.update_ref(self.robots[self.robot_id])

        # #check if the robot has reached its goal
        if dist_to_goal(self.robots[self.robot_id]) < Constants.goal_tol and not self.robots[self.robot_id]['reached_goal']:
            self.robots[self.robot_id]['reached_goal'] = True
            print("REACHED GOAL")

        # #check if all robots have reached their goal
        # if self.nr_of_finised == self.nr_of_robots:
        #     self.all_finished = True
        # #update the position of the dynamic obstacle
        self.update_dynamic_obstacle(self.obstacles)

        # #save data on distance to obstacles
        # obstacle_distances = get_dist_obstacles(robots, obstacles)
        # self.closest_obstacle['Static'].append(obstacle_distances[0])
        # self.closest_obstacle['Robot'].append(obstacle_distances[1])
        # self.closest_obstacle['Dynamic'].append(obstacle_distances[2])
        # self.closest_obstacle['Hard'].append(obstacle_distances[3])

  

    def distributed_algorithm(self):
        # Updated divided distributed algorithm:
        '''
        Function runs through all robots and runs the solver to get new predicted states and inputs for the robots
        '''
        predicted_states_temp = self.predicted_states.copy()

        #Update the old control signals to last iterations signals
        x = []
        [x.extend(self.robots[self.robot_id]['Ref'][self.nx*i+3:self.nx*(i+1)]) for i in range(self.N)]
        u_p_old = x

        pmax = 1
        epsilon = .01
        if len(self.ustar)>1:
            self.shift_left_and_append_last(self.ustar)

        # Newly ".py" functions:
        # MPCsolution = mpc_solver(r_model)   # Insert input arguments of that in "__init__" and uncomment, if desirable
        # fixed_input = fix_input(r_model)   # Insert input arguments of that in "__init__" and uncomment, if desirable
        # collision = collision_check()


        for i in range(pmax):  # KB: Not sure about this loop
            K = 0
            

            # Fixed input vector for the mpc
            mpc_input = self.get_fixed_input()

            # Call the solver
            # t1 = perf_counter_ns()  # Measures time sinced the fcn was called first time

            #call solver to get control inputs, use previous guess as initial guess for better convergence - 
            # x,y,theta,K = MPCsolution.get_MPCsolution(robots,robot_id,self.ustar,mpc_input,state,u_p_old,K,predicted_states_temp)
            predicted_states_temp = self.get_MPCsolution(self.ustar,mpc_input,u_p_old,K,predicted_states_temp)

            #update predicted states for all robots
            self.predicted_states.update(predicted_states_temp)
            self.get_collision_check()

            if K < epsilon:
                break
    


    def predicted_states_from_u(self,x,y,theta,u):
        # Get the linear and angular velocities
        v = u[0::2]
        w = u[1::2]

        # Create a list of x and y states
        states = []

        #loop through inputs
        for vi,wi in zip(v,w): 
            x,y,theta = model(x,y,theta,[vi,wi],self.ts)
            states.extend([x,y,theta])

        return states
    
    def update_state(self, robot):
        #update robots state 1 timestep and save the previous values
        x,y,theta,v,w = robot['State']
        robot['Past_v'].append(v) 
        robot['Past_w'].append(w)
        x,y,theta = model(x,y,theta,robot['u'][:2],self.ts)
        robot['State'] = [x,y,theta,robot['u'][0],robot['u'][1]]

    def update_ref(self,robot):
        # Shift reference once step to the left depending on how far ahead the closest point along the reference is
        closest_index = closest_ref(robot)
        for iteration in range(closest_index):
            robot['Ref'][:self.nx*(self.N-1)] = robot['Ref'][self.nx:]

            # If there are more points in trajectory, append them to the end of the reference
            # else, we are getting closer to the end of the trajectory
            if len(robot['Remainder']) > 0:
                robot['Ref'][-self.nx:] = robot['Remainder'][:self.nx]
                del robot['Remainder'][:self.nx]

    def update_dynamic_obstacle(self,obstacles): 
        # Take one step for the ellipse with velocity in each coordinate times sampling time
        obstacles['Dynamic']['center'][0] += self.ts*obstacles['Dynamic']['vel'][0]
        obstacles['Dynamic']['center'][1] += self.ts*obstacles['Dynamic']['vel'][1]

    def get_MPCsolution(self,ustar,mpc_input,u_p_old,K,predicted_states_temp):

        state = self.robots[self.robot_id]['State']
        ref = self.robots[self.robot_id]['Ref']

        # Generates the mpc solution from the PANOC solver
        # Call solver to get control inputs, use previous guess as initial guess for better convergence - 
        if not self.robots[self.robot_id]['dyn_obj']: # If new objects (dynamic) are blocking the reference path
            if not self.robots[self.robot_id]['reached_goal']:
                xs = list(range(int(self.r_model.N/self.r_config.NumberOf_S),
                                        int(self.r_model.N+1),
                                        int(self.r_model.N/self.r_config.NumberOf_S)))
                initial_guess = ustar + xs
                if len(self.ustar) > 0:
                    solution = self.solver.run(p=mpc_input, initial_guess = self.ustar)
                else:
                    solution = self.solver.run(p=mpc_input)
                #add optimization variable "s" to the self.robots dict
                self.robots[self.robot_id]['s'] = solution.solution[self.r_model.N*self.r_model.nu:]

                # Get the solver output, only use control signals and not S 
                ustar = solution.solution[:self.r_model.N*self.r_model.nu]
                #save how often a bad solver exit happens
                if solution.exit_status != 'Converged':
                    print(solution.exit_status)
                    self.bad_exit += 1
            
            #modify the output to not be to far from the previous
            u_p = [self.w*ustar[j] + (1-self.w)*u_p_old[j] for j in range(self.N*self.nu)]   # The output is the new input values, Horizon length 2m of future inputs

            K = max(K, max([abs(u_p[j] - u_p_old[j]) for j in range(self.N*self.nu)]))    

            #to be used in next it
            u_p_old = u_p

            # Predict future state
            x,y,theta = state[0], state[1],state[2] # States from the specific case that was chosen in the simulation, from use_cases
        
        else:   # If no obstacles are blocking the current path -> continue previous reference path 
            ustar = []
            [ustar.extend(self.robots[self.robot_id]['Ref'][self.nx*i+3:self.nx*(i+1)]) for i in range(self.N)]    # 2m input ahead 
            x,y,theta = self.robots[self.robot_id]['Ref'][0:3]
        
        # return x,y,theta,K    # OBS!! If these are the desired output values for this node, use code here and above

        #get predicted states from new inputs and put them in the predicted states dict
        states = self.predicted_states_from_u(x,y,theta,ustar)
        predicted_states_temp[self.robot_id] = states

        #if robot not reached goal, update input with solver output
        #else, move it one step left and replace last with zeroes
        if not self.robots[self.robot_id]['reached_goal']:
            self.robots[self.robot_id]['u'] = ustar
        else:
            self.robots[self.robot_id]['u'] = self.robots[self.robot_id]['u'][:self.N*self.nu]
            self.robots[self.robot_id]['u'][:self.nu*(self.N-1)] = self.robots[self.robot_id]['u'][self.nu:]
            self.robots[self.robot_id]['u'][-self.nu:] = [0,0]

        return predicted_states_temp



    def get_fixed_input(self):
        # fixed input vector from the fixed path generated by trajectory planner
        # for the mpc_solver

        if not self.robots[self.robot_id]['dyn_obj']: # If only dynamic obstacles/objects
            if not self.robots[self.robot_id]['reached_goal']:
                state = self.robots[self.robot_id]['State']
                ref = self.robots[self.robot_id]['Ref'] 

                p = []
                p.extend(state)
                p.extend(ref)
                p.extend(self.weights)

                # Get predicted states for the other robot  
                for i in self.predicted_states:
                    if i != self.robot_id:
                        p.extend(extract_xy(self.predicted_states[i]))
                #Fill in with  zero trejectories for missing robots
                p.extend([0]*2*self.N*(self.max_nr_of_robots - self.nr_of_robots))

                p.extend([1]*(self.nr_of_robots-1))
                p.extend([0]*(self.max_nr_of_robots - self.nr_of_robots))
                #find closest five obstacles to be used as inputs in the solver
                closest_padded, closest_extrapadded = get_closest_obstacles(state[:2], self.obstacles)
                for ob in closest_padded:
                    p.extend(polygon_to_eqs(ob))

                for ob in closest_extrapadded:
                    p.extend(polygon_to_eqs(ob))

                # Append equation for boundaries
                p.extend(polygon_to_eqs(self.obstacles['Boundaries']))

                # Append parameters for the ellipse: a,b,phi,predicted centers
                p.append(self.obstacles['Dynamic']['a']+self.obstacles['Dynamic']['apad'])
                p.append(self.obstacles['Dynamic']['b']+self.obstacles['Dynamic']['bpad'])
                p.append(self.obstacles['Dynamic']['phi'])
                p.extend(predict(self.obstacles['Dynamic']['center'][0],
                                self.obstacles['Dynamic']['center'][1],
                                self.obstacles['Dynamic']['vel'][0],
                                self.obstacles['Dynamic']['vel'][1],
                                self.N,
                                self.ts))

                return p


    def get_collision_check(self):
        # Run check_collision if active and save how often robots gets stopped
        robots, multiplier = check_collision(self.robots, self.obstacles, self.predicted_states)
        collisions = 0
        for individual_multiplier in multiplier:
            if individual_multiplier == 0:
                print('stopped')
                collisions += 1
        self.possible_collisions.append(collisions)

    def shift_left_and_append_last(self,dikt):
        # Shift reference once step to the left
        dikt.extend(dikt[-self.nu:]) 
        dikt = dikt[self.nu:]
