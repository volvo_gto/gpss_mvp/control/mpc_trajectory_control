import casadi.casadi as cs
from mpc.RobotModelData import RobotModelData, Constants
from mpc.function_lib import (generate_straight_trajectory,generate_turn_left_trajectory,
                          generate_turn_right_trajectory,padded_square,unpadded_square)
from shapely.geometry import Polygon
'''
File that contains all of the different use cases. The static obstacles and boundary has to be defined as 4 sided 
convex polygons and the dynamic obstacle as a ellipse
'''

def get_case_data(case_nr):
    '''
    Gives case data
    Arguments
        case_nr: integer
    Returns
        Robots: dictionary containing all robots and their respective information
        Obstacles: dictionary containing unpadded,padded,extrapadded static obstacles, dynamic obstacle and boundaries
        N_steps: Integer for how many steps the simulation should run
        r_model: dataclass containing weights for nmpc
    '''
    nx = RobotModelData.nx
    N = RobotModelData.N
    pad = Constants.padding
    extra_pad = Constants.padding+Constants.extra_padding
    if case_nr == 1:
        N_steps = 90

        traj1 = generate_straight_trajectory(x=-5, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right
        traj2 = generate_straight_trajectory(x=0, y=-4.95, theta=cs.pi / 2, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 0] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 0] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'b', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}
        N_steps = 150
        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(-1.25, -1.25, 1, 1), unpadded_square(1.25, -1.25, 1, 1),
                                 unpadded_square(1.25, 1.25, 1, 1), unpadded_square(-1.25, 1.25, 1, 1)]
        obstacles['Padded'] = [padded_square(-1.25, -1.25, 1, 1, pad), padded_square(1.25, -1.25, 1, 1, pad),
                               padded_square(1.25, 1.25, 1, 1, pad), padded_square(-1.25, 1.25, 1, 1, pad)]
        obstacles['Extra_pad'] = [padded_square(-1.25, -1.25, 1, 1, extra_pad),
                                padded_square(1.25, -1.25, 1, 1, extra_pad),padded_square(1.25, 1.25, 1, 1, extra_pad),
                                padded_square(-1.25, 1.25, 1, 1, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-6, -6], [6, -6], [6, 6], [-6, 6]])
        obstacles['Dynamic'] = {'center': [-3, -3], 'a': 0.5, 'b': 0.25, 'vel': [1, 1], 'apad': 0.5, 'bpad': 0.5,
                                'phi': cs.pi / 4, 'active': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 2:
        N_steps = 70
        traj1 = generate_straight_trajectory(x=-2.5, y=4, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj2 = generate_straight_trajectory(x=0, y=4, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj3 = generate_straight_trajectory(x=3, y=4, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj4 = generate_straight_trajectory(x=4, y=1, theta=cs.pi, v=1, ts=0.1, N=N_steps)
        traj5 = generate_straight_trajectory(x=4, y=-1, theta=cs.pi, v=1, ts=0.1, N=N_steps)
        traj6 = generate_straight_trajectory(x=1, y=-4, theta=cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj7 = generate_straight_trajectory(x=-1, y=-4, theta=cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj8 = generate_straight_trajectory(x=-4, y=3, theta=0, v=1, ts=0.1, N=N_steps)
        traj9 = generate_straight_trajectory(x=-4, y=0, theta=0, v=1, ts=0.1, N=N_steps)
        traj10 = generate_straight_trajectory(x=-4, y=-3, theta=0, v=1, ts=0.1, N=N_steps-15)
        N_steps = 100
        obstacles = {}
        obstacles['Unpadded'] = []
        obstacles['Padded'] = []
        obstacles['Extra_pad'] = []
        obstacles['Boundaries'] = Polygon([[-4.5, -4.5], [4.5, -4.5], [4.5, 4.5], [-4.5, 4.5]])
        obstacles['Dynamic'] = {'center': [-3, -3], 'a': 0.5, 'b': 0.25, 'vel': [1, 1], 'apad': 0.5, 'bpad': 0.5,
                                'phi': cs.pi / 4, 'active': True}

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'g', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}
        robots[2] = {"State": traj3[:nx], 'Ref': traj3[nx:N * nx + nx], 'Remainder': traj3[N * nx + nx:],
                     'Temporary': traj3[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'b', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj3[-5:],'reached_goal': False}
        robots[3] = {"State": traj4[:nx], 'Ref': traj4[nx:N * nx + nx], 'Remainder': traj4[N * nx + nx:],
                     'Temporary': traj4[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'y', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj4[-5:],'reached_goal': False}
        robots[4] = {"State": traj5[:nx], 'Ref': traj5[nx:N * nx + nx], 'Remainder': traj5[N * nx + nx:],
                     'Temporary': traj5[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'm', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj5[-5:],'reached_goal': False}
        robots[5] = {"State": traj6[:nx], 'Ref': traj6[nx:N * nx + nx], 'Remainder': traj6[N * nx + nx:],
                     'Temporary': traj6[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'c', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj6[-5:],'reached_goal': False}
        robots[6] = {"State": traj7[:nx], 'Ref': traj7[nx:N * nx + nx], 'Remainder': traj7[N * nx + nx:],
                     'Temporary': traj7[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:orange', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj7[-5:],'reached_goal': False}
        robots[7] = {"State": traj8[:nx], 'Ref': traj8[nx:N * nx + nx], 'Remainder': traj8[N * nx + nx:],
                     'Temporary': traj8[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:brown', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj8[-5:],'reached_goal': False}
        robots[8] = {"State": traj9[:nx], 'Ref': traj9[nx:N * nx + nx], 'Remainder': traj9[N * nx + nx:],
                     'Temporary': traj9[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:purple', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj9[-5:],'reached_goal': False}
        robots[9] = {"State": traj10[:nx], 'Ref': traj10[nx:N * nx + nx], 'Remainder': traj10[N * nx + nx:],
                     'Temporary': traj10[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:pink', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj10[-5:],'reached_goal': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 3:
        N_steps = 75

        traj1 = generate_straight_trajectory(x=-4, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps-10)  # Trajectory from x=-1, y=0 driving straight to the right
        traj2 = generate_straight_trajectory(x=-2.5, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up
        traj3 = generate_straight_trajectory(x=-1.0, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up
        traj4 = generate_straight_trajectory(x=0.5, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up
        traj5 = generate_turn_right_trajectory(x=0, y=-3.5, theta=cs.pi / 2, v=1, ts=0.1, N1=35,
                                               N2=N_steps-35)  # Trajectory from x=0,y=-1 driving straight up
        N_steps = 120
        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'g', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}
        robots[2] = {"State": traj3[:nx], 'Ref': traj3[nx:N * nx + nx], 'Remainder': traj3[N * nx + nx:],
                     'Temporary': traj3[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'b', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj3[-5:],'reached_goal': False}
        robots[3] = {"State": traj4[:nx], 'Ref': traj4[nx:N * nx + nx], 'Remainder': traj4[N * nx + nx:],
                     'Temporary': traj4[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'y', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj4[-5:],'reached_goal': False}
        robots[4] = {"State": traj5[:nx], 'Ref': traj5[nx:N * nx + nx], 'Remainder': traj5[N * nx + nx:],
                     'Temporary': traj5[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'm', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj5[-5:],'reached_goal': False}

        obstacles = {}

        obstacles['Unpadded'] = []
        obstacles['Padded'] = []
        obstacles['Extra_pad'] = []
        obstacles['Boundaries'] = Polygon([[-9, -9], [9, -9], [9, 9], [-9, 9]])
        obstacles['Dynamic'] = {'center': [-3, -3], 'a': 0.5, 'b': 0.25, 'vel': [1, 1], 'apad': 0.5, 'bpad': 0.5,
                                'phi': cs.pi / 4, 'active': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 4:
        N_steps = 70

        traj1 = generate_straight_trajectory(x=-4, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right
        traj2 = generate_straight_trajectory(x=4, y=0, theta=-cs.pi, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up
        traj3 = generate_straight_trajectory(x=1, y=-2, theta=cs.pi / 2, v=1, ts=0.1,
                                             N=N_steps-20)  # Trajectory from x=0,y=-1 driving straight up
        traj4 = generate_straight_trajectory(x=-1, y=-2, theta=cs.pi / 2, v=1, ts=0.1,
                                             N=N_steps-20)  # Trajectory from x=0,y=-1 driving straight up
        traj5 = generate_straight_trajectory(x=-4, y=2, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right
        N_steps = 120
        obstacles = {}
        obstacles['Unpadded'] = []
        obstacles['Padded'] = []
        obstacles['Extra_pad'] = []
        obstacles['Boundaries'] = Polygon([[-4.5, -4.5], [4.5, -4.5], [4.5, 4.5], [-4.5, 4.5]])
        obstacles['Dynamic'] = {'center': [-3, -3], 'a': 0.5, 'b': 0.25, 'vel': [1, 1], 'apad': 0.5, 'bpad': 0.5,
                                'phi': cs.pi / 4, 'active': True}

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'g', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}
        robots[2] = {"State": traj3[:nx], 'Ref': traj3[nx:N * nx + nx], 'Remainder': traj3[N * nx + nx:],
                     'Temporary': traj3[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'b', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj3[-5:],'reached_goal': False}
        robots[3] = {"State": traj4[:nx], 'Ref': traj4[nx:N * nx + nx], 'Remainder': traj4[N * nx + nx:],
                     'Temporary': traj4[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'y', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj4[-5:],'reached_goal': False}
        robots[4] = {"State": traj5[:nx], 'Ref': traj5[nx:N * nx + nx], 'Remainder': traj5[N * nx + nx:],
                     'Temporary': traj5[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'm', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj5[-5:],'reached_goal': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 5:
        N_steps = 120

        traj1 = generate_straight_trajectory(x=-3, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right
        traj2 = generate_straight_trajectory(x=3, y=0, theta=cs.pi, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}

        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(0, 2, 8, 2.5), unpadded_square(0, -2, 8, 2.5)]
        obstacles['Padded'] = [padded_square(0, 2, 8, 2.5, pad), padded_square(0, -2, 8, 2.5, pad)]
        obstacles['Extra_pad'] = [padded_square(0, 2, 8, 2.5, extra_pad), padded_square(0, -2, 8, 2.5, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-8, -8], [8, -8], [8, 8], [-8, 8]])
        obstacles['Dynamic'] = {'center': [-3, -3], 'a': 0.5, 'b': 0.25, 'vel': [1, 1], 'apad': 0.5, 'bpad': 0.5,
                                'phi': cs.pi / 4, 'active': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 6:
        N_steps = 110

        traj1 = generate_straight_trajectory(x=-2.5, y=4, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj2 = generate_straight_trajectory(x=0, y=4.2, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj3 = generate_straight_trajectory(x=3, y=4, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj4 = generate_straight_trajectory(x=4, y=1.35, theta=cs.pi, v=1, ts=0.1, N=N_steps)
        traj5 = generate_straight_trajectory(x=4, y=-1.35, theta=cs.pi, v=1, ts=0.1, N=N_steps)
        traj6 = generate_straight_trajectory(x=1.35, y=-4, theta=cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj7 = generate_straight_trajectory(x=-1.35, y=-4, theta=cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj8 = generate_straight_trajectory(x=-4, y=3, theta=0, v=1, ts=0.1, N=N_steps)
        traj9 = generate_straight_trajectory(x=-4, y=0, theta=0, v=1, ts=0.1, N=N_steps)
        traj10 = generate_straight_trajectory(x=-4, y=-3, theta=0, v=1, ts=0.1, N=N_steps)
        N_steps = 200
        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'g', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}
        robots[2] = {"State": traj3[:nx], 'Ref': traj3[nx:N * nx + nx], 'Remainder': traj3[N * nx + nx:],
                     'Temporary': traj3[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'b', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj3[-5:],'reached_goal': False}
        robots[3] = {"State": traj4[:nx], 'Ref': traj4[nx:N * nx + nx], 'Remainder': traj4[N * nx + nx:],
                     'Temporary': traj4[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'y', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj4[-5:],'reached_goal': False}
        robots[4] = {"State": traj5[:nx], 'Ref': traj5[nx:N * nx + nx], 'Remainder': traj5[N * nx + nx:],
                     'Temporary': traj5[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'm', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj5[-5:],'reached_goal': False}
        robots[5] = {"State": traj6[:nx], 'Ref': traj6[nx:N * nx + nx], 'Remainder': traj6[N * nx + nx:],
                     'Temporary': traj6[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],  'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'c', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj6[-5:],'reached_goal': False}
        robots[6] = {"State": traj7[:nx], 'Ref': traj7[nx:N * nx + nx], 'Remainder': traj7[N * nx + nx:],
                     'Temporary': traj7[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:orange', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj7[-5:],'reached_goal': False}
        robots[7] = {"State": traj8[:nx], 'Ref': traj8[nx:N * nx + nx], 'Remainder': traj8[N * nx + nx:],
                     'Temporary': traj8[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:brown', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj8[-5:],'reached_goal': False}
        robots[8] = {"State": traj9[:nx], 'Ref': traj9[nx:N * nx + nx], 'Remainder': traj9[N * nx + nx:],
                     'Temporary': traj9[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:purple', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj9[-5:],'reached_goal': False}
        robots[9] = {"State": traj10[:nx], 'Ref': traj10[nx:N * nx + nx], 'Remainder': traj10[N * nx + nx:],
                     'Temporary': traj10[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:pink', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj10[-5:],'reached_goal': False}

        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(-1.25, -1.25, 1, 1), unpadded_square(1.25, -1.25, 1, 1),
                                 unpadded_square(1.25, 1.25, 1, 1), unpadded_square(-1.25, 1.25, 1, 1)]
        obstacles['Padded'] = [padded_square(-1.25, -1.25, 1, 1, pad), padded_square(1.25, -1.25, 1, 1, pad),
                               padded_square(1.25, 1.25, 1, 1, pad), padded_square(-1.25, 1.25, 1, 1, pad)]
        obstacles['Extra_pad'] = [padded_square(-1.25, -1.25, 1, 1, extra_pad), padded_square(1.25, -1.25, 1, 1, extra_pad),
                               padded_square(1.25, 1.25, 1, 1, extra_pad), padded_square(-1.25, 1.25, 1, 1, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-10, -10], [10, -10], [10, 10], [-10, 10]])
        obstacles['Dynamic'] = {'center': [-3, -3], 'a': 0.5, 'b': 0.25, 'vel': [1, 1], 'apad': 0.5, 'bpad': 0.5,
                                'phi': cs.pi / 4, 'active': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 7:
        N_steps = 90

        traj1 = generate_straight_trajectory(x=-2.5, y=4, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj2 = generate_straight_trajectory(x=0, y=4, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj3 = generate_straight_trajectory(x=3, y=4, theta=3 * cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj4 = generate_straight_trajectory(x=4, y=1.35, theta=cs.pi, v=1, ts=0.1, N=N_steps)
        traj5 = generate_straight_trajectory(x=4, y=-1.35, theta=cs.pi, v=1, ts=0.1, N=N_steps)
        traj6 = generate_straight_trajectory(x=1.35, y=-4, theta=cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj7 = generate_straight_trajectory(x=-1.35, y=-4, theta=cs.pi / 2, v=1, ts=0.1, N=N_steps)
        traj8 = generate_straight_trajectory(x=-4, y=3, theta=0, v=1, ts=0.1, N=N_steps)
        traj9 = generate_straight_trajectory(x=-4, y=0, theta=0, v=1, ts=0.1, N=N_steps)
        traj10 = generate_straight_trajectory(x=-4, y=-3, theta=0, v=1, ts=0.1, N=N_steps)
        N_steps = 250
        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'g', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}
        robots[2] = {"State": traj3[:nx], 'Ref': traj3[nx:N * nx + nx], 'Remainder': traj3[N * nx + nx:],
                     'Temporary': traj3[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'b', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj3[-5:],'reached_goal': False}
        robots[3] = {"State": traj4[:nx], 'Ref': traj4[nx:N * nx + nx], 'Remainder': traj4[N * nx + nx:],
                     'Temporary': traj4[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [],'s': [],
                     'Color': 'y', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj4[-5:],'reached_goal': False}
        robots[4] = {"State": traj5[:nx], 'Ref': traj5[nx:N * nx + nx], 'Remainder': traj5[N * nx + nx:],
                     'Temporary': traj5[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'm', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj5[-5:],'reached_goal': False}
        robots[5] = {"State": traj6[:nx], 'Ref': traj6[nx:N * nx + nx], 'Remainder': traj6[N * nx + nx:],
                     'Temporary': traj6[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'c', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj6[-5:],'reached_goal': False}
        robots[6] = {"State": traj7[:nx], 'Ref': traj7[nx:N * nx + nx], 'Remainder': traj7[N * nx + nx:],
                     'Temporary': traj7[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:orange', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj7[-5:],'reached_goal': False}
        robots[7] = {"State": traj8[:nx], 'Ref': traj8[nx:N * nx + nx], 'Remainder': traj8[N * nx + nx:],
                     'Temporary': traj8[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],   'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:brown', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj8[-5:],'reached_goal': False}
        robots[8] = {"State": traj9[:nx], 'Ref': traj9[nx:N * nx + nx], 'Remainder': traj9[N * nx + nx:],
                     'Temporary': traj9[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:purple', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj9[-5:],'reached_goal': False}
        robots[9] = {"State": traj10[:nx], 'Ref': traj10[nx:N * nx + nx], 'Remainder': traj10[N * nx + nx:],
                     'Temporary': traj10[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [],'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [],
                     'Color': 'tab:pink', 'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj10[-5:],'reached_goal': False}

        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(-1.3, -1.3, 1, 1), unpadded_square(1.3, -1.3, 1, 1),
                                 unpadded_square(-1.3, 1.3, 1, 1), unpadded_square(1.3, 1.3, 1, 1),
                                 unpadded_square(0, 0, 1, 1)]
        obstacles['Padded'] = [padded_square(-1.3, -1.3, 1, 1, pad), padded_square(1.3, -1.3, 1, 1, pad),
                               padded_square(-1.3, 1.3, 1, 1, pad), padded_square(1.3, 1.3, 1, 1, pad),
                               padded_square(0, 0, 1, 1, pad)]
        obstacles['Extra_pad'] = [padded_square(-1.3, -1.3, 1, 1, extra_pad), padded_square(1.3, -1.3, 1, 1, extra_pad),
                               padded_square(-1.3, 1.3, 1, 1, extra_pad), padded_square(1.3, 1.3, 1, 1, extra_pad),
                               padded_square(0, 0, 1, 1, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-6, -6], [6, -6], [6, 6], [-6, 6]])
        obstacles['Dynamic'] = {'center': [-3, -3], 'a': 0.5, 'b': 0.25, 'vel': [1, 1], 'apad': 0.5, 'bpad': 0.5,
                                'phi': cs.pi / 4, 'active': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 8:
        N_steps = 110

        traj1 = generate_straight_trajectory(x=-6, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right
        traj2 = generate_straight_trajectory(x=5, y=0, theta=cs.pi, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 0] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [], 'Color': 'r',
                     'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [], 'Color': 'b',
                     'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}
        N_steps = 170
        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(0, 0.1, 4, 3), unpadded_square(0, -1, 2, 3),
                                 unpadded_square(1.5, 1.5, 2, 1)]
        obstacles['Padded'] = [padded_square(0, 0.1, 4, 3, pad), padded_square(0, -1, 2, 3, pad),
                               padded_square(1.5, 1.5, 2, 1, pad)]
        obstacles['Extra_pad'] = [padded_square(0, 0.1, 4, 3, extra_pad), padded_square(0, -1, 2, 3, extra_pad),
                               padded_square(1.5, 1.5, 2, 1, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-8, -8], [8, -8], [8, 8], [-8, 8]])
        obstacles['Dynamic'] = {'center': [1, 0], 'a': 0.5, 'b': 0.5, 'vel': [0, 0], 'apad': 0.5, 'bpad': 0.5,
                                'phi': -cs.pi / 4, 'active': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 9:
        N_steps = 110

        traj1 = generate_straight_trajectory(x=-6, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right
        traj2 = generate_straight_trajectory(x=5, y=0, theta=cs.pi, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 0] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [], 'Color': 'r',
                     'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [], 'Color': 'b',
                     'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}
        N_steps = 180
        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(0, 0.1, 4, 3), unpadded_square(-2, -1.1, 2, 2),
                                 unpadded_square(1.5, 1.5, 2, 2)]
        obstacles['Padded'] = [padded_square(0, 0.1, 4, 3, pad), padded_square(-2, -1.1, 2, 2, pad),
                               padded_square(1.5, 1.5, 2, 2, pad)]
        obstacles['Extra_pad'] = [padded_square(0, 0.1, 4, 3, extra_pad), padded_square(-2, -1.1, 2, 2, extra_pad),
                               padded_square(1.5, 1.5, 2, 2, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-8, -8], [8, -8], [8, 8], [-8, 8]])
        obstacles['Dynamic'] = {'center': [1, 0], 'a': 0.5, 'b': 0.5, 'vel': [0, 0], 'apad': 0.5, 'bpad': 0.5,
                                'phi': -cs.pi / 4, 'active': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 10:
        N_steps = 70

        traj1 = generate_straight_trajectory(x=-3, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right
        traj2 = generate_straight_trajectory(x=3, y=0, theta=-cs.pi, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=0,y=-1 driving straight up
        N_steps = 90
        obstacles = {}
        obstacles['Unpadded'] = []
        obstacles['Padded'] = []
        obstacles['Extra_pad'] = []
        obstacles['Boundaries'] = Polygon([[-8, -8], [8, -8], [8, 8], [-8, 8]])
        obstacles['Dynamic'] = {'center': [0, 0], 'a': 1, 'b': 0.5, 'vel': [0.2, 0.2], 'apad': 0.5, 'bpad': 0.5,
                                'phi': -cs.pi / 4, 'active': False}

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [], 'Color': 'r',
                     'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}
        robots[1] = {"State": traj2[:nx], 'Ref': traj2[nx:N * nx + nx], 'Remainder': traj2[N * nx + nx:],
                     'Temporary': traj2[nx:N * nx + nx], 'Use Temporary': False, "displaced_ref": [],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 's': [], 'Color': 'b',
                     'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj2[-5:],'reached_goal': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 11:
        N_steps = 90

        traj1 = generate_straight_trajectory(x=-3, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right

        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(0, .1, 0.4, 0.5)]
        obstacles['Padded'] = [padded_square(0, .1, 0.4, 0.5, pad)]
        obstacles['Extra_pad'] = [padded_square(0, .1, 0.4, 0.5, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-8, -8], [8, -8], [8, 8], [-8, 8]])
        obstacles['Dynamic'] = {'center': [1, 0], 'a': 0.5, 'b': 0.5, 'vel': [0, 0], 'apad': 0.5, 'bpad': 0.5,
                                'phi': -cs.pi / 4, 'active': False}
        N_steps = 200
        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, 'u': [1, 1] * N, 'Past_x': [],
                     'Past_y': [], 'Past_v': [], 'Past_w': [], 'Color': 'r', 'dyn_obj': False, 'New_Ref_start': [],
                     'New_Ref_goal':[],'x_goal':traj1[-5:],'reached_goal': False}

        nr_of_robots = len(robots)

        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 12:
        N_steps = 275
        part1 = generate_straight_trajectory(-4, -4, 0, 1, 0.1, 65)
        part2 = generate_straight_trajectory(2.5, -4, cs.pi / 2, 1, 0.1, 30)
        part3 = generate_straight_trajectory(2.5, -1, cs.pi, 1, 0.1, 45)
        part4 = generate_straight_trajectory(-2, -1, cs.pi / 2, 1, 0.1, 50)
        part5 = generate_straight_trajectory(-2, 4, 0, 1, 0.1, 55)
        part4.extend(part5)
        part3.extend(part4)
        part2.extend(part3)
        part1.extend(part2)
        traj1 = part1

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'u': [1, 0] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 'Color': 'r',
                     'dyn_obj': False, 'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False,
                     'New_Ref_start': [], 'New_Ref_goal':[],'x_goal':traj1[-5:],'reached_goal': False}

        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(-1, -2.5, 5.5, 1.5), unpadded_square(-3.25, 2, 1, 7.5),
                                 unpadded_square(0.5, 1.5, 3, 3.5), unpadded_square(0, 5.25, 5.5, 1)]
        obstacles['Padded'] = [padded_square(-1, -2.5, 5.5, 1.5, pad), padded_square(-3.25, 2, 1, 7.5, pad),
                               padded_square(0.5, 1.5, 3, 3.5, pad), padded_square(0, 5.25, 5.5, 1, pad)]
        obstacles['Extra_pad'] = [padded_square(-1, -2.5, 5.5, 1.5, extra_pad), padded_square(-3.25, 2, 1, 7.5, extra_pad),
                               padded_square(0.5, 1.5, 3, 3.5, extra_pad), padded_square(0, 5.25, 5.5, 1, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-8, -8], [8, -8], [8, 8], [-8, 8]])
        obstacles['Dynamic'] = {'center': [-2, -4], 'a': 0.5, 'b': 0.25, 'vel': [0, -0.1], 'apad': 0.5, 'bpad': 0.5,
                                'phi': cs.pi / 4, 'active': False}

        nr_of_robots = len(robots)
        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 13:
        N_steps = 70

        traj1 = generate_straight_trajectory(x=-3, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right
        obstacles = {}
        obstacles['Unpadded'] = []
        obstacles['Padded'] = []
        obstacles['Extra_pad'] = []
        obstacles['Boundaries'] = Polygon([[-8, -8], [8, -8], [8, 8], [-8, 8]])
        obstacles['Dynamic'] = {'center': [1, 0], 'a': 0.5, 'b': 0.5, 'vel': [0, 0], 'apad': 0.5, 'bpad': 0.5,
                                'phi': -cs.pi / 4, 'active': False}

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 'Color': 'r',
                     'dyn_obj': False, 'Temporary': traj1[nx:N*nx+nx],'Use Temporary': False, 'New_Ref_start': [],
                     'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}

        nr_of_robots = len(robots)

        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)


    if case_nr == 14:
        N_steps = 320
        traj1 = generate_turn_right_trajectory(-4,-4,cs.pi/2,1,0.1,80,80)+\
                generate_turn_right_trajectory(4,4,3*cs.pi/2,1,0.1,80,40)
        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(0,0,2,2)]
        obstacles['Padded'] = [padded_square(0,0,2,2,pad)]
        obstacles['Extra_pad'] = [padded_square(0,0,2,2,extra_pad)]
        obstacles['Boundaries'] = Polygon([[-6, -6], [6, -6], [6, 6], [-6, 6]])
        obstacles['Dynamic'] = {'center': [1, 0], 'a': 0.5, 'b': 0.5, 'vel': [0, 0], 'apad': 0.5, 'bpad': 0.5,
                                'phi': -cs.pi / 4, 'active': False}

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 'Color': 'r',
                     'dyn_obj': False,'Use Temporary': False, 'New_Ref_start': [],'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}

        nr_of_robots = len(robots)

        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 15:
        N_steps = 400

        traj1 = generate_straight_trajectory(5,5.5,cs.pi/2,1,0.1,95)+\
                generate_straight_trajectory(5,15,0.6055,1,0.1,80)+\
                generate_straight_trajectory(11.5,19.5,3*cs.pi/4,1,0.1,8)+\
                generate_straight_trajectory(11,20,cs.pi,1,0.1,80)+\
                generate_straight_trajectory(3,20,cs.pi/2,1,0.1,40)
        obstacle_list = [[(5.5, 13), (5.5, 14.7), (6, 15.1), (6, 13.5)],
                         [(4, 17), (6.5, 17), (4.3, 15.5), (4, 15.5)],
                         [(9, 19.5), (10.4, 19.5), (9.3, 18.5), (9, 18.5)],
                         [(11, 21), (14, 21), (14, 20), (13, 20)],
                         [(14, 19), (14, 20), (13, 20), (11, 18)],
                         [(3.5, 20.5), (3.5, 21), (4, 21), (4, 20.5)],
                         [(2, 21), (2.3, 21), (2.3, 20), (2, 20)],
                         [(4, 9), (4, 10), (6, 10), (6, 9)]]
        obstacles = {}
        obstacles['Unpadded'] = []
        obstacles['Padded'] = []
        obstacles['Extra_pad'] = []
        obstacles['Boundaries'] = Polygon([[0, 0], [15, 0], [15, 30], [0, 30]])
        obstacles['Dynamic'] = {'center': [1, 0], 'a': 0.5, 'b': 0.5, 'vel': [0, 0], 'apad': 0.5, 'bpad': 0.5,
                                'phi': -cs.pi / 4, 'active': False}
        for ob in obstacle_list:
            obstacles['Unpadded'].append(Polygon(ob))

        for ob in obstacles['Unpadded']:
            obstacles['Padded'].append(ob.buffer(0.2, join_style=2))
            obstacles['Extra_pad'].append(ob.buffer(0.43, join_style=2))


        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx],
                     'u': [1, 1] * N, 'Past_x': [], 'Past_y': [], 'Past_v': [], 'Past_w': [], 'Color': 'r',
                     'dyn_obj': False,'Use Temporary': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}

        nr_of_robots = len(robots)

        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    if case_nr == 16:
        N_steps = 200

        traj1 = generate_straight_trajectory(x=-8, y=0, theta=0, v=1, ts=0.1,
                                             N=N_steps)  # Trajectory from x=-1, y=0 driving straight to the right

        obstacles = {}
        obstacles['Unpadded'] = [unpadded_square(0, .1, 0.4, 0.5), unpadded_square(4, .1, 0.4, 0.5)]
        obstacles['Padded'] = [padded_square(0, .1, 0.4, 0.5, pad), padded_square(4, .1, 0.4, 0.5, pad)]
        obstacles['Extra_pad'] = [padded_square(0, .1, 0.4, 0.5, extra_pad), padded_square(4, .1, 0.4, 0.5, extra_pad)]
        obstacles['Boundaries'] = Polygon([[-8, -8], [8, -8], [8, 8], [-8, 8]])
        obstacles['Dynamic'] = {'center': [1, 0], 'a': 0.5, 'b': 0.5, 'vel': [0, 0], 'apad': 0.5, 'bpad': 0.5,
                                'phi': -cs.pi / 4, 'active': False}

        robots = {}
        robots[0] = {"State": traj1[:nx], 'Ref': traj1[nx:N * nx + nx], 'Remainder': traj1[N * nx + nx:],
                     'Temporary': traj1[nx:N * nx + nx], 'Use Temporary': False, 'u': [1, 1] * N, 'Past_x': [],
                     'Past_y': [], 'Past_v': [], 'Past_w': [], 'Color': 'r',
                     'dyn_obj': False, 'New_Ref_start': [], 'New_Ref_goal':[],
                     'x_goal':traj1[-5:],'reached_goal': False}

        nr_of_robots = len(robots)

        if obstacles['Dynamic']['active'] == False:
            r_model = RobotModelData(nr_of_robots=nr_of_robots, qdyn=0)
        else:
            r_model = RobotModelData(nr_of_robots=nr_of_robots)

    return robots,obstacles,N_steps,r_model
