import numpy as np
import time
from mpc.obstacles import Frame

class SOADSRolloutStatus:
    def __init__(self, exit_flag, dist_to_goal, dist_travelled, compute_time):
        self.exit_flag = exit_flag
        self.dist_to_goal = dist_to_goal
        self.dist_travelled = dist_travelled
        self.compute_time = compute_time

    def __str__(self):
        exit_status = ["Converged", "Maximum distance exceeded", "Timeout", "Maximum number of steps", "Obstacle collision"]
        return "Exit status: " + exit_status[self.exit_flag] + "\n" + \
               "Distance to goal: {:.2f}\n".format(self.dist_to_goal) + \
               "Distance travelled: {:.2f}\n".format(self.dist_travelled) + \
               "Compute time: {:.0f} ms".format(self.compute_time)


def f(x, xg, obstacles, adapt_obstacle_velocity=0, unit_magnitude=False, crep=1, rho=1, tail_effect=True, convergence_tolerance=1e-4, d=False):
    # t0 = time.time()
    if np.linalg.norm(x - xg) < convergence_tolerance:
        return 0*x

    No = len(obstacles)
    fa = (xg - x) / np.linalg.norm(xg - x)  # Attractor dynamics
    if No == 0:
        return fa

    ef = [-fa[1], fa[0]]
    Rf = np.vstack((fa, ef)).T

    mu = [obs.reference_direction(x) for obs in obstacles]
    # t1 = time.time()
    normal = [obs.normal(x) for obs in obstacles]
    # print("Normal: " + str((time.time()-t1)*1000) + " ms, ")
    gamma = [obs.distance_function(x) for obs in obstacles]

    # Compute obstacle velocities
    xd_o = np.zeros((2, No))
    if adapt_obstacle_velocity:
        for i, obs in enumerate(obstacles):
            xd_o[:, i] = obs.vel_intertial_frame(x)


    kappa = 0.
    f_mag = 0.
    for i in range(No):
        # Compute basis matrix
        E = np.zeros((2, 2))
        E[:, 0] = mu[i]
        E[:, 1] = [-normal[i][1], normal[i][0]]
        # Compute eigenvalues
        D = np.zeros((2, 2))
        D[0, 0] = 1 - crep / (gamma[i] ** (1 / rho)) if tail_effect or normal[i].dot(fa) < 0. else 1
        D[1, 1] = 1 + 1 / gamma[i]
        # Compute modulation
        M = E.dot(D).dot(np.linalg.inv(E))
        # f_i = M.dot(fa)
        f_i = M.dot(fa - xd_o[:, i]) + xd_o[:, i]
        # Compute weight
        num = np.prod([gamma[j] - 1 for j in range(No) if j != i])
        den = np.sum([np.prod([gamma[j] - 1 for j in range(No) if j != k]) for k in range(No)])
        w = num / den
        # Compute contribution to velocity magnitude
        f_i_abs = np.linalg.norm(f_i)
        f_mag += w * f_i_abs
        # Compute contribution to velocity direction
        nu_i = f_i / f_i_abs
        nu_i_hat = Rf.T.dot(nu_i)
        kappa_i = np.arccos(np.clip(nu_i_hat[0], -1, 1)) * np.sign(nu_i_hat[1])
        kappa += w * kappa_i
    kappa_norm = abs(kappa)
    f_o = Rf.dot([np.cos(kappa_norm), np.sin(kappa_norm) / kappa_norm * kappa]) if kappa_norm > 0. else fa

    if d:
        import matplotlib.pyplot as plt
        from matplotlib.pyplot import cm
        col = cm.rainbow(np.linspace(0, 1, 6))
        plt.figure()
        ax = plt.gca()

        print(D)

        ax.quiver(x[0], x[1], E[0, 0], E[1, 0], color='g')
        ax.quiver(x[0], x[1], E[0, 1], E[1, 1], color='r')
        # ax.text(x[0]+0.1*E[0, 0], x[1]+0.1*E[1, 0], str(D[0, 0]))
        # ax.text(x[0]+0.1*E[0, 1], x[1]+0.1*E[1, 1], str(D[1, 1]))

        for i, o in enumerate(obstacles):
            o.draw(ax=ax, ec='k', linestyle='--')
            b = o.boundary_point(x)
            ax.plot(*b, 'x', color=col[i])
            ax.quiver(b[0], b[1], normal[i][0], normal[i][1], color=col[i])
            # ax.text(b[0], b[1], str(gamma[i]))
            # ax.text(b[0], b[1], str(ws[i]))
        ax.quiver(x[0], x[1], f_i[0], f_i[1], color='k')
        # ax.quiver(x[0], x[1], fa[0], fa[1], color='g')
        ax.plot(*x, 'ks')
        plt.show()

    if unit_magnitude:
        f_mag = 1.
    return f_mag * f_o


def rollout(x0, xg, obstacles, step_length, convergence_tolerance, max_travelled_distance, max_compute_time, gamma=0.9,
            crep=1, rho=1, max_nr_steps=100, tail_effect=True, d=False):
    t_init_rollout = time.time()
    # Initialize
    x = np.empty((max_nr_steps, 2))
    x[0, :] = x0
    dist_travelled = 0.
    for i in range(1, max_nr_steps):
        # print(i)
        dist_to_goal = np.linalg.norm(x[i-1, :] - xg)
        compute_time = (time.time() - t_init_rollout) * 1000.
        # Check exit conditions
        if dist_to_goal < convergence_tolerance:
            return x[:i, :], SOADSRolloutStatus(0, dist_to_goal, dist_travelled, compute_time)
        if dist_travelled > max_travelled_distance:
            return x[:i, :], SOADSRolloutStatus(1, dist_to_goal, dist_travelled, compute_time)
        if compute_time > max_compute_time:
            return x[:i, :], SOADSRolloutStatus(2, dist_to_goal, dist_travelled, compute_time)
        if any([o.interior_point(x[i-1, :]) for o in obstacles]):
            return x[:i, :], SOADSRolloutStatus(4, dist_to_goal, dist_travelled, compute_time)

        # Movement using SOADS dynamics
        x[i, :] = x[i-1, :] + min(step_length, dist_to_goal) * f(x[i-1, :], xg, obstacles, unit_magnitude=True,
                                                                 crep=crep, rho=rho, tail_effect=tail_effect, d=d)

        # Check if path inside any obstacle
        intersection_points = []
        for o in obstacles:
            intersection_points += o.line_intersection([x[i-1, :], x[i, :]])
        # If path penetrates obstacle, shorten step with interpolation to closest collision point
        if intersection_points:
            # Use closest point
            dist_intersection_points = np.linalg.norm(np.array(intersection_points) - x[i-1, :], axis=1)
            ip = intersection_points[np.argmin(dist_intersection_points)]
            # Interpolation
            x_interior = x[i, :]
            x[i, :] = (1.-gamma) * x[i-1, :] + gamma * ip

            # if any([o.interior_point(x[i, :]) for o in obstacles]):
            #     f(x[i - 1, :], xg, obstacles, unit_magnitude=True,
            #       crep=crep, rho=rho, tail_effect=tail_effect, d=1)
            #     for o in obstacles[0].hull_cluster():
            #         print(o.point_location(x[i-1, :]))
            #     print(x[i-1, :])
            #     # obstacles[0].normal(x[i-1, :], debug=1)
            #     import matplotlib.pyplot as plt
            #     fig, ax = plt.subplots()
            #     for o in obstacles:
            #         o.draw(ax=ax)
            #         ax.quiver(*o.boundary_point(x[i-1, :]), *o.normal(x[i-1, :]), color='k')
            #     ax.plot(*x[i-1, :], 'ko')
            #     # ax.plot(*x_interior, 'rx')
            #     # ax.plot(*ip, 'yd')
            #     # ax.plot(*x[i, :], 'go')
            #     plt.show()

        # Update travelled distance
        dist_travelled += np.linalg.norm(x[i, :] - x[i-1, :])

    return x, SOADSRolloutStatus(3, dist_to_goal, dist_travelled, compute_time)

def plot_streamlines(
    points_init,
    ax,
    obs=[],
    attractorPos=[0, 0],
    dim=2,
    dt=0.01,
    max_simu_step=300,
    convergence_margin=0.03,
):
    """Plot streamlines."""
    n_points = np.array(points_init).shape[1]

    x_pos = np.zeros((dim, max_simu_step + 1, n_points))
    x_pos[:, 0, :] = points_init

    for j in range(n_points):
        print(x_pos[:, 0, j])
        print(points_init)
    it_count = 0
    for iSim in range(max_simu_step):
        for j in range(n_points):
            x_pos[:, iSim + 1, j] = x_pos[:, iSim, j] + f(x_pos[:, iSim, j], attractorPos, obs) * dt
            # obs_avoidance_rk4(
            #     dt,
            #     x_pos[:, iSim, j],
            #     obs,
            #     obs_avoidance=obs_avoidance_interpolation_moving,
            #     ds=LinearSystem(attractor_position=attractorPos).evaluate,
            # )

        # Check convergence
        # if (
        #     np.sum(
        #         (x_pos[:, iSim + 1, :] - np.tile(attractorPos, (n_points, 1)).T) ** 2
        #     )
        #     < convergence_margin
        # ):
        #     x_pos = x_pos[:, : iSim + 2, :]
        #
        #     print("Convergence reached after {} iterations.".format(it_count))
        #     break
        # it_count += 1

    for j in range(n_points):
        ax.plot(x_pos[0, :, j], x_pos[1, :, j], "--", linewidth=4, color="r")
        ax.plot(
            x_pos[0, 0, j],
            x_pos[1, 0, j],
            "k*",
            markeredgewidth=4,
            markersize=13,
            zorder=5,
        )

def plot_vector_field(xg, obstacles, ax, xlim=None, ylim=None, n=50, **kwargs):
    if xlim is None:
        xlim = ax.get_xlim()
    if ylim is None:
        ylim = ax.get_ylim()

    Y, X = np.mgrid[ylim[0]:ylim[1]:n*1j, xlim[0]:xlim[1]:n*1j]
    U = np.zeros(X.shape)
    V = np.zeros(X.shape)
    mask = np.zeros(U.shape, dtype=bool)
    for i in range(X.shape[1]):
        for j in range(X.shape[0]):
            x = np.array([X[i, j], Y[i, j]])
            if np.any([o.interior_point(x) for o in obstacles]):
                mask[i, j] = True
                U[i, j] = np.nan
            else:
                U[i, j], V[i, j] = f(x, xg, obstacles)
    U = np.ma.array(U, mask=mask)
    return ax.streamplot(X, Y, U, V, **kwargs)