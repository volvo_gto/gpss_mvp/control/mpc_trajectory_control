import casadi.casadi as cs
import numpy as np
import shapely.affinity
from scipy.spatial import ConvexHull
import cdd # apt-get install libgmp-dev python3-dev, pip install pycddlib
from shapely.geometry import Polygon, Point
import math

import mpc.obstacles as ob
import mpc.soads.soads as soads
from mpc.RobotModelData import RobotModelData, RunConfig, Constants
from shapely.geometry import LinearRing
import matplotlib.pyplot as plt

def get_angle_between_points(point1,point2):
    #Gets the angle between two points, with point1 seen as the "base" point
    #point1 = [x1,y1]
    x1 = point1[0]
    y1 = point1[1]
    x2 = point2[0]
    y2 = point2[1]
    angle = math.atan2(y2-y1,x2-x1)
    if angle < 0:
        angle = angle+2*math.pi
    return angle

def make_obstacles_StarshapedPolygon(obstacles):
    # Takes all static obstacles and makes them of type StarshapedPolygon,
    # and later adds hull.
    starshaped_obstacles = []
    for OneObstacle in obstacles['Extra_pad']:
        if OneObstacle != None:
            starshaped_obstacles.append(ob.StarshapedPolygon(OneObstacle))

    #Boundaries are divided into lines that dont intersect.
    Boundaries = obstacles['Boundaries']
    d = 1
    x1 = Boundaries.exterior.coords[:][0][0]
    y1 = Boundaries.exterior.coords[:][0][1]
    x2 = Boundaries.exterior.coords[:][1][0]
    y2 = Boundaries.exterior.coords[:][1][1]
    x3 = Boundaries.exterior.coords[:][2][0]
    y3 = Boundaries.exterior.coords[:][2][1]
    x4 = Boundaries.exterior.coords[:][3][0]
    y4 = Boundaries.exterior.coords[:][3][1]

    line1 = Polygon([[x1+d, y1], [x2-d, y2], [x2-d, y2-d], [x1+d, y1-d]])
    line2 = Polygon([[x2, y2+d], [x3, y3-d], [x3+d, y3-d], [x2+d, y2+d]])
    line3 = Polygon([[x3-d, y3], [x4+d, y4], [x4+d, y4+d], [x3-d, y3+d]])
    line4 = Polygon([[x4, y4-d], [x1, y1+d], [x1-d, y1+d], [x4-d, y4-d]])

    starshaped_obstacles.append(ob.StarshapedPolygon(line1))
    starshaped_obstacles.append(ob.StarshapedPolygon(line2))
    starshaped_obstacles.append(ob.StarshapedPolygon(line3))
    starshaped_obstacles.append(ob.StarshapedPolygon(line4))

    return starshaped_obstacles

def add_boundaries_to_star_obstacles_as_StarshapedPolygon(star_obstacles, obstacles):
    Boundaries = obstacles['Boundaries']
    d = 1
    x1 = Boundaries.exterior.coords[:][0][0]
    y1 = Boundaries.exterior.coords[:][0][1]
    x2 = Boundaries.exterior.coords[:][1][0]
    y2 = Boundaries.exterior.coords[:][1][1]
    x3 = Boundaries.exterior.coords[:][2][0]
    y3 = Boundaries.exterior.coords[:][2][1]
    x4 = Boundaries.exterior.coords[:][3][0]
    y4 = Boundaries.exterior.coords[:][3][1]

    line1 = Polygon([[x1-d, y1], [x2, y2], [x2, y2-d], [x1-d, y1-d]])
    line2 = Polygon([[x2, y2-d], [x3, y3], [x3+d, y3], [x2+d, y2-d]])
    line3 = Polygon([[x3+d, y3], [x4, y4], [x4, y4+d], [x3+d, y3+d]])
    line4 = Polygon([[x4, y4+d], [x1, y1], [x1-d, y1], [x4-d, y4+d]])

    star_obstacles.append(ob.StarshapedPolygon(line1))
    star_obstacles.append(ob.StarshapedPolygon(line2))
    star_obstacles.append(ob.StarshapedPolygon(line3))
    star_obstacles.append(ob.StarshapedPolygon(line4))
    return star_obstacles

def get_regenerated_ref(robots, robot_id, x0_Design, xg_Design, obstacles):
    # x0_Design start position, and xg_Design goal position
    x0_Design_numpy = np.array(x0_Design)
    xg_Design_numpy = np.array(xg_Design)

    epsilon = 0.2
    soads_step_length = RobotModelData.ts*RobotModelData.nominal_v
    d = 0
    star_clusters = None

    obstacles_StarshapedPolygon = make_obstacles_StarshapedPolygon(obstacles)

    # Cluster obstacles and make starshaped
    star_clusters, obstacle_timing = ob.cluster_and_starify(obstacles_StarshapedPolygon, x0_Design_numpy,
                                                            xg_Design_numpy, epsilon,
                                                            star_clusters, make_convex=1,
                                                            exclude_obstacles=False, debug=d)
    star_obstacles = [cl.cluster_obstacle for cl in star_clusters]

    # Boundaries are added after obstacles are cluster_and_starify to avoid hull.
    star_obstacles = add_boundaries_to_star_obstacles_as_StarshapedPolygon(star_obstacles, obstacles)
    if RunConfig.RefGeneration_overwrite == False:
        max_nr_steps = RobotModelData.N
        max_travelled_distance = RobotModelData.N * RobotModelData.vmax * RobotModelData.ts
    if RunConfig.RefGeneration_overwrite == True:
        max_nr_steps = RobotModelData.N*100 # almost 200 meter
        max_travelled_distance = RobotModelData.N * RobotModelData.vmax * RobotModelData.ts * 100 #almost 300 meter
    xs, status = soads.rollout(x0_Design_numpy, xg_Design_numpy, star_obstacles, step_length=soads_step_length,
                               convergence_tolerance=RobotModelData.ts*RobotModelData.nominal_v,
                               max_travelled_distance=max_travelled_distance, max_compute_time=1e4,
                               max_nr_steps=max_nr_steps, tail_effect=False, crep=1.1, rho=1.1)

    if RunConfig.Plot_starshaped == True:
        ax = None
        plt.close(2)
        plt.figure(1)
        for o in star_obstacles:
            ax, o_hs = o.draw(ax=ax, fc='b', alpha=0.8, show_name=False, show_reference=False)
        ax.plot(robots[robot_id]['State'][0], robots[robot_id]['State'][1], 'g*')

        for polygon in obstacles['Padded']:
            if polygon != None:
                x, y = polygon.exterior.xy
                ax.plot(x, y, color='k')
        x,y = obstacles['Boundaries'].exterior.xy
        plt.plot(x,y,color='k')
    return xs

def fill_Temporary_array_with_regenerated_ref(robot, r_i, regenerated_ref,robots,robots_id):
    ## This for-loop updates 2 of 5 Temporary's states with Albin_ref two states; x and y
    ## Sometimes regenerated_ref is shorter than our hoizon.
    ## It also makes sure that if the Albin's refpath is too short,
    ## that we fill out the rest of the horizon with our remainder path.

    ## TODO: 'Temporary' doesn't have angles included, as for example 'Ref' has.
    length_index = len(regenerated_ref) - 1
    i=0
    if RunConfig.RefGeneration_overwrite == False:
        for k in range(RobotModelData.N):
            if k <= length_index:
                robot['Temporary'][5 * k] = regenerated_ref[k][0]
                robot['Temporary'][5 * k + 1] = regenerated_ref[k][1]
                if k < length_index:
                    robot['Temporary'][5 * k + 2] = get_angle_between_points(list(regenerated_ref[k]),
                                                                             list(regenerated_ref[k + 1]))
                else:
                    robot['Temporary'][5 * k + 2] = get_angle_between_points(list(regenerated_ref[k]),
                                                                             [robot['Remainder'][r_i + 5 * i ],
                                                                              robot['Remainder'][r_i + 5 * i + 1]])
            if k > length_index:
                robot['Temporary'][5 * k] = robot['Remainder'][r_i + 5 * i ]
                robot['Temporary'][5 * k + 1] = robot['Remainder'][r_i + 5 * i + 1]
                robot['Temporary'][5 * k + 2] = get_angle_between_points([robot['Remainder'][r_i + 5 * i],
                                                                          robot['Remainder'][r_i + 5 * i + 1]],
                                                                         [robot['Remainder'][r_i + 5 * (i+1)],
                                                                          robot['Remainder'][r_i + 5 * (i+1) + 1]])
                i += 1
        return robot['Temporary'], robot['Remainder']
    if RunConfig.RefGeneration_overwrite == True:
        length_index = len(regenerated_ref)-1
        a=[]
        for k in range(length_index):
            if k<=19:
                if k <= length_index:
                    robot['Ref'][5 * k] = regenerated_ref[k][0]
                    robot['Ref'][5 * k + 1] = regenerated_ref[k][1]
                    if k < length_index:
                        robot['Ref'][5 * k + 2] = get_angle_between_points(list(regenerated_ref[k]),
                                                                           list(regenerated_ref[k + 1]))
                    else:
                        robot['Temporary'][5 * k + 2] = get_angle_between_points(list(regenerated_ref[k]),
                                                                                 [robot['Remainder'][r_i + 5 * i],
                                                                                  robot['Remainder'][r_i + 5 * i + 1]])
                else: # This should be needed if len(length_index) is less than RobotModelData.N
                    robot['Ref'][5 * k] = robot['Remainder'][r_i + 5 * i]
                    robot['Ref'][5 * k + 1] = robot['Remainder'][r_i + 5 * i + 1]
                    robot['Ref'][5 * k + 2] = robot['Remainder'][r_i + 5 * i + 2]
                    i += 1
            if k > 19:
                a1 = regenerated_ref[k][0]
                a2 = regenerated_ref[k][1]
                if k < length_index:
                    a3 = get_angle_between_points(list(regenerated_ref[k]),list(regenerated_ref[k+1]))
                else:
                    a3 = get_angle_between_points(list(regenerated_ref[k]),robot['Remainder'][r_i*5:r_i*5+2])
                a += [a1, a2, a3, 1, 0.1]
        a.extend(robot['Remainder'][((r_i)*5):])
        robots[robots_id]['Remainder'] = a
        return robot['Ref'], robots[robots_id]['Remainder']

def move_x0_outside_of_closest_border(x0, OneObstacle):
    # print('x0 is inside obstacle, old x0' , x0)
    # Point on the border:
    pol_ext = LinearRing(OneObstacle.exterior.coords)
    d = pol_ext.project(Point(x0[0], x0[1]))
    p = pol_ext.interpolate(d)
    closest_point_coords = list(p.coords)[0]
    # print('Closest point on border', closest_point_coords[0], closest_point_coords[1])

    x0[0] = x0[0] + 2 * (closest_point_coords[0] - x0[0])
    x0[1] = x0[1] + 2 * (closest_point_coords[1] - x0[1])
    # print('x0 moved outside static obstacle to', x0)
    # print('x difference', (closest_point_coords[0] - x0[0]))
    # print('y difference', (closest_point_coords[1] - x0[1]))

    return x0

def check_x0_and_xg_outside_static_obstacle(robots, robot_id, obstacles):
    r_i = 0  # Remainder Index
    points_ahead_to_look_for_a_good_xg_outside_obstacles = Constants.points_ahead_to_look_for_a_good_xg_outside_obstacles

    x0 = ([robots[robot_id]['State'][0],  # robot's position
           robots[robot_id]['State'][1]])
    xg = ([robots[robot_id]['Ref'][-5],  # Goal/End point
           robots[robot_id]['Ref'][-4]])
    x0_point = Point(x0[0], x0[1]) # Point format
    xg_point = Point(xg[0], xg[1]) # Point format

    was_I_inside_a_static_obstacle = True  # initial value
    while was_I_inside_a_static_obstacle == True:
        was_I_inside_a_static_obstacle = False
        for OneObstacle in obstacles["Extra_pad"]:
            if OneObstacle != None:
                if OneObstacle.contains(x0_point):
                    x0 = move_x0_outside_of_closest_border(x0, OneObstacle)
                    x0_point = Point(x0[0], x0[1])
                if OneObstacle.contains(xg_point):
                    was_I_inside_a_static_obstacle = True
                    # Change xg outside of the obstacle, to a other point further ahead in the full reference path
                    while OneObstacle.contains(xg_point):
                        r_i += points_ahead_to_look_for_a_good_xg_outside_obstacles
                        xg = ([robots[robot_id]['Remainder'][r_i * 5],
                               robots[robot_id]['Remainder'][r_i * 5 + 1]])  # Find new xg
                        xg_point = Point(xg[0], xg[1])
                    # print('robot %d, xg  was moved outside a obstacle to' % (robot_id), xg)
                    r_i += 1    ## Even though point is outside of obstacle, Albins code
                                # doesn't work with that point. We need to take a other
                                # point, another timestep in the direction of the trajectory
                                # out of the obstacle and after that and it works fine.
                                # Therefore the extra r_i +=1, even though point outside obstacle.
                    xg = ([robots[robot_id]['Remainder'][r_i * 5],
                           robots[robot_id]['Remainder'][r_i * 5 + 1]])  # Find new xg
                    xg_point = Point(xg[0], xg[1])
    xg = ([robots[robot_id]['Remainder'][r_i * 5], robots[robot_id]['Remainder'][r_i * 5 + 1]])
    robots[robot_id]['New_Ref_start'] = x0
    robots[robot_id]['New_Ref_goal'] = xg
    return x0, xg, r_i

def update_Temporary_ref_with_regenerated_ref(robots, robot_id, obstacles):
    # print('Since reference is colliding now')
    x0, xg, r_i = check_x0_and_xg_outside_static_obstacle(robots, robot_id, obstacles)
    regenerated_ref = get_regenerated_ref(robots, robot_id, x0, xg, obstacles)
    robots[robot_id]['Temporary'], robots[robot_id]['Remainder'] = fill_Temporary_array_with_regenerated_ref(
        robots[robot_id], r_i, regenerated_ref, robots, robot_id)
    return robots[robot_id]['Temporary']

def check_if_ref_is_colliding_with_static_obstacles(robots,robot_id,obstacles):
    robots[robot_id]['Use Temporary'] = False
    for timestep in range(RobotModelData.N):
        x_nominal_ref_timestep = robots[robot_id]['Ref'][5 * timestep]
        y_nominal_ref_timestep = robots[robot_id]['Ref'][5 * timestep + 1]

        # x and y in Point format
        robot_x_y_nominal_reference_timestep = Point(x_nominal_ref_timestep, y_nominal_ref_timestep)

        for OneObstacle in obstacles["Extra_pad"]:
            if OneObstacle != None:
                if OneObstacle.contains(robot_x_y_nominal_reference_timestep):
                    # print('Reference is colliding with static obstacle')
                    robots[robot_id]['Use Temporary'] = True
                    return

def nominal_refpath_or_regenerate_refpath(robots, robot_id, obstacles):
    check_if_ref_is_colliding_with_static_obstacles(robots, robot_id, obstacles)
    if robots[robot_id]['Use Temporary'] == True:
        # print('Reference is colliding now with static obstacle')
        used_ref_in_mpc = update_Temporary_ref_with_regenerated_ref(robots, robot_id, obstacles)
    elif robots[robot_id]['Use Temporary'] == False:
        # print('Reference is NOT colliding now with static obstacle')
        used_ref_in_mpc = robots[robot_id]['Ref']
    return used_ref_in_mpc

def dynamics_ct(x,u,ts):
    #calculate the dynamics of the ATR model
    dx = ts * u[0] * cs.cos(x[2])
    dy = ts * u[0] * cs.sin(x[2])
    dtheta = ts * u[1]
    return cs.vertcat(dx, dy, dtheta)

def model_mpc(x,y,theta,u,ts):
    #RK4 discretization of the ATR model
    x_all = cs.vertcat(x,y,theta)
    k1 = dynamics_ct(x_all, u,ts)
    k2 = dynamics_ct(x_all + 0.5 * k1, u,ts)
    k3 = dynamics_ct(x_all + 0.5 * k2, u,ts)
    k4 = dynamics_ct(x_all + k3, u,ts)
    x_next = x_all + (1 / 6) * (k1 + 2 * k2 + 2 * k3 + k4)
    return x_next[0],x_next[1],x_next[2]

def model(x,y,theta,u,ts):
    #Returns RK4 discretization as float values instead of casadi variables which are used in the mpcgenerator
    x,y,theta = model_mpc(x,y,theta,u,ts)
    return x.__float__(), y.__float__(),theta.__float__()

def generate_straight_trajectory(x,y,theta,v,ts,N):
    #generates a straight trajectory with starting points x,y and with point distance v*ts at an angle theta for
    #N timesteps
    states = [x,y,theta,v,0]

    for i in range(0,N): 
        if i == N-1: 
            v = 0
        x,y,theta = model(x,y,theta,[v,0],ts=ts)
        states.extend([x,y,theta,v,0])

    return states

def dist_to_ref(robots):
    """dist = 0
    for robot_id in robots: 
        x_ref,y_ref = robots[robot_id]['Ref'][0:2]
        x,y = robots[robot_id]['State'][0:2]
        dist += math.sqrt((x-x_ref)**2+(y-y_ref)**2)"""
    dist = []
    for robot_id in robots: 
        x_ref,y_ref = robots[robot_id]['Ref'][0:2]
        x,y = robots[robot_id]['State'][0:2]
        dist.append(math.sqrt((x-x_ref)**2+(y-y_ref)**2))
    return dist

def dist_to_ref_line(robot):
    #calculates the distance from the robot the the line representation of the reference
    #get required data
    x,y = robot['State'][0],robot['State'][1]
    if robot['Use Temporary'] == True:
        ref = robot['Temporary']
    else:
        ref = robot['Ref']
    x_ref = ref[0::5]
    y_ref = ref[1::5]

    #define point we are at
    p = np.array([x,y])

    #first point on line segment
    s1 = np.array([x_ref[0],y_ref[0]])

    #vector for holding distance to each line
    dist_vec = np.array([])

    #loop over all line segments
    for i in range(1,RobotModelData.N):
        #get next point
        s2 = np.array([x_ref[i],y_ref[i]])

        #calculate t_hat and t_star
        t_hat = np.dot(p - s1, s2 - s1) / ((s2[1] - s1[1]) ** 2 + (s2[0] - s1[0]) ** 2 + 1e-16)
        t_star = np.fmin(np.fmax(t_hat,0.0),1.0)

        #get closest point from the line s
        st = s1+t_star*(s2-s1)

        #vector from point to closest point on the line
        dvec = st-p

        #calculate distance
        dist = dvec[0] ** 2 + dvec[1] ** 2

        #add to distance vector
        dist_vec = np.append(dist_vec,dist)

        #update s1
        s1 = s2

    return np.amin(dist_vec)

def generate_turn_right_trajectory(x,y,theta,v,ts,N1,N2):
    #generates a 90 degree right turn trajectory
    #starts at x,y with point distance v*ts and has length N1 before turning and N2 after turning
    states = [x,y,theta,v,0]

    theta_turn = theta-cs.pi/2
    if theta_turn < 0:
        theta_turn+2*cs.pi

    for i in range(0,N1): 
        x,y,theta = model(x,y,theta,[v,0],ts=ts)
        states.extend([x,y,theta,v,0])

    for i in range(0,N2): 
        if i == N2-1: 
            v = 0
        x,y,theta_turn = model(x,y,theta_turn,[v,0],ts=ts)
        states.extend([x,y,theta_turn,v,0])

    return states

def generate_turn_left_trajectory(x,y,theta,v,ts,N1,N2):
    # generates a 90 degree left turn trajectory
    # starts at x,y with point distance v*ts and has length N1 before turning and N2 after turning
    states = [x,y,theta,v,0]

    theta_turn = theta+cs.pi/2
    if theta_turn > 2*cs.pi:
        theta_turn-2*cs.pi

    for i in range(0,N1):
        x,y,theta = model(x,y,theta,[v,0],ts=ts)
        states.extend([x,y,theta,v,0])

    for i in range(0,N2):
        if i == N2-1:
            v = 0
        x,y,theta_turn = model(x,y,theta_turn,[v,0],ts=ts)
        states.extend([x,y,theta_turn,v,0])

    return states

def control_action_to_trajectory(x,y,theta,u,ts):
    #generates a trajectory from a set of control inputs
    # Get the linear and angular velocities
    v = u[0::2]
    w = u[1::2]

    # Create a list of x and y states
    xlist = []
    ylist = []

    for vi,wi in zip(v,w):
        x,y,theta = model(x,y,theta,[vi,wi],ts)
        xlist.append(x)
        ylist.append(y)

    return xlist,ylist

def predict(x,y,dx,dy, N, ts):
    #predicts future centers for the dynamic obstacle
    xy = []
    for i in range(0,N):
            x += ts*dx
            y += ts*dy
            xy.append(x)
            xy.append(y)
    return xy

def unpadded_square(xc,yc,width,height):
    #generates a square with shapely Polygon
    return Polygon( [[xc-width/2, yc-height/2],[xc+width/2, yc-height/2],[xc+width/2, yc+height/2],
                     [xc-width/2, yc+height/2] ])

def padded_square(xc,yc,width,height, pad):
    #Generates a padded square with shapely Polygon
    padw = (pad+width/2)
    padh = (pad+height/2)
    return Polygon([[xc-padw, yc-padh],[xc+padw, yc-padh],[xc+padw, yc+padh],[xc-padw, yc+padh] ])

def polygon_to_eqs(polygon):
        #get the variables for halfspace inequalities of polygon
        if polygon == None: 
            return [0]*12
        vertices = polygon.exterior.coords[:-1]
        A, b = compute_polytope_halfspaces(vertices)
        return [A[0,0],A[0,1],b[0],A[1,0],A[1,1],b[1],A[2,0],A[2,1],b[2],A[3,0],A[3,1],b[3] ]

def compute_polytope_halfspaces(vertices):
    """

    Implementation from: https://github.com/stephane-caron/pypoman
    
    Compute the halfspace representation (H-rep) of a polytope defined as
    convex hull of a set of vertices:
    .. math::
        A x \\leq b
        \\quad \\Leftrightarrow \\quad
        x \\in \\mathrm{conv}(\\mathrm{vertices})
    Parameters
    ----------
    vertices : list of arrays
        List of polytope vertices.
    Returns
    -------
    A : array, shape=(m, k)
        Matrix of halfspace representation.
    b : array, shape=(m,)
        Vector of halfspace representation.
    """

    V = np.vstack(vertices)
    t = np.ones((V.shape[0], 1))  # first column is 1 for vertices
    tV = np.hstack([t, V])
    mat = cdd.Matrix(tV, number_type='float')
    mat.rep_type = cdd.RepType.GENERATOR
    P = cdd.Polyhedron(mat)
    bA = np.array(P.get_inequalities())
    if bA.shape == (0,):  # bA == []
        return bA
    # the polyhedron is given by b + A x >= 0 where bA = [b|A]
    b, A = np.array(bA[:, 0]), -np.array(bA[:, 1:])
    return (A, b)

def robot_to_polygon(x,y,theta):
    #Represent a robot as a shapely polygon
    length = RobotModelData.length
    width = RobotModelData.width
    #get shape of robot
    corners = np.array([[length / 2, width / 2],
                        [length / 2, -width / 2],
                        [-length / 2, -width / 2],
                        [-length / 2, width / 2],
                        [length / 2, width / 2]]).T

    # Define rotation matrix
    rot = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])

    # Rotate rectangle with the current angle
    rot_corners = rot @ corners

    #remake to array that Polygon can use and displace the shape with the position of the robot
    real_corners = [[x+rot_corners[0,i],y+rot_corners[1,i]]for i in range(len(rot_corners[1]))]
    ATR_polygon = Polygon(real_corners)

    return ATR_polygon

def extract_xy(predicted_states):
    #extract only the x and y states from predicted states list
    np_predicted = np.array(predicted_states)
    predicted_xy = np_predicted.reshape(-1,3)[:,:2].reshape(-1)
    return predicted_xy

def check_collision_robots(robots,robot_id,ATR_poly,predicted_states,timestep,critical_point, horizon,
                           multiplier,stopped_robots):
    #checks for collisions between robots
    #loop through all other robots
    collision_found = False
    for other_id in robots:
        if other_id != robot_id:
            #Get predicted states of the other robot
            other_x, other_y, other_theta = predicted_states[other_id][timestep*3:timestep*3+3]
            #turn other ATR into polygon
            Other_poly = robot_to_polygon(other_x,other_y,other_theta)
            #distance less than 1 means robots are predicted to collide and they need to slow down
            if ATR_poly.intersects(Other_poly):
                #only save smallest multiplier
                multiplier[robot_id] = np.minimum(multiplier[robot_id],
                                                  np.maximum(Constants.minimum_collision_multiplier, timestep/horizon))
                #if it is closer than the critical point, the robot needs to stop unless it has a free way ahead and is
                #stopped by a robot being to close to its side
                if timestep <= critical_point:
                    #check if the future states are free from collisions with other robots 4 timesteps ahead, while the
                    #other stays in its place
                    for j in range(critical_point):
                        xpred, ypred,thetapred = predicted_states[robot_id][(timestep+j)*3:(timestep+j)*3+3]
                        other_x,other_y,other_theta = robots[other_id]["State"][:3]
                        ATR_poly = robot_to_polygon(xpred,ypred,thetapred)
                        Other_poly = robot_to_polygon(other_x,other_y,other_theta)
                        #check distance if they are still colliding and if that is the case stop the robot
                        if ATR_poly.intersects(Other_poly):
                            multiplier[robot_id] = 0
                            collision_found = True
                            return multiplier, collision_found, predicted_states, stopped_robots, robots

                    #if the loop doesnt return the robotid has free way and other id must stop
                    predicted_states[other_id] = robots[other_id]["State"][:3]*horizon
                    stopped_robots.append(other_id)
                    multiplier[other_id] = 0
                    robots[other_id]["u"][:2] = [0,0]


    return multiplier, collision_found, predicted_states, stopped_robots,robots

def check_collision_static(ATR_poly,obstacles,timestep,critical_point,horizon,multiplier):
    #checks for collisions between static obstacles and an ATR
    collision_found = False
    for tmp_polygon in obstacles["Unpadded"]:
        if tmp_polygon != None:
            if tmp_polygon.intersects(ATR_poly):
                multiplier = np.minimum(multiplier,np.maximum(Constants.minimum_collision_multiplier,
                                                              timestep / horizon))
                if timestep <= critical_point:
                    multiplier = 0
                    collision_found = True
                return multiplier, collision_found
    return multiplier, collision_found

def check_collision_dynamic(xpred,ypred,obstacles,a,b,phi,centers,timestep,critical_point,horizon,multiplier):
    #checks for collisions between a dynamic obstacle(ellipse) and a ATR
    collision_found = False
    if obstacles["Dynamic"]["active"] == True:
        c = centers[timestep * 2:timestep * 2 + 2]
        dist = ((xpred-c[0])*cs.cos(phi)+(ypred-c[1])*cs.sin(phi))**2/a**2+\
               ((xpred - c[0]) * cs.sin(phi) - (ypred - c[1]) * cs.cos(phi)) ** 2 / b ** 2
        if dist <= 1:
            multiplier = np.minimum(multiplier,np.maximum(Constants.minimum_collision_multiplier,timestep/horizon))
            if timestep <= critical_point:
                multiplier = 0
                collision_found = True
            return multiplier, collision_found
    return multiplier,collision_found

def check_collision(robots,obstacles,predicted_states):
    #Create list that will contain all stopped robots
    stopped_robots = []

    # Define simulation horizon
    horizon = RobotModelData.N
    #Calculate the predicted centers and shape of the dynamic obstacle
    centers = predict(obstacles['Dynamic']['center'][0],
                         obstacles['Dynamic']['center'][1],
                         obstacles['Dynamic']['vel'][0],
                         obstacles['Dynamic']['vel'][1],
                         horizon,
                         0.1)

    a = obstacles["Dynamic"]["a"]+obstacles["Dynamic"]["apad"]
    b = obstacles["Dynamic"]["b"]+obstacles["Dynamic"]["bpad"]
    phi = obstacles["Dynamic"]["phi"]

    multiplier = [None]*len(robots)

    for robot_id in robots:
        #calculate critical point(timestep)
        margin = Constants.collision_margin
        #calculate braking distance https://physics.stackexchange.com/questions/3818/stopping-distance-frictionless
        deceleration = Constants.decceleration #m/s^2, placeholder
        braking_dist = robots[robot_id]["State"][3]**2/(2*deceleration)
        maximum_dist = margin+braking_dist
        #set how far ahead the robot must stop, should in future rely on current speed and maximum decceleration
        critical_point = 0
        x,y = robots[robot_id]["State"][:2]
        multiplier[robot_id] = 1

        for i in range(20):
            xp, yp = predicted_states[robot_id][i*3:i*3+2]
            #check distance between current state and the predicted states
            dist = np.sqrt((x-xp)**2+(y-yp)**2)
            if dist > maximum_dist:
                break
            elif critical_point == 10:
                break
            critical_point += 1

        if robot_id in stopped_robots:
            robots[robot_id]["u"][:2] = [0,0]
            multiplier[robot_id] = 0
            #print("in stopped")
        else:
            for timestep in range(horizon):
                xpred, ypred, thetapred = predicted_states[robot_id][timestep*3:timestep*3+3]
                ATR_poly = robot_to_polygon(xpred, ypred, thetapred)
                #check for collisions with static objects
                multiplier[robot_id], collision_found = check_collision_static(ATR_poly,obstacles,timestep,
                                                                               critical_point,horizon,
                                                                               multiplier[robot_id])
                if collision_found:
                    predicted_states[robot_id] = robots[robot_id]["State"][:3]*horizon
                    stopped_robots.append(robot_id)
                    #print("robot:{}, cause: static".format(robot_id))
                    robots[robot_id]["u"][:2] = [0,0]
                    break

                #check for collisions with dynamic objects
                multiplier[robot_id], collision_found = check_collision_dynamic(xpred, ypred, obstacles, a, b, phi,
                                                                                centers, timestep, critical_point,
                                                                                horizon, multiplier[robot_id])
                if collision_found:
                    predicted_states[robot_id] = robots[robot_id]["State"][:3]*horizon
                    stopped_robots.append(robot_id)
                    #print("robot:{}, cause: dynamic".format(robot_id))
                    robots[robot_id]["u"][:2] = [0,0]
                    break

                #check for collisions with other robots
                multiplier, collision_found, predicted_states, stopped_robots, robots = check_collision_robots(robots,
                                                robot_id, ATR_poly, predicted_states, timestep, critical_point, horizon,
                                                multiplier,stopped_robots)
                if collision_found:
                    predicted_states[robot_id] = robots[robot_id]["State"][:3]*horizon
                    stopped_robots.append(robot_id)
                    #print("robot:{}, cause: robot".format(robot_id))
                    robots[robot_id]["u"][:2] = [0,0]
                    break

            robots[robot_id]["u"][0] = robots[robot_id]["u"][0]*multiplier[robot_id]
            #print("regular,robot:{},mult:{}".format(robot_id,multiplier[robot_id]))
    return robots,multiplier

def closest_ref(robot):
    #Find the index of the closest reference point
    N = RobotModelData.N
    x = robot["State"][0]
    y = robot["State"][1]
    ref = robot["Ref"]
    xref = ref[0::5]
    yref = ref[1::5]
    dist = 100
    for i in range(N):
        dist_tmp = np.sqrt((x - xref[i]) ** 2 + (y - yref[i]) ** 2)
        if dist_tmp <= dist:
            closest_index = i
            dist = dist_tmp
    return closest_index

def displace_ref(robot):
    #Displaces the reference to start at the robots position, is necessary if many tracking error points are used.
    #IS NOT WORKING PROPERLY AND NOT USED
    x_robot, y_robot = robot['State'][0],robot['State'][1]
    x_ref, y_ref = robot['Ref'][0],robot['Ref'][1]
    #reftot = robot['State']+robot['Ref']
    reftot = [robot['State'][0]]+[robot['State'][1]]+robot['State'][2:]+robot['Ref']
    x_diff = x_robot-x_ref
    y_diff = y_robot-y_ref
    new_ref = []
    nx = RobotModelData.nx
    for i in range(1,RobotModelData.N+1):
        if reftot[(i-1)*nx:(i-1)*nx+nx] == reftot[i*nx:i*nx+nx] or reftot[nx:2*nx] == reftot[2*nx:3*nx]:
            new_ref += [reftot[i * RobotModelData.nx]]
            new_ref += [reftot[i * RobotModelData.nx + 1]]
        else:
            new_ref += [reftot[i*RobotModelData.nx]+x_diff*abs(np.cos(reftot[i*RobotModelData.nx+2]))+
                        0.1*np.cos(reftot[i*RobotModelData.nx+2])]
            new_ref += [reftot[i*RobotModelData.nx+1]+y_diff*abs(np.sin(reftot[i*RobotModelData.nx+2]))+
                        0.1*np.sin(reftot[i*RobotModelData.nx+2])]
        new_ref += [0]
        new_ref += [0]
        new_ref += [0]

    return new_ref

def get_run_data(time_vec,dist_dict,possible_collisions,closest_obstacle,bad_exit,time_ref_vec,time_col_vec):
    #save all of the gathered data in a list
    #get data for computation times
    avg_time = np.mean(time_vec)
    var_time = np.std(time_vec)
    max_time = np.max(time_vec)
    min_time = np.min(time_vec)

    #get data for distance to reference
    tot_dist = []
    for i in range(len(dist_dict)):
        tot_dist += dist_dict[i]

    avg_dist = np.mean(tot_dist)
    max_dist = np.max(tot_dist)
    std_dist = np.std(tot_dist)

    #get data for number of collisions
    tot_stopped_collisions = sum(possible_collisions)

    #get data for obstacles
    closest_static = min(closest_obstacle['Static'])
    closest_robot = min(closest_obstacle['Robot'])
    closest_dynamic = min(closest_obstacle['Dynamic'])
    closest_hard = min(closest_obstacle['Hard'])

    #get calculation times for check collision and the new references
    if time_ref_vec == []:
        mean_ref = 0
        max_ref = 0
    else:
        mean_ref = np.mean(time_ref_vec)
        max_ref = np.max(time_ref_vec)

    if time_col_vec == []:
        mean_col = 0
        max_col = 0
    else:
        mean_col = np.mean(time_col_vec)
        max_col = np.max(time_col_vec)

    run_data = [avg_time,var_time,max_time,min_time,avg_dist,max_dist,std_dist,
                tot_stopped_collisions,closest_static,closest_robot,closest_dynamic,
                closest_hard,mean_ref,max_ref,mean_col,max_col,bad_exit]
    return run_data

def ellipe_tan_dot(rx, ry, px, py, theta):
    '''Dot product of the equation of the line formed by the point
    with another point on the ellipse's boundary and the tangent of the ellipse
    at that point on the boundary.
    '''
    return ((rx ** 2 - ry ** 2) * math.cos(theta) * math.sin(theta) -
            px * rx * math.sin(theta) + py * ry * math.cos(theta))

def ellipe_tan_dot_derivative(rx, ry, px, py, theta):
    '''The derivative of ellipe_tan_dot.
    '''
    return ((rx ** 2 - ry ** 2) * (math.cos(theta) ** 2 - math.sin(theta) ** 2) -
            px * rx * math.cos(theta) - py * ry * math.sin(theta))

def estimate_distance(x, y, rx, ry, x0=0, y0=0, angle=0, error=1e-5):
    '''Given a point (x, y), and an ellipse with major - minor axis (rx, ry),
    its center at (x0, y0), and with a counter clockwise rotation of
    `angle` degrees, will return the distance between the ellipse and the
    closest point on the ellipses boundary.
    '''
    x -= x0
    y -= y0
    if angle:
        # rotate the points onto an ellipse whose rx, and ry lay on the x, y
        # axis
        angle = -math.pi / 180. * angle
        x, y = x * math.cos(angle) - y * math.sin(angle), x * math.sin(angle) + y * math.cos(angle)

    theta = math.atan2(rx * y, ry * x)
    while math.fabs(ellipe_tan_dot(rx, ry, x, y, theta)) > error:
        theta -= ellipe_tan_dot(
            rx, ry, x, y, theta) / \
            ellipe_tan_dot_derivative(rx, ry, x, y, theta)

    px, py = rx * math.cos(theta), ry * math.sin(theta)
    return ((x - px) ** 2 + (y - py) ** 2) ** .5

def ellipse_to_poly(c,a,b,phi):
    #create circle
    circle = Point(c[0],c[1]).buffer(1)
    #extend circle to ellipse
    ellipse = shapely.affinity.scale(circle,a,b)
    #rotate ellipse
    ellipse_r = shapely.affinity.rotate(ellipse,phi*180/np.pi)
    return ellipse_r

def get_dist_obstacles(robots,obstacles):
    #initialize distances
    dist_static = math.inf
    dist_robot = math.inf
    dist_dynamic = math.inf
    dist_hard = math.inf
    for robot_id in robots:
        x = robots[robot_id]['State'][0]
        y = robots[robot_id]['State'][1]
        theta = robots[robot_id]['State'][2]
        ATR_poly = robot_to_polygon(x,y,theta)
        xy_point = Point([x,y])

        #calculate distances to hard constraints
        dist_tmp = math.inf
        for obstacle in obstacles['Padded']:
            if obstacle != None:
                dist_tmp = min(dist_tmp,obstacle.distance(xy_point))
        dist_hard = min(dist_tmp,dist_hard)

        #calculate distance to static obstacles, including boundaries
        dist_tmp = math.inf
        for obstacle in obstacles['Unpadded']:
            if obstacle != None:
                dist_tmp = min(dist_tmp,obstacle.distance(ATR_poly))
        dist_tmp = min(dist_tmp,obstacles['Boundaries'].exterior.distance(ATR_poly))
        dist_static = min(dist_tmp,dist_static)

        #calculate distance to other robots
        dist_tmp = math.inf
        for other_id in robots:
            if other_id != robot_id:
                other_poly = robot_to_polygon(robots[other_id]['State'][0],robots[other_id]['State'][1],
                                              robots[other_id]['State'][2])
                dist_tmp = min(dist_tmp,ATR_poly.distance(other_poly))
        dist_robot = min(dist_tmp,dist_robot)

        #calculate distance to dynamic object(ellipse)
        dist_tmp = math.inf
        if obstacles['Dynamic']['active'] == True:
            a = obstacles["Dynamic"]["a"]# + obstacles["Dynamic"]["apad"]
            b = obstacles["Dynamic"]["b"]# + obstacles["Dynamic"]["bpad"]
            phi = obstacles["Dynamic"]["phi"]
            x_center = obstacles['Dynamic']['center'][0]
            y_center = obstacles['Dynamic']['center'][1]
            ellipse_poly = ellipse_to_poly([x_center,y_center],a,b,phi)
            #dist_tmp = min(dist_tmp,estimate_distance(x,y,a,b,x_center,y_center,phi))
            dist_tmp = min(dist_tmp,ATR_poly.distance(ellipse_poly))
            dist_dynamic = min(dist_dynamic,dist_tmp)


    return dist_static,dist_robot,dist_dynamic,dist_hard

def get_inputs_for_cost(predicted_states,robot_id,nr_of_robots,max_nr_of_robots,obstacles,N,ts,x,y):
    #calculate inputs for robot distance cost
    trajectories = []
    other_robots = []
    for other_id in predicted_states:
        if other_id != robot_id:
            trajectories.extend(extract_xy(predicted_states[other_id]))
    # fill with zero trajectories for missing robots
    trajectories.extend([0] * 2 * N * (max_nr_of_robots - nr_of_robots))
    # list with ones for robots that exist and 0 for robots that doesnt
    other_robots.extend([1] * (nr_of_robots - 1))
    other_robots.extend([0] * (max_nr_of_robots - nr_of_robots))

    #get input for static obstacles
    static = []
    closest_padded,closest_extrapadded = get_closest_obstacles([x,y],obstacles)
    for ob in closest_extrapadded:
        static.extend(polygon_to_eqs(ob))

    #get input for dynamic obstacles
    dynamic = []
    dynamic.append(obstacles['Dynamic']['a'] + obstacles['Dynamic']['apad'])
    dynamic.append(obstacles['Dynamic']['b'] + obstacles['Dynamic']['bpad'])
    dynamic.append(obstacles['Dynamic']['phi'])
    dynamic.extend(predict(obstacles['Dynamic']['center'][0],
                           obstacles['Dynamic']['center'][1],
                           obstacles['Dynamic']['vel'][0],
                           obstacles['Dynamic']['vel'][1],
                           N,
                           ts))

    return trajectories,other_robots,static,dynamic

def calculate_costs(robot_cost,robots,robot_id,obstacles,predicted_states,weights,distributed):
    #function for calculating all the separate costs to be able to plot them
    #get constants
    N = RobotModelData.N
    nx = RobotModelData.nx
    nu = RobotModelData.nu
    #weights = RobotModelData.get_weights()
    ts = RobotModelData.ts
    nr_of_robots = RobotModelData.nr_of_robots
    max_nr_of_robots = RobotModelData.max_nr_of_robots

    #get current and reference states for the robot
    ref = []
    ref.extend(robots[robot_id]["State"])
    if robots[robot_id]["Use Temporary"] == True:
        ref.extend(robots[robot_id]["Temporary"])
    else:
        ref.extend(robots[robot_id]["displaced_ref"])

    #get reference path with only x_ref,y_ref
    path = []
    for index in range(0, N + 1):
        x_ref = ref[index * nx]
        y_ref = ref[index * nx + 1]
        path += [x_ref, y_ref]

    x, y, theta, v, w = ref[:5]
    u = robots[robot_id]['u']+robots[robot_id]['s']

    #get inputs for some cost functions
    trajectories, other_robots, static, dynamic = get_inputs_for_cost(predicted_states,robot_id,nr_of_robots,
                                                                      max_nr_of_robots,obstacles,N,ts,x,y)

    #initialize the costs that are calculated
    tracking_cost = 0
    acceleration_cost = 0
    s_cost = 0
    collision_cost = 0
    static_cost = 0
    dynamic_cost = 0
    boundaries_cost = 0

    u_prev = ref[3:5]
    for i,j,k in zip(range(0,nx*N,nx),range(0,nu*N,nu),range(0,2*N,2)):
        uj = u[j:j+nu]

        x, y, theta = model(x, y, theta, uj, ts)

        if (i + nx) % (100 / RunConfig.NumberOf_S) == 0:
            s = u[int(nu * N + ((i + nx) / (100 / RunConfig.NumberOf_S)) - 1)]

            # tracking error
            te2 = distributed.tracking_error2([x, y], s, path)
            tracking_cost += weights[0] * te2
            s_cost += weights[11] * s

        collision_cost += distributed.cost_collision(x,y,trajectories,other_robots,k,N,weights[5])
        static_cost += distributed.cost_inside_polygon_soft(x,y,static,weights[8])
        dynamic_cost += distributed.cost_dynamic_obstacle(x,y,dynamic,j,weights[10])
        boundaries_cost += distributed.cost_outside_boundaries(x,y,polygon_to_eqs(obstacles["Boundaries"]),weights[9])
        acceleration_cost += distributed.cost_acceleration(u_prev,uj,weights[6],weights[7])
        u_prev = uj

    #append all costs to the robot_cost dict
    robot_cost[robot_id]["tracking_error"].append(tracking_cost)
    robot_cost[robot_id]["s"].append(s_cost)
    robot_cost[robot_id]["acceleration"].append(acceleration_cost)
    robot_cost[robot_id]["robots"].append(collision_cost)
    robot_cost[robot_id]["dynamic"].append(dynamic_cost)
    robot_cost[robot_id]["boundaries"].append(boundaries_cost)
    robot_cost[robot_id]["static"].append(static_cost)

    totcost = 0
    for test in robot_cost[robot_id]:
        totcost += robot_cost[robot_id][test][-1]

    #print("calculated cost:{}".format(totcost))

    return robot_cost[robot_id]

def get_closest_obstacles(xy,obstacles):
    #get the closest obstacles to the robot which are used in the MPC
    nr_of_obstacles = len(obstacles['Unpadded'])
    if nr_of_obstacles <= Constants.max_nr_obstacles:
        #use the existing obstacles and add "None" to fill out
        closest_padded = obstacles['Padded']+(Constants.max_nr_obstacles-nr_of_obstacles)*[None]
        closest_extrapadded = obstacles['Extra_pad']+(Constants.max_nr_obstacles-nr_of_obstacles)*[None]
    else:
        #find the closest obstacles
        xy_point = Point(xy[0],xy[1])
        distances = {}
        i = 0
        for ob in obstacles['Unpadded']:
            dist = xy_point.distance(ob)
            distances[i] = dist
            i += 1
        sorted_distances = {k: v for k, v in sorted(distances.items(), key=lambda item: item[1])}
        closest_ob_indices = list(sorted_distances.keys())[:5]
        closest_padded = [obstacles['Padded'][j] for j in closest_ob_indices]
        closest_extrapadded = [obstacles['Extra_pad'][j] for j in closest_ob_indices]

    return closest_padded,closest_extrapadded

def dist_to_goal(robot):
    #Get the distance from the ATR to the goal
    ATR_xy = robot['State'][:2]
    goal_xy = robot['x_goal'][:2]
    distance = (ATR_xy[0]-goal_xy[0])**2+(ATR_xy[1]-goal_xy[1])**2
    return distance





