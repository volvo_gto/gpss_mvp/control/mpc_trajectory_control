from dataclasses import dataclass
import numpy as np
import os

@dataclass
class RobotModelData:
    # Robot parameters
    nr_of_robots: int = 2       # Number of robots
    max_nr_of_robots: int = 20  # Maximum number of robots
    nx: int = 5                 # Number of states for each robot, includes velocity and angular velocity
    nu: int = 2             # Nr of control inputs
    N: int = 20             # Length of horizon
    nominal_v: int = 1      # Prefered speed
    width = 0.5             # Width of Robot
    length = 0.7            # Length of Robot
    smin = 0                # Minimum value s can take
    smax = 20               # Maxmimum value s can take
    vmin = -0.0             # Minimum Velocity
    vmax = 1.5              # Maximum Velocity
    wmax = 2                # Maximum angular velocity
    wmin = -2               # Minimum angular velocity

    # Model parameters, weights where not all are used. The ones used are commented with "used"
    ts: float = 0.1         # Sampling time
    q: float = 150          # Cost of deviating from x and y reference "used"
    qtheta: float = 1       # Cost of deviating from angle reference
    r: float = 0.01         # Cost for control action
    qN: float = 100         # Final cost of deviating from x and y reference
    qr: float = 50          # cost on reversing
    qobs: float = 200       # Cost for being closer than r to the other robot "used"
    qaccV: float = 50       # Cost for accelerating and decelerating "used"
    qaccW: float = 50       # Cost for angular acceleration "used"
    qpol: float = 200       # Cost for being inside obstacle (polygon) "used"
    qbounds: float = 200    # Cost for being out of bounds "used"
    qdyn: float = 500       # Cost for being inside dynamic object "used"
    qS: int = -20           # Cost on "s" variable "used"

    def get_weights(self):
        """
        Get all weights as a list.
        """
        return [self.q, self.qtheta, self.r, self.qN, self.qr, self.qobs, self.qaccV, self.qaccW, self.qpol,
                self.qbounds, self.qdyn, self.qS]

class Constants:
    decceleration = 10                  # assumed decceleration of ATR m/s^2
    collision_margin = 0.3              # Set for the check collision, isnt working properly
    minimum_collision_multiplier = 1    # lowest multiplier by check collision
    #maximim distance of two robots precisely touching
    collision_distance_robot = 2*np.sqrt((RobotModelData.width/2)**2+(RobotModelData.length/2)**2)
    points_ahead_to_look_for_a_good_xg_outside_obstacles = 20 #Used in generating new references, how many steps ahead
                                                    #each iteration takes when looking for a point outside of obstacles
    max_nr_obstacles = 5                # Maximum number of obstacles which are included in the MPC formulation
    padding = collision_distance_robot/2 # Padding for hard constraints
    extra_padding = 0.2                 # Padding for soft constraints on top if hard constraints
    goal_tol = 0.1                      # How close robots have to be to their goal position to be considered finished


class RunConfig:
    WithCollisionCheck = True           # Decide of to run with check collision or not
    RefGeneration = True                # Decide if new path should be generated
    RefGeneration_overwrite = True      # Decide if new paths should generate all the way or only N timesteps ahead
    Plot_starshaped = False             # Decide if the obstacles in the Refgeneration should be plotted
    RunAllCases = False                 # Decide if more than one case should be run consecutively
    CasesToRun = [14,10,3,5,4,2,6,7,9,15]   # If RunAllCases = True, list of the cases which will be ran
    SaveData = False                    # Decide if gathered data should be saved to a file
    HardConstraints = True              # Decide if Hard constraints should be used on static obstacles
    NumberOf_S = 1                      # Number of "s" points, also becomes number of tracking error points
    PlotCosts = False                   # Decide if the costs for the robots should be plotted
    max_iterations = 400                # Maximum number of iterations if the robots never reach their goal
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))   #Root directory of folder, used for the solver bindings

class PlotConfig:
    PlotPadHard = True          #Decide if the hard padding should be plotted
    PlotPadSoft = True          #Decide if the soft padding should be plotted
    PlotEntireRef = []          #list of robots to plot entire reference path for
    PlotNewRefStars = []        #list of robots to plot start and end points if a new path is generated for


