import shapely.geometry
import shapely.ops
import numpy as np
import time
from mpc.obstacles import Frame, StarshapedObstacle, Ellipse, Polygon, StarshapedPolygon, Static, is_ccw, is_cw, is_collinear, \
    line, ray, cone_from_rays, equilateral_triangle, draw_shapely_polygon, Polyhedron, RayCone, point_in_triangle
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
from copy import deepcopy


class ObstacleCluster:
    def __init__(self, obstacles):
        self.name = '_'.join([str(o.id()) for o in obstacles])
        self.obstacles = obstacles
        self.cluster_obstacle = None
        self.kernel_points = None
        self.admissible_kernel = None
        self._polygon = None

    def polygon(self):
        if self.cluster_obstacle is not None:
            self._polygon = self.cluster_obstacle.polygon()
        if self._polygon is None:
            self._polygon = shapely.ops.unary_union([o.polygon() for o in self.obstacles])
        return self._polygon

    def draw(self, ax=None):
        if self.cluster_obstacle is not None:
            ax, _ = self.cluster_obstacle.draw(ax=ax, fc="green")
        for obs in self.obstacles:
            ax, _ = obs.draw(ax=ax)
        return ax, _


def polygon_admissible_kernel(polygon, x):
    polygon_boundary = polygon.polygon().exterior
    vertices = np.asarray(polygon_boundary.coords)[:-1, :]  # Vertices of polygon
    N = vertices.shape[0]

    Phi = [0] * (N + 1)
    r = vertices[0] - x
    phi_prev = np.arctan2(r[1], r[0])
    if phi_prev < 0:
        phi_prev += 2 * np.pi
    phi_prev /= 2 * np.pi
    Phi[0] = phi_prev

    for i in range(1, N+1):
        r = vertices[i % N] - x
        phi = np.arctan2(r[1], r[0])
        if phi < 0:
            phi += 2*np.pi
        phi /= 2*np.pi

        phi_diff = phi - phi_prev
        Phi[i] = Phi[i - 1] + phi_diff
        if phi_diff > 0.5:
            Phi[i] -= 1
        if phi_diff < -0.5:
            Phi[i] += 1

        phi_prev = phi

    if abs(Phi[0] - Phi[-1]) > 0.00001:
        # plt.show()
        # Interior point
        return None
    if (max(Phi) - min(Phi)) >= 1.:
        # plt.show()
        # Blocked exterior point
        return None

    t1 = vertices[np.argmax(Phi) % N]
    t2 = vertices[np.argmin(Phi) % N]
    return RayCone(x, x - t1, x - t2)

    ax, _ = polygon.draw()
    ax.plot(*x, 'o')
    RayCone(x, x - t1, x - t2).draw(ax=ax, alpha=0.2)
    ax.plot(*zip(x, t1), 'k--')
    ax.plot(*zip(x, t2), 'k--')
    plt.show()

    # for i in range(N):
    #
    #     v, vp = vertices[i], vertices[(i + 1) % N]
    #     # Check tangency by ray shooting
    #     xv_normal = (v - x) / np.linalg.norm(v - x)
    #     xv_tangent = np.array([-xv_normal[1], xv_normal[0]])
    #     r1 = ray(x, x, v + 0.0001 * xv_tangent)
    #     r2 = ray(x, x, v - 0.0001 * xv_tangent)
    #     is_tangent = r1.disjoint(polygon_boundary) or r2.disjoint(polygon.polygon())
    #
    #     if is_tangent:
    #         if is_ccw(x, v, vp):
    #             t2 = v
    #         if is_ccw(x, vp, v):
    #             t1 = v
    #     if t1 is not None and t2 is not None:
    #         break
    #
    # if t1 is None or t2 is None:
    #     for i in range(N):
    #         v, vp = vertices[i], vertices[(i + 1) % N]
    #         xv_normal = (v-x) / np.linalg.norm(v-x)
    #         xv_tangent = np.array([-xv_normal[1], xv_normal[0]])
    #         r1 = ray(x, x, v+0.01*xv_tangent)
    #         r2 = ray(x, x, v-0.01*xv_tangent)
    #         check_tangency = r1.disjoint(polygon.polygon()) or r2.disjoint(polygon.polygon())
    #
    #         # ip = ray(x, x, v).intersection(polygon.polygon())
    #         #
    #         # print(ip)
    #         ax, _ = polygon.draw()
    #         ax.plot(*x, 'o')
    #         #
    #         # check_tangency = False
    #         # if ip.geom_type == 'Point':
    #         #     check_tangency = True
    #         # if ip.geom_type == 'MultiPoint':
    #         #     check_tangency = True
    #         #     p0 = np.array([ip.geoms[0].x, ip.geoms[0].y])
    #         #     if not all([is_collinear(x, p0, [p.x, p.y]) for p in ip.geoms]):
    #         #         check_tangency = False
    #
    #         col = 'k' if check_tangency else 'r'
    #         ax.plot(*v, 'x', color=col)
    #         if check_tangency:
    #             # if not line(x, v).intersects(polygon_sh) and not ray(v, x, v).intersects(polygon_sh):
    #             if is_ccw(x, v, vp):
    #                 ax.plot(*v, 'gs')
    #             if is_ccw(x, vp, v):
    #                 ax.plot(*v, 'bs')
    #         plt.show()
    #
    #     return None
    # return RayCone(x, x-t1, x-t2)


def polygon_admissible_kernel_set(polygon, exclude_points, K_bounds=None):
    xs_exclude = exclude_points.reshape((exclude_points.size//2, 2))
    polygon_sh = polygon.polygon()
    bounds = K_bounds if K_bounds else polygon_sh.bounds
    K = shapely.geometry.box(*bounds)
    vertices = np.asarray(polygon_sh.exterior.coords)[:-1, :]  # Vertices of polygon
    N = vertices.shape[0]
    cone_time, intersect_time = 0, 0

    for k, x in enumerate(xs_exclude):
        t1, t2 = None, None
        for i in range(N):
            v, vp = vertices[i], vertices[(i + 1) % N]
            # TODO: More stable check of ray intersection
            ip = ray(x, x, v).intersection(polygon_sh.exterior)
            check_tangency = False
            if ip.geom_type in ['Point','LineString']:
                check_tangency = True
            if ip.geom_type == 'MultiPoint':
                check_tangency = True
                p0 = np.array([ip.geoms[0].x, ip.geoms[0].y])
                for p in ip.geoms:
                    if not np.allclose(np.array([p.x, p.y]), p0):
                        check_tangency = False
                        break
            if check_tangency:
            # if not line(x, v).intersects(polygon_sh) and not ray(v, x, v).intersects(polygon_sh):
                if is_ccw(x, v, vp):
                    t2 = v
                if is_ccw(x, vp, v):
                    t1 = v
        if t1 is None:
            return None

        # ax, _ = polygon.draw()
        # xlim, ylim = ax.get_xlim(), ax.get_ylim()
        # # draw_shapely_polygon(cone_from_rays(ray(x, t1, x), ray(x, t2, x)), ax=ax, color='yellow', alpha=0.6)
        # ax.plot(*t1, 'gs')
        # ax.plot(*t2, 'gd')
        # ax.set_xlim(xlim)
        # ax.set_ylim(ylim)
        # plt.show()
        t0 = time.time()
        C = cone_from_rays(ray(x, t1, x), ray(x, t2, x))
        cone_time += (time.time() - t0) * 1000
        t0 = time.time()
        K = K.intersection(C)
        intersect_time += (time.time() - t0) * 1000

        if K.is_empty:
            return None
    # print("P -- Cone: " + str(cone_time) + ", Intersect: " + str(intersect_time))
    return K


def convex_admissible_kernel(convex_obstacle, x):
    t0 = time.time()
    # Check that not excluding point is inside obstacle
    if convex_obstacle.interior_point(x):
        # ax, _ = convex_obstacle.draw()
        # ax.plot(*x, 'o')
        # plt.show()
        return None

    # Find tangents of obstacle through x
    tp = convex_obstacle.tangent_points(x)
    # print("Conv0: " + str((time.time()-t0)*1000))
    if not tp:
        ax, _ = convex_obstacle.draw()
        ax.plot(*x, 'o')
        plt.show()

    return RayCone(x, x - tp[0], x - tp[1])
    # Order s.t. xt1t2 is CCW
    t1, t2 = tp if is_ccw(x, *tp) else tp[::-1]
    # Create cone from rays

    return RayCone(x, x-t2, x-t1)


def convex_admissible_kernel_set(convex_obstacle, exclude_points, K_bounds=None):
    xs_exclude = exclude_points.reshape((exclude_points.size//2, 2))
    bounds = K_bounds if K_bounds else convex_obstacle.polygon().bounds

    # Check that not excluding point is inside obstacle
    if any([convex_obstacle.interior_point(x) for x in xs_exclude]):
        return None

    K = shapely.geometry.box(*bounds)
    cone_time, intersect_time = 0, 0
    for i, x in enumerate(xs_exclude):
        # Find tangents of obstacle through x
        tp = convex_obstacle.tangent_points(x)
        # Order s.t. xt1t2 is CCW
        t1, t2 = tp if is_ccw(x, *tp) else tp[::-1]
        # Create cone from rays
        t0 = time.time()
        C = cone_from_rays(ray(x, t2, x), ray(x, t1, x))
        cone_time += (time.time()-t0)*1000
        t0 = time.time()
        K = K.intersection(C)
        intersect_time += (time.time()-t0)*1000
        # K = K.intersection(cone_from_rays(ray(x, t2, x), ray(x, t1, x)))
    # print("E -- Cone: " + str(cone_time) + ", Intersect: " + str(intersect_time))
    return K


def convex_local_starshaped_hull(convex_obstacle, kernel_points, id="temp"):
    ndim = np.array(convex_obstacle.polygon().exterior.coords[:]).shape[1]
    kernel_points = kernel_points.reshape((kernel_points.size//ndim, ndim))
    ch_points = kernel_points.copy()
    k_centroid = np.mean(kernel_points, axis=0)
    for k in kernel_points:
        if convex_obstacle.exterior_point(k):
            tp = convex_obstacle.tangent_points(k)
            for p in tp:
                ch_points = np.vstack((ch_points, p))
    if ch_points.size == kernel_points.size:
        hull = convex_obstacle.copy(id)
        hull.set_xr(k_centroid, Frame.GLOBAL)
        return hull
    else:
        ch = ConvexHull(ch_points)
        if ndim == 2:
            # TODO: Fix transformation to local frame
            local_points = convex_obstacle.transform(ch_points[ch.vertices, :], Frame.GLOBAL, Frame.OBSTACLE)
            local_k_centroid = convex_obstacle.transform(k_centroid, Frame.GLOBAL, Frame.OBSTACLE)
            hull_cone = StarshapedPolygon(local_points, xr=local_k_centroid, id="temp", is_convex=True, motion_model=convex_obstacle._motion_model)
            # ax, _ = hull_cone.draw()
            # x0 = np.array([5.5, -3.3])
            # x1 = np.array([6., -4.5])
            # b0 = hull_cone.boundary_point(x0)
            # b1 = hull_cone.boundary_point(x1)
            # n0 = hull_cone.normal(b0, x_is_boundary=True)
            # n1 = hull_cone.normal(b1)
            # ax.plot(*zip(x0, b0, k_centroid), '--o')
            # ax.plot(*zip(x1, b1, k_centroid), '--o')
            # ax.quiver(*b0, *n0)
            # ax.quiver(*b1, *n1)
            # plt.show()
            # hull_cone = StarshapedPolygon(ch_points[ch.vertices, :], xr=k_centroid, id="temp", is_convex=True)
        if ndim == 3:
            hull_cone = Polyhedron(ch_points[ch.vertices, :], xr=k_centroid, id="temp", is_convex=True)
        return StarshapedHullPrimitiveCombination(obstacle_cluster=[convex_obstacle],
                                                      hull_cluster=[convex_obstacle, hull_cone],
                                                      xr=k_centroid, id=id)


def polygon_local_starshaped_hull(polygon, kernel_points, id="temp", debug=0):

    if all([issubclass(polygon.__class__, StarshapedObstacle) and polygon.interior_point(k) for k in kernel_points]):
        hull = polygon.copy(id)
        hull.set_xr(np.mean(kernel_points, axis=0), Frame.GLOBAL)
        return hull

    kernel_points = kernel_points.reshape((kernel_points.size//2, 2))

    if kernel_points.shape[0] > 2:
        convex_kernel_subset = shapely.geometry.Polygon(kernel_points[ConvexHull(kernel_points).vertices, :])
    polygon_sh = polygon.polygon()  # Shapely represenation of polygon
    vertices = np.asarray(polygon_sh.exterior.coords)[:-1, :]  # Vertices of polygon
    star_vertices = []  # Vertices of starshaped hull polygon
    v_bar = kernel_points[0].copy() # Last vertex of starshaped hull polygon
    e1_idx = 0
    e2_idx = 0
    k_centroid = np.mean(kernel_points, axis=0)
    k_included = [False] * kernel_points.shape[0]

    # Arrange vertices such that v_1 is the one with largest x-value and vendv1v2 is CCW, (assumes no collinear vertices in P)
    start_idx = np.argmax(vertices[:, 0])
    vertices = np.roll(vertices, -start_idx, axis=0)
    if is_cw(vertices[-1], vertices[0], vertices[1]):
        vertices = np.flip(vertices, axis=0)
        vertices = np.roll(vertices, 1, axis=0)

    # Iterate through all vertices
    for v_idx, v in enumerate(vertices):
        adjust_e1 = False
        # Check if no ray r(v,kv) intersects with interior of polygon
        if all([ray(v, k, v).disjoint(polygon_sh) for k in kernel_points]):
            # Add current vertex
            if kernel_points.shape[0] < 3 or not convex_kernel_subset.contains(shapely.geometry.Point(v)):
                star_vertices += [v]

            if star_vertices:
                # Intersections of lines l(k,v) and l(e1,e2)
                e1, e2 = star_vertices[e1_idx], star_vertices[e2_idx]
                e1_e2 = line(e1, e2)

                for k in kernel_points:
                    kv_e1e2_intersect = line(k,v).intersection(e1_e2)
                    # Adjust to closest intersection to e2
                    if not kv_e1e2_intersect.is_empty:
                        adjust_e1 = True
                        e1_candidate = np.array([kv_e1e2_intersect.x, kv_e1e2_intersect.y])
                        if np.linalg.norm(e2 - e1_candidate) < np.linalg.norm(e2 - star_vertices[e1_idx]):
                            star_vertices[e1_idx] = e1_candidate
            if not adjust_e1:
                for k_idx, k in enumerate(kernel_points):
                    kps = [kp for kp in kernel_points if not np.array_equal(kp, k)]
                    kv_P_intersect = line(k, v).intersection(polygon_sh)
                    # If l(k,v) intersects interior of P
                    if not kv_P_intersect.is_empty:
                        # Find last intersection of l(k,v) and polygon boundary
                        if kv_P_intersect.geom_type == 'LineString':
                            intersection_points = [np.array([ip[0], ip[1]]) for ip in kv_P_intersect.coords]
                        elif kv_P_intersect.geom_type == 'MultiLineString':
                            intersection_points = [np.array([ip[0], ip[1]]) for l in kv_P_intersect.geoms for ip in
                                                   l.coords]
                        elif kv_P_intersect.geom_type == 'GeometryCollection':
                            intersection_points = []
                            for g in kv_P_intersect.geoms:
                                if g.geom_type == 'Point':
                                    intersection_points += [np.array(g.coords[0])]
                                if kv_P_intersect.geom_type == 'LineString':
                                    intersection_points += [np.array([ip[0], ip[1]]) for ip in g.coords]
                        else:
                            intersection_points = []
                        u = None
                        u_v = None
                        for u_candidate in intersection_points:
                            u_v = line(u_candidate, v)
                            if u_v.disjoint(polygon_sh):
                                u = u_candidate
                                break
                        if u is None:
                            continue

                        # If no ray r(u,k'v) intersect with interior of polygon
                        if not any([ray(u, kp, v).intersects(polygon_sh) for kp in kps]):
                            # Adjust u if l(k',v_bar) intersects l(u,v)
                            for kp in kps:
                                kpvb_uv_intersect = line(kp, v_bar).intersection(u_v)
                                if not kpvb_uv_intersect.is_empty:
                                    u = np.array([kpvb_uv_intersect.x, kpvb_uv_intersect.y])
                            # Append u to P*
                            star_vertices += [u]
                            # Update last augmented edge
                            e1_idx, e2_idx = len(star_vertices)-1, len(star_vertices)-2
                            # Swap last vertices if not CCW
                            if is_ccw(u, v, vertices[v_idx-1]):
                            # if is_ccw(v_bar, v, u):
                            # if is_cw(k_centroid, v, u):
                                star_vertices[-2], star_vertices[-1] = star_vertices[-1], star_vertices[-2]
                                e1_idx, e2_idx = e2_idx, e1_idx
                            adjust_e1 = True
                    else:
                        # Check if no ray r(k,k'v) intersect with interior of polygon
                        if (not k_included[k_idx]) and (not any([ray(k, kp, v).intersects(polygon_sh) for kp in kps])):
                            k_included[k_idx] = True
                            # Append k to P*
                            star_vertices += [k]
                            # Update last augmented edge
                            e1_idx, e2_idx = len(star_vertices)-1, len(star_vertices)-2
                            # Swap last vertices if not CCW
                            if is_ccw(k, v, vertices[v_idx-1]):
                            # if is_cw(k_centroid, v, k):
                                star_vertices[-2], star_vertices[-1] = star_vertices[-1], star_vertices[-2]
                                e1_idx, e2_idx = e2_idx, e1_idx
                            adjust_e1 = True

            # Update v_bar
            v_bar = star_vertices[-1]

            # Visualize debug information
            if debug == 1:
                plt.plot(*k_centroid, 'ko')
                plt.plot(*polygon_sh.exterior.xy, 'k')
                plt.plot([p[0] for p in star_vertices], [p[1] for p in star_vertices], 'g-o', linewidth=2)
                [plt.plot(*k, 'kx') for k in kernel_points]
                [plt.plot(*line(k,v).xy, 'k--') for k in kernel_points]
                if adjust_e1:
                    plt.plot(*star_vertices[e1_idx], 'ys')
                plt.show()

    # Check not added kernel points if they should be included
    for j in range(len(star_vertices)):
        v, vp = star_vertices[j - 1], star_vertices[j]
        for k_idx, k in enumerate(kernel_points):
            if (not k_included[k_idx]) and is_cw(k, v, vp):
                k_included[k_idx] = True
                # Insert k
                star_vertices = star_vertices[:j] + [k] + star_vertices[j:]
                # Visualize debug information
                if debug == 1:
                    plt.plot(*k_centroid, 'ko')
                    plt.plot(*polygon_sh.exterior.xy, 'k')
                    plt.plot([p[0] for p in star_vertices], [p[1] for p in star_vertices], 'g-o', linewidth=2)
                    [plt.plot(*ki, 'kx') for ki in kernel_points]
                    plt.plot(*line(k, v).xy, 'r--*')
                    plt.plot(*line(k, vp).xy, 'r--*')
                    plt.plot(*k, 'go')
                    plt.show()

    if debug:
        ax = plt.gca()
        ax.plot(*polygon_sh.exterior.xy, 'k')
        ax.plot([p[0] for p in star_vertices] + [star_vertices[0][0]],
                [p[1] for p in star_vertices] + [star_vertices[0][1]], 'g-o', linewidth=2)
        # [ax.plot(star_vertices[i][0], star_vertices[i][1], 'r*') for i in augmented_vertex_idcs]
        [ax.plot(*zip(k, sv), 'y--') for sv in star_vertices for k in kernel_points]
        ax.plot(*k_centroid, 'bs')
        plt.show()

    local_vertices = polygon.transform(star_vertices, Frame.GLOBAL, Frame.LOCAL)
    return StarshapedPolygon(local_vertices, xr=k_centroid, id=id, motion_model=polygon._motion_model)
    # return StarshapedPolygon(star_vertices, xr=k_centroid, id=id)


# Note: Local == Global frame
class StarshapedHullPrimitiveCombination(StarshapedObstacle):

    def __init__(self, obstacle_cluster, hull_cluster, xr, **kwargs):
        polygon = shapely.ops.unary_union([obs.polygon() for obs in hull_cluster])
        super().__init__(polygon=polygon, xr=xr, **kwargs)
        self._obstacle_cluster = obstacle_cluster
        # Only include obstacles contributing to the boundary in the hull cluster
        self._hull_cluster = []
        for cl in hull_cluster:
            # if cl.polygon().within(polygon):
            #     fig, ax = plt.subplots()
            #     draw_shapely_polygon(polygon, ax=ax)
            #     cl.draw(ax=ax, fc='yellow')
            #     plt.show()
            self._hull_cluster += [cl]
        self._line_length = abs(self._polygon.bounds[2] - self._polygon.bounds[0]) + abs(self._polygon.bounds[3] - self._polygon.bounds[1])

    def obstacle_cluster(self): return self._obstacle_cluster

    def hull_cluster(self): return self._hull_cluster

    def dilated_obstacle(self, padding, id="new", name=None):
        cp = self.copy(id, name)
        dilated_hull_cluster = []
        for obs in self._hull_cluster:
            dilated_hull_cluster += [obs.dilated_obstacle(padding=padding, id="temp")]
        cp._hull_cluster = dilated_hull_cluster
        cp._polygon = shapely.ops.unary_union([obs.polygon() for obs in cp._hull_cluster])
        return cp

    def point_location(self, x, input_frame=Frame.GLOBAL, state=None):
        locs = []
        for obs in self._hull_cluster:
            locs += [obs.point_location(x)]
        if any([l < 0 for l in locs]):
            return -1
        if all([l > 0 for l in locs]):
            return 1
        return 0

    def line_intersection(self, line, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        intersection_points = []
        for obs in self._hull_cluster:
            intersection_points += obs.line_intersection(line, Frame.GLOBAL, Frame.GLOBAL)
        return intersection_points

    def tangent_points(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        tp, tp_candidates = [], []
        for obs in self._hull_cluster:
            tp_candidates += obs.tangent_points(x, Frame.GLOBAL, Frame.GLOBAL)
        for i in range(len(tp_candidates)):
            if all([is_ccw(x, tp_candidates[i], tp_candidates[j]) for j in range(i+1, len(tp_candidates))]) or \
                    all([is_cw(x, tp_candidates[i], tp_candidates[j]) for j in range(i+1, len(tp_candidates))]):
                tp += [tp_candidates[i]]
        return tp

    def _compute_kernel(self):
        self._kernel = StarshapedPolygon(self.polygon(), xr=self.xr(), id="temp").kernel()

    def _check_convexity(self):
        self._is_convex = StarshapedPolygon(self.polygon(), xr=self.xr(), id="temp").is_convex()

    def boundary_point(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        intersection_points = self.line_intersection([self._xr, self._xr + self._line_length * self.reference_direction(x)])
        dist_intersection_points = [np.linalg.norm(ip - self._xr) for ip in intersection_points]
        return intersection_points[np.argmax(dist_intersection_points)]

    def vel_intertial_frame(self, x):
        b = self.boundary_point(x)#
        # Prioritize original obstacles
        for obs in self._obstacle_cluster:
            if obs.is_starshaped():
                intersection_points = obs.line_intersection([self._xr, self._xr + self._line_length * self.reference_direction(x)])
                for ip in intersection_points:
                    if all(np.isclose(b, ip)):
                        return obs.vel_intertial_frame(x)
        # Otherwise check hull obstacles
        for obs in self._hull_cluster:
            intersection_points = obs.line_intersection([self._xr, self._xr + self._line_length * self.reference_direction(x)])
            for ip in intersection_points:
                if all(np.isclose(b, ip)):
                    return obs.vel_intertial_frame(x)

    def normal(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, x_is_boundary=False, state=None, debug=0):
        b = self.boundary_point(x)

        if debug:
            ax, _ = self.draw()
            ax.plot(*zip(x,self._xr), '--ob')
        for obs in self._hull_cluster:
            intersection_points = obs.line_intersection([self._xr, self._xr + self._line_length * self.reference_direction(x)])
            for ip in intersection_points:
                if debug:
                    ax.plot(*ip, 'xr')
                if all(np.isclose(b, ip)):
                    n = obs.normal(b, x_is_boundary=1)
                    if debug:
                        obs.draw(ax=ax, fc='g')
                        ax.quiver(*b, *n, color='g')
                        # n = obs.normal(b, x_is_boundary=True, debug=debug)
        return n
        # for obs in self._hull_cluster:
        #     if all(np.isclose(b, obs.boundary_point(x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL))):
        #         return obs.normal(x)
        #     print(b, obs.boundary_point(x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL))

        # r0 = self.reference_direction(x)
        # e1 = [-r0[1], r0[0]]
        # Rpn = np.vstack((r0, e1)).T
        # b = np.array([obs.boundary_point(x) for obs in self._hull_cluster])
        # n = np.array([obs.normal(b[i, :], x_is_boundary=True) for i, obs in enumerate(self._hull_cluster)])
        # b_dist = np.linalg.norm(x - b, axis=1)
        # w = -np.ones(len(self._hull_cluster))
        # w[np.nonzero(b_dist)] = 1. / b_dist[np.nonzero(b_dist)]**4
        # nu = Rpn.T.dot(n.T).T
        # nu[nu[:, 0] > 1, 0] = 1
        # kappa = np.arccos(nu[:, 0]) * np.sign(nu[:, 1])
        # # ax, _ = self.draw(alpha=0.6,show_cluster_reference=True, show_reference=False, show_cluster_name=True)
        # # ax.plot(*x, 'o')
        # # ax.plot(*b.T, 'x')
        # # plt.show()
        # if np.any(w < 0):
        #     w[np.nonzero(w > 0)] = 0
        #     w[np.nonzero(w)] = 1
        # w = w / np.sum(w)
        # kappa_bar = w.dot(kappa)
        # n = Rpn.dot([np.cos(abs(kappa_bar)), np.sin(abs(kappa_bar)) * np.sign(kappa_bar)])
        # return n

    def init_plot(self, ax=None, fc='lightgrey', show_reference=True, show_name=False, **kwargs):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        if "show_reference" in kwargs:
            show_reference = kwargs["show_reference"]
        line_handles = []
        # Boundary
        for o in self._hull_cluster:
            lh = o.init_plot(ax, fc, show_reference=False, **kwargs)
            line_handles += [lh[0]]
        # Reference point
        line_handles += ax.plot(0, 0, '+', color='k') if show_reference else [None]
        # Name
        line_handles += [ax.text(0, 0, self._name)] if show_name else [None]
        return line_handles

    def update_plot(self, line_handles, state=None, frame=Frame.GLOBAL):
        for i, obs in enumerate(self.hull_cluster()):
            obs.update_plot([line_handles[i], None, None], state, frame)
        if line_handles[i+1] is not None:
            line_handles[i+1].set_data(*self.xr(frame, state))
        if line_handles[i+2] is not None:
            line_handles[i+2].update_positions(*self.xr(frame, state))


    def draw(self, ax=None, show_reference=True, show_name=False, show_cluster_reference=False, show_cluster_name=False, **kwargs):
        line_handle = []
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        # Boundary
        kwargs['show_name'] = show_cluster_name
        kwargs['show_reference'] = show_cluster_reference
        for obs in self.hull_cluster():
            ax, line_handle_i = obs.draw(ax=ax, **kwargs)
            line_handle += line_handle_i
            ax.plot(*self._xr)
        # Reference point
        if show_reference:
            line_handle += ax.plot(*self._xr, '+', color='k')
        # Name
        if show_name:
            line_handle += [ax.text(*self._xr, self._name)]
        return ax, line_handle


def global_starshaped_hull(obstacle, id="temp"):
    # Check if intersecting kernel
    kernel = obstacle[0].kernel().polygon()
    for o in obstacle[1:]:
        kernel = kernel.intersection(o.kernel().polygon())
    if not kernel.is_empty:
        k = np.array([kernel.centroid.x, kernel.centroid.y])
        return local_starshaped_hull(obstacle, k, id)


def local_starshaped_hull(obstacle, kernel_points, id="temp"):
    kernel_points = kernel_points.reshape((kernel_points.size//2, 2))
    # TODO: Fix for intersecting kernel for faster computation
    # Cluster of obstacles
    if type(obstacle) is list:
        # TODO: Check if it should be removed!
        # If all polygons, perform hull of union of them
        if False and all(issubclass(o.__class__, Polygon) for o in obstacle):
            pol = shapely.ops.unary_union([o.polygon() for o in obstacle])
            fig, ax = plt.subplots()
            draw_shapely_polygon(pol, ax=ax)
            ax.plot(0,0)
            ax.set_xlim(-1, 13)
            ax.set_xlim(-8, 2)
            plt.show()
            return local_starshaped_hull(Polygon(pol), kernel_points, id)
        # Else, create a StarshapedHullPrimitiveCombination
        obs_cluster, hull_cluster = [], []
        for o in obstacle:
            obs_cluster += o.obstacle_cluster() if issubclass(o.__class__, StarshapedHullPrimitiveCombination) else [o]
            o_hull_cluster = local_starshaped_hull(o, kernel_points)
            if isinstance(o_hull_cluster, StarshapedHullPrimitiveCombination):
                hull_cluster += o_hull_cluster.hull_cluster()
            else:
                hull_cluster += [o_hull_cluster]
        k_centroid = np.mean(kernel_points, axis=0)
        star_obs = StarshapedHullPrimitiveCombination(obs_cluster, hull_cluster, xr=k_centroid, id=id)
        return star_obs

    # if issubclass(obstacle.__class__, StarshapedObstacle) and all([obstacle.kernel().interior_point(k) for k in kernel_points]):
    if obstacle.is_convex():
        return convex_local_starshaped_hull(obstacle, kernel_points, id)
    # Polygon
    if issubclass(obstacle.__class__, Polygon):
        return polygon_local_starshaped_hull(obstacle, kernel_points, id)
    # StarshapedHullPrimitiveCombination
    if issubclass(obstacle.__class__, StarshapedHullPrimitiveCombination):
        hull_cluster = []
        for o in obstacle.obstacle_cluster():
            hull_cluster += local_starshaped_hull(o, kernel_points)
        k_centroid = np.mean(kernel_points, axis=0)
        star_obs = StarshapedHullPrimitiveCombination(obstacle.obstacle_cluster(), hull_cluster, xr=k_centroid, id=id)
        return star_obs


def admissible_kernel(obstacle, x):
    # Polygon
    if issubclass(obstacle.__class__, Polygon):
        t0 = time.time()
        adm_ker = polygon_admissible_kernel(obstacle, x)
        # print("Pol: " + str((time.time() - t0) * 1000))
        return adm_ker
    # Convex
    if obstacle.is_convex():
        t0 = time.time()
        adm_ker = convex_admissible_kernel(obstacle, x)
        # print("Conv: " + str((time.time() - t0) * 1000))
        return adm_ker
    # Else use polygon approximation
    t0 = time.time()
    adm_ker = polygon_admissible_kernel(obstacle, x)
    # print("Other: " + str((time.time() - t0) * 1000))
    return adm_ker


def admissible_kernel_set(obstacle, X_exclude, K_bounds):
    # Polygon
    if issubclass(obstacle.__class__, Polygon):
        return polygon_admissible_kernel_set(obstacle, X_exclude, K_bounds)
    # Convex
    if obstacle.is_convex():
        return convex_admissible_kernel_set(obstacle, X_exclude, K_bounds)
    # Else use polygon approximation
    return polygon_admissible_kernel_set(obstacle, X_exclude, K_bounds)


def get_intersection_clusters(clusters):
    No = len(clusters)
    intersection_idcs = []

    # Use polygon approximations for intersection check
    cluster_polygons = [cl.polygon() for cl in clusters]

    # Find intersections
    intersections_exist = False
    for i in range(No):
        intersection_idcs += [[i]]
        for j in range(i + 1, No):
            if cluster_polygons[i].intersects(cluster_polygons[j]):
                intersection_idcs[i] += [j]
                intersections_exist = True

    if not intersections_exist:
        return clusters, intersections_exist

    # Cluster intersecting obstacles
    for i in range(No - 1, 0, -1):
        for j in range(i - 1, -1, -1):
            found = False
            for l_j in intersection_idcs[j]:
                if l_j in intersection_idcs[i]:
                    found = True
                    break
            if found:
                intersection_idcs[j] = list(set(intersection_idcs[j] + intersection_idcs[i]))
                intersection_idcs[i] = []
                break

    # Create obstacle clusters
    cluster_obstacles = [cl.obstacles if isinstance(cl, ObstacleCluster) else [cl] for cl in clusters]
    new_clusters = []
    for i in range(No):
        if intersection_idcs[i]:
            # for j in intersection_idcs[i]:
            #     for o in cluster_obstacles[j]:
            #         print(type(o))
            new_clusters += [ObstacleCluster([o for j in intersection_idcs[i] for o in cluster_obstacles[j]])]

    return new_clusters, intersections_exist


def cluster_and_starify1(obstacles, x, xg, epsilon, previous_clusters=None, make_convex=True, exclude_obstacles=False, debug=False):

    # Debug plotting
    # cl_color = ['lightgrey', 'sienna', 'lightblue', 'cyan']
    from matplotlib.pyplot import cm
    cl_color = cm.rainbow(np.linspace(0, 1, 10))

    def debug_plot_cluster(clusters, fig, gs):
        ax = fig.add_subplot(gs)
        for i, cl in enumerate(clusters):
            for o in cl.obstacles:
                o.draw(ax=ax, fc=cl_color[i], alpha=0.8, show_name=True)
        ax.plot(*x.T, 'bo')
        ax.plot(*xg.T, 'b*')
        ax.tick_params(bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)

    def debug_plot_admker(clusters, fig, gs):
        sgs = gs.subgridspec(len(clusters), 1)
        for i, cl in enumerate(clusters):
            ax = fig.add_subplot(sgs[i])
            for o in cl.obstacles:
                o.draw(ax=ax, fc=cl_color[i], alpha=0.8, show_name=True)
            ax.plot(*x.T, 'bo')
            ax.plot(*xg.T, 'b*')
            xlim, ylim = ax.get_xlim(), ax.get_ylim()
            draw_shapely_polygon(cl.admissible_kernel, ax=ax, fc="y", alpha=0.4)
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)
            ax.tick_params(bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)

    def debug_plot_star(clusters, fig, gs):
        ax = fig.add_subplot(gs)
        for i, cl in enumerate(clusters):
            hull_extra = cl.cluster_obstacle.polygon()
            for o in cl.obstacles:
                hull_extra = hull_extra.difference(o.polygon())
                o.draw(ax=ax, fc=cl_color[i], alpha=0.8, show_reference=False)
            draw_shapely_polygon(hull_extra, ax=ax, fc="green", alpha=0.8)
            cl.cluster_obstacle.draw(ax=ax, show_name=True, fc='None')
            ax.plot(*x.T, 'bo')
            ax.plot(*xg.T, 'b*')
        ax.tick_params(bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)

    def debug_plotter(clusters):
        fig = plt.figure(figsize=(5.5, 3.5), constrained_layout=True)
        gs = fig.add_gridspec(1, 3)
        debug_plot_cluster(clusters, fig, gs[0])
        debug_plot_admker(clusters, fig, gs[1])
        debug_plot_star(clusters, fig, gs[2])
        plt.show()

    inf_val = 1e6
    inf_box = shapely.geometry.Polygon([[-inf_val, -inf_val], [inf_val, -inf_val], [inf_val, inf_val], [-inf_val, inf_val]])

    # Variable initialization
    cluster_time, kernel_time, hull_time = [0.] * 3
    adm_ker_robot_cones = {obs.id(): None for obs in obstacles}
    adm_ker_goal_cones = {obs.id(): None for obs in obstacles}
    adm_ker_obstacles = {obs.id(): {} for obs in obstacles}
    clusters_prev = []


    # TODO: Make sure admissible kernel exists for all obstacles in first iteration
    for o in obstacles:
        t0 = time.time()
        # Find admissible kernel
        adm_ker_robot_cones[o.id()] = admissible_kernel(o, x)
        adm_ker_goal_cones[o.id()] = admissible_kernel(o, xg)

        # Admissible kernel when excluding points of other obstacles
        if exclude_obstacles:
            for o_ex in obstacles:
                if o_ex.id() == o.id():
                    continue
                o_x_exclude = o_ex.extreme_points()
                if not all([o.exterior_point(x_ex) for x_ex in o_x_exclude]):
                    adm_ker_obstacles[o.id()][o_ex.id()] = None
                    continue
                adm_ker_obstacles[o.id()][o_ex.id()] = admissible_kernel(o, o_x_exclude[0]).polygon()
                for v in o_x_exclude[1:]:
                    adm_ker_obstacles[o.id()][o_ex.id()] = adm_ker_obstacles[o.id()][o_ex.id()].intersection(
                        admissible_kernel(o, v).polygon())

        kernel_time += (time.time() - t0) * 1000

    # Initialize
    robot_surrounded = False
    for o in obstacles:
        # Cluster as single obstacles
        cl = ObstacleCluster([o])

        t0 = time.time()
        # Admissible kernel
        cl.admissible_kernel = adm_ker_robot_cones[o.id()].polygon().intersection(adm_ker_goal_cones[o.id()].polygon())
        # Exclude other obstacles
        K_o_ex = cl.admissible_kernel
        if exclude_obstacles:
            for o_ex in obstacles:
                if o_ex.id() == o.id() or adm_ker_obstacles[o.id()][o_ex.id()] is None:
                    continue
                K_o_ex = K_o_ex.intersection(adm_ker_obstacles[o.id()][o_ex.id()])
        if not (K_o_ex.is_empty or K_o_ex.area < 1e-6):
            cl.admissible_kernel = K_o_ex

        #
        # # Robot fully surrounded by obstacle
        # if adm_ker_robot_cones[o.id()] is None:
        #     cl.cluster_obstacle = o
        #     cl.admissible_kernel = o.polygon()
        #     # Add cluster to list
        #     clusters_prev += [cl]
        #     robot_surrounded = True
        #     continue
        # # Goal fully surrounded by obstacle
        # if adm_ker_goal_cones[o.id()] is None:
        #     K = adm_ker_robot_cones[o.id()].polygon()
        # else:
        #     K = adm_ker_robot_cones[o.id()].polygon().intersection(adm_ker_goal_cones[o.id()].polygon())
        #     # If empty kernel, use admissible kernel only excluding robot
        #     if K.is_empty or K.area < 1e-6:
        #         K = adm_ker_robot_cones[o.id()].polygon()
        #     else:
        #         K_o_ex = K
        #         if exclude_obstacles:
        #             for o_ex in obstacles:
        #                 if o_ex.id() == o.id() or adm_ker_obstacles[o.id()][o_ex.id()] is None:
        #                     print(o)
        #                     continue
        #                 K_o_ex = K_o_ex.intersection(adm_ker_obstacles[o.id()][o_ex.id()])
        #         if not (K_o_ex.is_empty or K_o_ex.area < 1e-6):
        #             K = K_o_ex
        # cl.admissible_kernel = K

        kernel_time += (time.time() - t0) * 1000

        t0 = time.time()

        # Do nothing if already starshaped
        if isinstance(o, StarshapedObstacle):
            cl.cluster_obstacle = o
            # TODO: Not hardcoded length of triangle
            cl.kernel_points = equilateral_triangle(o.xr(Frame.GLOBAL), 0.1)
        else:
            # Check if cluster exist in previous cluster
            cl_prev = None
            if previous_clusters:
                for p_cl in previous_clusters:
                    if cl.name == p_cl.name:
                        cl_prev = p_cl

            # If possible, use internal points of cluster as kernel points
            K_cl_intersection = cl.admissible_kernel.intersection(cl.polygon())
            if not K_cl_intersection.is_empty:
                K = K_cl_intersection
            # Use same kernel points as previous time instance for cluster if still in K
            if cl_prev is not None and K.contains(shapely.geometry.Polygon(cl_prev.kernel_points)):
                cl.kernel_points = cl_prev.kernel_points
            else:
                # Use equilateral triangle at centroid of admissible kernel as kernel points
                triangle_center = np.array(K.centroid.coords[0]) if K.contains(K.centroid) else np.array(K.representative_point())
                length = epsilon
                cl.kernel_points = equilateral_triangle(triangle_center, length)
                while not K.contains(shapely.geometry.Polygon(cl.kernel_points)):
                    length = 0.9 * length
                    cl.kernel_points = equilateral_triangle(triangle_center, length)

            # Starshaped hull
            cl.cluster_obstacle = local_starshaped_hull(o, cl.kernel_points, o.id())

        hull_time += (time.time() - t0) * 1000

        # Add cluster to list
        clusters_prev += [cl]

    if debug:
        debug_plotter(clusters_prev)

    # Set cluster history
    cluster_history = {cl.name: cl for cl in clusters_prev}

    # robot_surrounded = False
    # Loop until no intersection exists
    # print("------------")
    # print([type(o) for o in clusters_prev])
    # print([type(o) for o in obstacles])
    while True:
        if robot_surrounded:
            debug_plotter(clusters_prev)
            return clusters_prev, [cluster_time, kernel_time, hull_time, 0]

        # ----------- Clustering ----------- #
        t0 = time.time()

        # -- Cluster star obstacles such that no intersection exists
        clusters, intersection_exists = get_intersection_clusters(clusters_prev)

        cluster_time += (time.time() - t0) * 1000
        # ----------- End Clustering ----------- #

        # Exit loop when no intersection exist
        if not intersection_exists:
            break

        # Find starshaped obstacle representation for each cluster
        for i, cl in enumerate(clusters):
            # If cluster already calculated keep it
            if cl.name in cluster_history:
                clusters[i] = cluster_history[cl.name]
                continue

            # If cluster is a single starshaped obstacle, nothing needs to be done
            # TODO: (REDUNDANT CHECK OF STARSHAPE?)
            if len(cl.obstacles) == 1 and isinstance(cl.obstacles[0], StarshapedObstacle):
                cl.cluster_obstacle = cl.obstacles[0]
                continue

            # ----------- Admissible Kernel ----------- #
            t0 = time.time()

            # -- Find admissible kernel excluding robot
            cl_adm_ker_robot_cones = RayCone.intersection_same_apex([adm_ker_robot_cones[obs.id()] for obs in cl.obstacles], debug_ax=debug)
            # Transform to shapely polygon
            adm_ker_robot = shapely.ops.unary_union([cone.polygon() for cone in cl_adm_ker_robot_cones])
            # Robot fully surrounded by obstacles in cluster
            if adm_ker_robot.is_empty or adm_ker_robot.area < 1e-6:
                # Divide cluster into separate obstacles
                for o in cl.obstacles[1:]:
                    cl_o = ObstacleCluster([o])
                    cl_o.cluster_obstacle = o
                    cl_o.admissible_kernel = adm_ker_robot_cones[o.id()].polygon()
                    clusters += [cl_o]
                o = cl.obstacles[0]
                clusters[i] = ObstacleCluster([o])
                clusters[i].cluster_obstacle = o
                clusters[i].admissible_kernel = adm_ker_robot_cones[o.id()].polygon()
                robot_surrounded = True
                continue

            # -- Find admissible kernel excluding goal
            cl_adm_ker_goal_cones = RayCone.intersection_same_apex([adm_ker_goal_cones[obs.id()] for obs in cl.obstacles])
            # Transform to shapely polygon
            adm_ker_goal = shapely.ops.unary_union([cone.polygon() for cone in cl_adm_ker_goal_cones])

            # -- Admissible kernel excluding both robot and goal
            adm_ker_robot_goal = adm_ker_robot.intersection(adm_ker_goal)

            # -- Assign admissible kernel
            only_exclude_robot = adm_ker_robot_goal.is_empty or adm_ker_robot_goal.area < 1e-6
            cl.admissible_kernel = adm_ker_robot if only_exclude_robot else adm_ker_robot_goal

            kernel_time += (time.time() - t0) * 1000
            # ----------- End Admissible Kernel ----------- #

            # ----------- Starshaped Hull ----------- #
            t0 = time.time()
            # Check if cluster exist in previous cluster
            cl_prev = None
            if previous_clusters:
                for p_cl in previous_clusters:
                    if cl.name == p_cl.name:
                        cl_prev = p_cl

            # -- Kernel points selection
            # If possible, use internal point of any obstacle in the cluster as reference
            kernel_selection_set = cl.admissible_kernel.intersection(cl.polygon())
            if kernel_selection_set.is_empty or kernel_selection_set.area < 1e-6:
                kernel_selection_set = cl.admissible_kernel
            # Use same reference point as previous time instance for cluster if still in kernel_selection_set
            if cl_prev is not None and kernel_selection_set.contains(shapely.geometry.Polygon(cl_prev.kernel_points)):
                cl.kernel_points = cl_prev.kernel_points
            else:
                # Use equilateral triangle as kernel points with centroid in kernel_selection_set centroid
                if kernel_selection_set.contains(kernel_selection_set.centroid):
                    triangle_center = np.array(kernel_selection_set.centroid.coords[0])
                else:
                    triangle_center = np.array(kernel_selection_set.representative_point())
                length = epsilon
                cl.kernel_points = equilateral_triangle(triangle_center, length)
                # If triangle too big, reduce size until fully contained in kernel_selection_set
                while not kernel_selection_set.contains(shapely.geometry.Polygon(cl.kernel_points)):
                    length = 0.9 * length
                    cl.kernel_points = equilateral_triangle(triangle_center, length)

            # -- Compute starshaped hull of cluster
            cl_id = "new" if cl_prev is None else cl_prev.cluster_obstacle.id()
            cl.cluster_obstacle = local_starshaped_hull(cl.obstacles, cl.kernel_points, cl_id)
            hull_time += (time.time() - t0) * 1000
            # ----------- End Starshaped Hull ----------- #

            # -- Add cluster to history
            cluster_history[cl.name] = cl
            clusters_prev = clusters

        # Show debug plots
        if debug:
            debug_plotter(clusters)

    # ----------- Make Convex ----------- #
    # Make convex if no intersection occurs
    t0 = time.time()

    if make_convex:
        for j, cl in enumerate(clusters):
            if not cl.cluster_obstacle.is_convex():
                v = np.array(cl.polygon().exterior.coords)[:-1, :]
                hull = ConvexHull(v)
                conv_obs = StarshapedPolygon(v[hull.vertices, :], xr=cl.cluster_obstacle.xr(Frame.GLOBAL), id=cl.cluster_obstacle.id())
                if all([conv_obs.exterior_point(x_ex) for x_ex in [x, xg]]) and not any(
                        [conv_obs.intersects(clusters[k].cluster_obstacle) for k in range(len(clusters)) if k != j]):
                    clusters[j].cluster_obstacle = conv_obs

    convex_time = (time.time() - t0) * 1000
    # ----------- End Make Convex ----------- #

    # Ensure convergence of SOADS by having xr in l(x,xg)
    for cl in clusters:
        if is_collinear(x, cl.cluster_obstacle.xr(), xg):
            new_xr = point_in_triangle(*cl.kernel_points)
            while is_collinear(x, new_xr, xg):
                new_xr = point_in_triangle(*cl.kernel_points)
            cl.cluster_obstacle.set_xr(new_xr, Frame.GLOBAL)

    return clusters, [cluster_time, kernel_time, hull_time, convex_time]


def cluster_and_starify(obstacles, x, xg, epsilon, previous_clusters=None, make_convex=True, exclude_obstacles=False, debug=False):

    # Debug plotting
    cl_color = ['lightgrey', 'sienna', 'lightblue', 'cyan', 'yellow']
    # from matplotlib.pyplot import cm
    # cl_color = cm.rainbow(np.linspace(0, 1, 10))

    def convex_decomposition(obstacles):
        from py2d.Math import Polygon as py2dPol
        conv_obstacles = []
        for o in obstacles:
            if o.is_convex():
                conv_obstacles += [o]
            else:
                pol = py2dPol.from_tuples(np.asarray(o.polygon().exterior.coords)[:-1, :])
                conv_pols = py2dPol.convex_decompose(pol)
                for cp in conv_pols:
                    conv_obstacles += [StarshapedPolygon([(v.x, v.y) for v in cp.points])]
        clusters = []
        for o in conv_obstacles:
            cl = ObstacleCluster([o])
            cl.cluster_obstacle = o
            clusters += [cl]
        return clusters

    def debug_plot_cluster(clusters, fig, gs):
        ax = fig.add_subplot(gs)
        for i, cl in enumerate(clusters):
            for o in cl.obstacles:
                o.draw(ax=ax, fc=cl_color[i], alpha=0.8, show_name=True)
        ax.plot(*x.T, 'bo')
        ax.plot(*xg.T, 'b*')
        ax.tick_params(bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)

    def debug_plot_admker(clusters, fig, gs):
        sgs = gs.subgridspec(len(clusters), 1)
        for i, cl in enumerate(clusters):
            ax = fig.add_subplot(sgs[i])
            for o in cl.obstacles:
                o.draw(ax=ax, fc=cl_color[i], alpha=0.8, show_name=True)
            ax.plot(*x.T, 'bo')
            ax.plot(*xg.T, 'b*')
            xlim, ylim = ax.get_xlim(), ax.get_ylim()
            if cl.admissible_kernel is not None:
                draw_shapely_polygon(cl.admissible_kernel, ax=ax, fc="y", alpha=0.4)
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)
            ax.tick_params(bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)

    def debug_plot_star(clusters, fig, gs):
        ax = fig.add_subplot(gs)
        for i, cl in enumerate(clusters):
            hull_extra = cl.cluster_obstacle.polygon()
            for o in cl.obstacles:
                hull_extra = hull_extra.difference(o.polygon())
                o.draw(ax=ax, fc=cl_color[i], alpha=0.8, show_reference=False)
            draw_shapely_polygon(hull_extra, ax=ax, fc="green", alpha=0.8)
            cl.cluster_obstacle.draw(ax=ax, show_name=True, fc='None')
            ax.plot(*x.T, 'bo')
            ax.plot(*xg.T, 'b*')
            if cl.kernel_points is not None:
                [ax.plot(*xk, 'y^') for xk in cl.kernel_points]
        ax.tick_params(bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)

    def debug_plotter(clusters):
        fig = plt.figure(figsize=(5.5, 3.5), constrained_layout=True)
        gs = fig.add_gridspec(1, 3)
        debug_plot_cluster(clusters, fig, gs[0])
        debug_plot_admker(clusters, fig, gs[1])
        debug_plot_star(clusters, fig, gs[2])
        plt.show()

    # Variable initialization
    cluster_time, kernel_time, hull_time = [0.] * 3
    adm_ker_robot_cones = {obs.id(): None for obs in obstacles}
    adm_ker_goal_cones = {obs.id(): None for obs in obstacles}
    adm_ker_obstacles = {obs.id(): {} for obs in obstacles}
    clusters = []

    for o in obstacles:
        t0 = time.time()
        # Find admissible kernel
        adm_ker_robot_cones[o.id()] = admissible_kernel(o, x)
        adm_ker_goal_cones[o.id()] = admissible_kernel(o, xg)
        if adm_ker_robot_cones[o.id()] is None:
            print("Robot position is not a free exterior point of obstacle " + str(o.id()))
            fig, ax = plt.subplots()
            [o.draw(ax=ax) for o in obstacles]
            ax.plot(*x, 'ok')
            plt.show()
            return convex_decomposition(obstacles), [cluster_time, kernel_time, hull_time, 0]
            # raise RuntimeError("Robot position is not a free exterior point of obstacle " + str(o.id()))
        if adm_ker_goal_cones[o.id()] is None:
            print("Goal position is not a free exterior point of obstacle " + str(o.id()))
            fig, ax = plt.subplots()
            [o.draw(ax=ax) for o in obstacles]
            ax.plot(*xg, 'ok')
            plt.show()
            return convex_decomposition(obstacles), [cluster_time, kernel_time, hull_time, 0]
            # raise RuntimeError("Goal position is not a free exterior point of obstacle " + str(o.id()))

        # Admissible kernel when excluding points of other obstacles
        if exclude_obstacles:
            for o_ex in obstacles:
                if o_ex.id() == o.id():
                    continue
                o_x_exclude = o_ex.extreme_points()
                if not all([o.exterior_point(x_ex) for x_ex in o_x_exclude]):
                    adm_ker_obstacles[o.id()][o_ex.id()] = None
                    continue
                adm_ker_obstacles[o.id()][o_ex.id()] = admissible_kernel(o, o_x_exclude[0]).polygon()
                for v in o_x_exclude[1:]:
                    adm_ker_obstacles[o.id()][o_ex.id()] = adm_ker_obstacles[o.id()][o_ex.id()].intersection(
                        admissible_kernel(o, v).polygon())

        kernel_time += (time.time() - t0) * 1000

    # Initialize clusters as single obstacles
    for o in obstacles:
        cl = ObstacleCluster([o])
        cl.cluster_obstacle = o
        clusters += [cl]

    # Set cluster history
    cluster_history = {}

    intersection_exists = True
    while intersection_exists:
        # Find starshaped obstacle representation for each cluster
        for i, cl in enumerate(clusters):
            # If cluster already calculated keep it
            if cl.name in cluster_history:
                clusters[i] = cluster_history[cl.name]
                continue

            # ----------- Admissible Kernel ----------- #
            t0 = time.time()

            cl.admissible_kernel = adm_ker_robot_cones[cl.obstacles[0].id()].polygon()
            cl.admissible_kernel = cl.admissible_kernel.intersection(adm_ker_goal_cones[cl.obstacles[0].id()].polygon())
            for o in cl.obstacles[1:]:
                cl.admissible_kernel = cl.admissible_kernel.intersection(adm_ker_robot_cones[o.id()].polygon())
                cl.admissible_kernel = cl.admissible_kernel.intersection(adm_ker_goal_cones[o.id()].polygon())
            if cl.admissible_kernel.is_empty or cl.admissible_kernel.area < 1e-6:
                print("Could not find disjoint starshaped obstacles. Some obstacles intersect.")
                return convex_decomposition(obstacles), [cluster_time, kernel_time, hull_time, 0]
                # return clusters_prev, [cluster_time, kernel_time, hull_time, 0]

            # Exclude other obstacles
            adm_ker_o_ex = cl.admissible_kernel
            for o in cl.obstacles:
                if exclude_obstacles:
                    for o_ex in obstacles:
                        if o_ex.id() == o.id() or adm_ker_obstacles[o.id()][o_ex.id()] is None:
                            continue
                        adm_ker_o_ex = adm_ker_o_ex.intersection(adm_ker_obstacles[o.id()][o_ex.id()])
                if not (adm_ker_o_ex.is_empty or adm_ker_o_ex.area < 1e-6):
                    cl.admissible_kernel = adm_ker_o_ex

            kernel_time += (time.time() - t0) * 1000
            # ----------- End Admissible Kernel ----------- #

            # ----------- Starshaped Hull ----------- #
            t0 = time.time()
            # Check if cluster exist in previous cluster
            cl_prev = None
            if previous_clusters:
                for p_cl in previous_clusters:
                    if cl.name == p_cl.name:
                        cl_prev = p_cl

            # -- Kernel points selection
            if all([issubclass(o.__class__, StarshapedObstacle) for o in cl.obstacles]):
                kernel_intersection = cl.obstacles[0].kernel().polygon()
                for o in cl.obstacles[1:]:
                    kernel_intersection = kernel_intersection.intersection(o.kernel().polygon())
            else:
                kernel_intersection = cl.polygon()
            # Select within kernel intersection if nonempty
            kernel_selection_set = kernel_intersection.intersection(cl.admissible_kernel)
            # Else, select within the cluster
            if kernel_selection_set.is_empty or kernel_selection_set.area < 1e-3:
                kernel_selection_set = cl.polygon().intersection(cl.admissible_kernel)
            # Otherwise, select within whole admissible kernel
            if kernel_selection_set.is_empty or kernel_selection_set.area < 1e-3:
                kernel_selection_set = cl.admissible_kernel

            kernel_selection_set = kernel_selection_set.buffer(-0.001)
            # cl.admissible_kernel = kernel_selection_set

            # Use same reference point as previous time instance for cluster if still in kernel_selection_set
            if len(cl.obstacles) > 1 and cl_prev is not None:
                kernel_prev = shapely.geometry.Polygon(cl_prev.kernel_points)
            if len(cl.obstacles) > 1 and cl_prev is not None and kernel_prev.area > np.sqrt(3)/4*(epsilon/2)**2 and kernel_selection_set.contains(kernel_prev):
                triangle_center = np.mean(cl_prev.kernel_points, axis=0)
                cl.kernel_points = cl_prev.kernel_points
            else:
                if kernel_selection_set.contains(kernel_selection_set.centroid):
                    triangle_center = np.array(kernel_selection_set.centroid.coords[0])
                else:
                    triangle_center = np.array(kernel_selection_set.representative_point())
                length = epsilon
                if triangle_center.shape[0] < 1:
                    plt.show()
                cl.kernel_points = equilateral_triangle(triangle_center, length)
                # If triangle too big, reduce size until fully contained in kernel_selection_set
                while not kernel_selection_set.contains(shapely.geometry.Polygon(cl.kernel_points)):
                    length = 0.9 * length
                    cl.kernel_points = equilateral_triangle(triangle_center, length)

            # -- Compute starshaped hull of cluster
            cl_id = "new" if cl_prev is None else cl_prev.cluster_obstacle.id()
            cl.cluster_obstacle = local_starshaped_hull(cl.obstacles, cl.kernel_points, cl_id)
            hull_time += (time.time() - t0) * 1000
            # ----------- End Starshaped Hull ----------- #

            # -- Add cluster to history
            cluster_history[cl.name] = cl

        # fig, axs = plt.subplots(2, 1)
        # for cl in clusters:
        #     print(cl.name)
        # [cl.cluster_obstacle.draw(ax=axs[0], ec='k', fc='lightgrey', show_name=0) for cl in clusters_prev]
        # [axs[0].text(*(o.xr() + o.pos()), str(i)) for i, cl in enumerate(clusters_prev) for o in cl.obstacles]
        # [cl.cluster_obstacle.draw(ax=axs[1], ec='k', fc='lightgrey', show_name=0) for cl in clusters]
        # [axs[1].text(*(o.xr() + o.pos()), str(i)) for i, cl in enumerate(clusters) for o in cl.obstacles]
        # plt.show()

        clusters_prev = clusters

        # Show debug plots
        if debug:
            debug_plotter(clusters)

        # ----------- Clustering ----------- #
        t0 = time.time()

        # -- Cluster star obstacles such that no intersection exists
        clusters, intersection_exists = get_intersection_clusters(clusters)

        cluster_time += (time.time() - t0) * 1000
        # ----------- End Clustering ----------- #


    # ----------- Make Convex ----------- #
    # Make convex if no intersection occurs
    t0 = time.time()

    if make_convex:
        for j, cl in enumerate(clusters):
            if not cl.cluster_obstacle.is_convex():
                v = np.array(cl.polygon().exterior.coords)[:-1, :]
                hull = ConvexHull(v)
                conv_obs = StarshapedPolygon(v[hull.vertices, :], xr=cl.cluster_obstacle.xr(Frame.GLOBAL), id=cl.cluster_obstacle.id())
                if all([conv_obs.exterior_point(x_ex) for x_ex in [x, xg]]) and not any(
                        [conv_obs.intersects(clusters[k].cluster_obstacle) for k in range(len(clusters)) if k != j]):
                    # clusters[j].cluster_obstacle = StarshapedHullPrimitiveCombination(cl.obstacles, cl.obstacles + [conv_obs], cl.cluster_obstacle.xr(Frame.GLOBAL))
                    clusters[j].cluster_obstacle = StarshapedHullPrimitiveCombination(cl.obstacles, [conv_obs], cl.cluster_obstacle.xr(Frame.GLOBAL))
                    # clusters[j].cluster_obstacle = conv_obs

    convex_time = (time.time() - t0) * 1000
    # ----------- End Make Convex ----------- #

    # Ensure convergence of SOADS by having xr in l(x,xg)
    for cl in clusters:
        if is_collinear(x, cl.cluster_obstacle.xr(), xg):
            new_xr = point_in_triangle(*cl.kernel_points)
            while is_collinear(x, new_xr, xg):
                new_xr = point_in_triangle(*cl.kernel_points)
            cl.cluster_obstacle.set_xr(new_xr, Frame.GLOBAL)

    return clusters, [cluster_time, kernel_time, hull_time, convex_time]