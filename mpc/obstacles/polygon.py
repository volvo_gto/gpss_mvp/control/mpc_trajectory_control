from mpc.obstacles import Obstacle, Frame, StarshapedObstacle, is_cw, is_ccw, is_collinear, orientation_val, get_intersection, is_between
import shapely.geometry
import shapely.ops
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


class Polygon(Obstacle):

    def __init__(self, polygon, **kwargs):
        # pol = shapely.geometry.Polygon(polygon)
        # x0 = np.array([pol.centroid.x, pol.centroid.y])
        # pol = shapely.affinity.translate(pol, *-x0)
        super().__init__(polygon=polygon, **kwargs)

    def extreme_points(self, frame=Frame.GLOBAL, state=None):
        vertices = np.asarray(self.polygon(frame, state=state).exterior.coords)[:-1, :]
        return [vertices[i] for i in range(vertices.shape[0])]

    def init_plot(self, ax=None, fc='lightgrey', show_reference=False, show_name=False, **kwargs):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        if "show_reference" in kwargs:
            show_reference = kwargs["show_reference"]
        line_handles = []
        # Boundary
        line_handles += [patches.Polygon(xy=[[0,0], [0,1], [1,0]], fc=fc, **kwargs)]
        ax.add_patch(line_handles[-1])
        # Reference point
        line_handles += [None]
        # Name
        line_handles += [ax.text(0, 0, self._name)] if show_name else [None]
        return line_handles

    def update_plot(self, line_handles, state=None, frame=Frame.GLOBAL):
        polygon = self.polygon(frame, state=state)
        boundary = np.vstack((polygon.exterior.xy[0], polygon.exterior.xy[1])).T
        line_handles[0].set_xy(boundary)
        if line_handles[2] is not None:
            line_handles[2].update_positions(*self.pos(frame, state))

    def draw(self, ax=None, line_handles=None, state=None, fc='darkred', show_name=False, show_reference=False, frame=Frame.GLOBAL, **kwargs):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        polygon = self.polygon(frame, state=state)
        boundary = np.vstack((polygon.exterior.xy[0], polygon.exterior.xy[1])).T
        if line_handles:
            line_handles[0].set_xy(boundary)
            if show_name:
                line_handles[1].update_positions(*self.pos(frame, state))
        else:
            line_handles = []
            # Boundary
            line_handles += [patches.Polygon(xy=boundary, fc=fc, **kwargs)]
            ax.add_patch(line_handles[-1])
            # ax.plot(polygon.centroid.x, polygon.centroid.y)
            # Name
            if show_name:
                line_handles += [ax.text(*self.pos(frame, state), self._name)]
        return ax, line_handles

    def dilated_obstacle(self, padding, id="new", name=None):
        cp = self.copy(id, name)
        cp._polygon = cp._polygon.buffer(padding, cap_style=3, join_style=3)
        return cp

    def point_location(self, x, input_frame=Frame.GLOBAL, state=None):
        x_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state)
        x_sh = shapely.geometry.Point(x_obstacle)
        if self._polygon.contains(x_sh):
            return -1
        if self._polygon.exterior.contains(x_sh):
            return 0
        return 1

    # TODO: Add treatment of additional geom_type (e.g. multipoint+mulilinestring)
    def line_intersection(self, line, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        l0_obstacle = self.transform(line[0], input_frame, Frame.OBSTACLE, state)
        l1_obstacle = self.transform(line[1], input_frame, Frame.OBSTACLE, state)
        intersection_points_shapely = shapely.geometry.LineString([l0_obstacle, l1_obstacle]).intersection(self._polygon.exterior)
        if intersection_points_shapely.is_empty:
            return []
        if intersection_points_shapely.geom_type == 'Point':
            intersection_points_obstacle = [np.array([intersection_points_shapely.x, intersection_points_shapely.y])]
        elif intersection_points_shapely.geom_type == 'MultiPoint':
            intersection_points_obstacle = [np.array([p.x, p.y]) for p in intersection_points_shapely.geoms]
        elif intersection_points_shapely.geom_type == 'LineString':
            intersection_points_obstacle = [np.array([ip[0], ip[1]]) for ip in intersection_points_shapely.coords]
        elif intersection_points_shapely.geom_type == 'MultiLineString':
            intersection_points_obstacle = [np.array([ip[0], ip[1]]) for line in intersection_points_shapely.geoms for ip in line.coords]
        else:
            print(intersection_points_shapely)
        return [self.transform(ip, Frame.OBSTACLE, output_frame, state) for ip in intersection_points_obstacle]

    def tangent_points(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        x_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state)
        vertices = np.asarray(self._polygon.exterior.coords)[:-1, :]
        N = vertices.shape[0]
        Phi = [0] * (N + 1)
        # First vertex
        # r = vertices[0] - x
        # phi_prev = np.arctan2(r[1], r[0])
        # if phi_prev < 0:
        #     phi_prev += 2 * np.pi
        # phi_prev /= 2 * np.pi
        # Phi[0] = phi_prev
        # phi_prev = 0
        for i in range(0, N + 1):
            r = vertices[i % N] - x
            phi = np.arctan2(r[1], r[0])
            if phi < 0:
                phi += 2 * np.pi
            phi /= 2 * np.pi

            if i == 0:
                Phi[i] = phi
            else:
                phi_diff = phi - phi_prev
                Phi[i] = Phi[i - 1] + phi_diff
                if phi_diff > 0.5:
                    Phi[i] -= 1
                if phi_diff < -0.5:
                    Phi[i] += 1

            phi_prev = phi

        if abs(Phi[0] - Phi[-1]) > 0.00001:
            # plt.show()
            # Interior point
            return []
        if (max(Phi) - min(Phi)) >= 1.:
            # plt.show()
            # Blocked exterior point
            return []

        t1 = vertices[np.argmax(Phi) % N]
        t2 = vertices[np.argmin(Phi) % N]
        return [t1, t2]

        # tp = []
        # for v in vertices:
        #     l1 = shapely.geometry.LineString([x_obstacle + .0001 * (v - x_obstacle), x_obstacle + 0.999 * (v - x_obstacle)])
        #     l2 = shapely.geometry.LineString([v + .0001 * (v - x_obstacle), v + 100 * (v - x_obstacle)])
        #     if (l1.disjoint(self._polygon) or l1.touches(self._polygon)) and l2.disjoint(self._polygon):
        #         tp += [self.transform(v, Frame.OBSTACLE, output_frame, state)]
        #     if len(tp) == 2:
        #         break
        # return tp

    def _check_convexity(self):
        v = np.asarray(self._polygon.exterior.coords)[:-1, :]
        i = 0
        N = v.shape[0]
        # Make sure first vertice is not collinear
        while is_collinear(v[i-1, :], v[i, :], v[(i+1) % N, :]):
            i += 1
            if i > N:
                raise RuntimeError("Bad polygon shape. All vertices collinear")
        # All vertices must be either cw or ccw when iterating through for convexity
        if is_cw(v[i-1, :], v[i, :], v[i+1, :]):
            self._is_convex = not any([is_ccw(v[j-1, :], v[j, :], v[(j+1) % N, :]) for j in range(v.shape[0])])
        else:
            self._is_convex = not any([is_cw(v[j-1, :], v[j, :], v[(j+1) % N, :]) for j in range(v.shape[0])])


class StarshapedPolygon(StarshapedObstacle, Polygon):
    def __init__(self, polygon, xr=None, **kwargs):
        super().__init__(polygon=polygon, **kwargs)
        if xr is None:
            self._compute_kernel()
            k_centroid = np.array(self._kernel._polygon.centroid.coords[0])
            if not self._kernel.interior_point(k_centroid):
                k_centroid = np.array(self._kernel._polygon.representative_point().coords[0])
            self._xr = self.transform(k_centroid, Frame.OBSTACLE, Frame.LOCAL)
        else:
            self._xr = np.array(xr)
        self._line_length = 100# abs(self._polygon.bounds[2] - self._polygon.bounds[0]) + abs(self._polygon.bounds[3] - self._polygon.bounds[1])

    def point_location(self, x, input_frame=Frame.GLOBAL, state=None):
        x_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state)
        x_sh = shapely.geometry.Point(x_obstacle)
        if self._polygon.contains(x_sh):
            return -1
        if self._polygon.exterior.contains(x_sh):
            return 0
        return 1

    def boundary_point(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        l0 = self._xr
        l1 = self._xr + self._line_length * self.reference_direction(x, input_frame, Frame.LOCAL, state)
        return self.line_intersection([l0, l1], Frame.LOCAL, output_frame, state)[0]

    # TODO: Add handling of x_is_boundary
    def normal(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, x_is_boundary=False, state=None):
        b = self.transform(x, input_frame, Frame.LOCAL) if x_is_boundary else self.boundary_point(x, input_frame,
                                                                                                  Frame.LOCAL)
        pol = shapely.ops.orient(self.polygon(Frame.LOCAL))
        vertices = np.array(pol.exterior.coords[:-1])
        n = None
        for i in range(vertices.shape[0]):
            if is_between(vertices[i-1], b, vertices[i]):
                n = np.array([vertices[i, 1] - vertices[i-1, 1], vertices[i-1, 0] - vertices[i, 0]])

        if n is None:
            ax, _ = self.draw()
            for i in range(vertices.shape[0]):
                ax.plot(*vertices[i], 'k+')
            ax.plot(*vertices[0], 'g+')
            ax.plot(*vertices[-1], 'y+')
            ax.plot(*x, 'o')
            plt.show()
        #
        # edges = vertices - np.roll(vertices, -1, axis=0)
        # edge_normals = np.vstack((-edges[:, 1], edges[:, 0]))
        # edge_normals = edge_normals / np.linalg.norm(edge_normals, axis=0)
        #
        # vertex_angles = np.array([np.arctan2(v[1] - self._xr[1], v[0] - self._xr[0]) for v in vertices]).flatten()
        # idcs = np.argsort(vertex_angles)
        # vertex_angles = vertex_angles[idcs]
        # vertices = vertices[idcs, :]
        # edge_normals = edge_normals[:, idcs]
        # ang = np.arctan2(r0[1], r0[0])
        # v_idx = np.argmax(vertex_angles > ang)
        # n = edge_normals[:, v_idx-1]

        # if debug:
        #     ax, _ = self.draw(frame=Frame.LOCAL)
        #     # for i, v in enumerate(vertices):
        #     #     ax.quiver(*v, *edge_normals[:, i], color='k')
        #     for i in range(vertices.shape[0]):
        #         edge_normal = [vertices[i, 1] - vertices[i-1, 1], vertices[i-1, 0] - vertices[i, 0]]
        #         ax.quiver(*vertices[i, :], *edge_normal, color='k')
        #     # l0 = self._xr
        #     # l1 = self._xr + self._line_length * self.reference_direction(x, input_frame, Frame.LOCAL)
        #     # ax.plot(*x_local, 'ko')
        #     ax.plot(*b, 'go')
        #     # ax.plot(*zip(l0, l1), 'b--')
        #     ax.quiver(*b, *n, color='g')
        #     plt.show()

        return self.rotate(n, input_frame=Frame.LOCAL, output_frame=output_frame, state=state)

        ax, _ = self.draw(frame=Frame.LOCAL)
        b = self.boundary_point(x, input_frame, Frame.LOCAL)
        ax.quiver(*b, *n)
        for i in range(vertices.shape[0]):
            ax.plot(*vertices[i, :], 'ko')
            ax.quiver(*vertices[i, :], *edge_normals[:, i])
        plt.show()


        ang = np.arctan2(r0[1], r0[0])
        vertex_angles = [np.arctan2(v[1], v[0]) for v in vertices]
        v_idx = np.argmax(vertex_angles>ang)
        print(ang)
        print(vertex_angles)
        edge_dir = np.array(vertices[v_idx]) - np.array(vertices[v_idx-1])
        n = np.array([-edge_dir[1], edge_dir[0]])
        n = n / np.linalg.norm(n)
        ax, _ = self.draw(frame=Frame.LOCAL)
        ax.plot(*self.transform(x, Frame.GLOBAL, Frame.LOCAL), 'o')
        ax.plot(*vertices[v_idx], 's')
        ax.plot(*vertices[v_idx-1], 'd')
        return n

        surfaces = [np.array([polygon_boundary[i - 1], polygon_boundary[i]]) for i in range(n)]


        ws = np.zeros(n)
        kappa = np.zeros(n)
        for i in range(n):
            surface_i = np.array([polygon_boundary[i - 1], polygon_boundary[i]])
            edge_normal_i = np.array([-(surface_i[0, 1] - surface_i[1, 1]), surface_i[0, 0] - surface_i[1, 0]])
            edge_normal_i = edge_normal_i / np.linalg.norm(edge_normal_i)
            rotated_edge_normal_i = Rpn.T.dot(edge_normal_i)

            pi = np.array(
                shapely.ops.nearest_points(shapely.geometry.LineString(surface_i), shapely.geometry.Point(x))[0].coords[
                    0])

            if np.all(np.isclose(pi, x)):
                ws[i] = -1
            else:
                ws[i] = 1 / np.linalg.norm(x - pi) ** 2

            kappa[i] = 0 if rotated_edge_normal_i[0] == 1 else np.arccos(rotated_edge_normal_i[0]) * np.sign(
                rotated_edge_normal_i[1])

        if np.any(ws < 0):
            ws[np.nonzero(ws > 0)] = 0
            ws[np.nonzero(ws)] = 1

        ws = ws / np.sum(ws)
        kappa_bar = ws.dot(kappa)
        tmp_vec = [np.cos(abs(kappa_bar)), np.sin(abs(kappa_bar)) * np.sign(kappa_bar)]
        n = Rpn.dot(tmp_vec)
        self.rotate(n, Frame.LOCAL, output_frame, state)
        return n

    def _compute_kernel(self, verbose=False, debug=False):
        if self.is_convex():
            self._kernel = self.copy(id="temporary")
            return
        # Returns true if vertex v[i, :] is reflex
        def is_reflex(i):
            return is_cw(v[i - 1, :], v[i, :], v[(i + 1) % v.shape[0], :])

        # Show polygon
        def draw(xk, F1_idx, L1_idx, i, xk_bounded):
            v_ext = np.vstack((v, v[0, :]))
            xk_ext = np.vstack((xk, xk[0, :])) if xk_bounded else xk
            plt.plot(v_ext[:, 0], v_ext[:, 1], label='v', marker='.')
            # plt.plot(v_ext[:, 0], v_ext[:, 1], label='v')
            # plt.text(v_ext[0, 0], v_ext[0, 1], 'v0')
            plt.text(xk[0, 0], xk[0, 1], 'xk0')
            axes = plt.gca()
            xlim, ylim = axes.get_xlim(), axes.get_ylim()
            plt.plot(xk_ext[:, 0], xk_ext[:, 1], label='xk')
            plt.plot(xk[F1_idx, 0], xk[F1_idx, 1], marker='o', c='c', label='F1')
            end_point = v[i, :] + INF_VAL * (xk[F1_idx, :] - v[i, :]) / np.linalg.norm(xk[F1_idx, :] - v[i, :])
            plt.plot([v[i, 0], end_point[0]], [v[i, 1], end_point[1]], 'c--')
            plt.plot(xk[L1_idx, 0], xk[L1_idx, 1], marker='o', c='b', label='L1')
            end_point = v[i, :] + INF_VAL * (xk[L1_idx, :] - v[i, :]) / np.linalg.norm(xk[L1_idx, :] - v[i, :])
            plt.plot([v[i, 0], end_point[0]], [v[i, 1], end_point[1]], 'b--')
            plt.plot(v[i, 0], v[i, 1], marker='x', c='r', ms=12, label='v{}'.format(i))
            axes.set_xlim(xlim)
            axes.set_ylim(ylim)

        def point_left_of_line(point, line_head, line_tail):
            return is_ccw(line_head, line_tail, point)

        def point_right_of_line(point, line_head, line_tail):
            return is_cw(line_head, line_tail, point)

        pol = shapely.ops.orient(self._polygon)
        v = np.asarray(pol.exterior.coords)[:-1, :]

        # Remove points in line
        v_idcs = np.arange(v.shape[0])
        for i in range(v.shape[0]):
            if is_collinear(v[v_idcs[i - 2], :], v[v_idcs[i - 1], :], v[v_idcs[i], :]):
                v_idcs[i - 1] = v_idcs[i - 2]
        v_idcs = np.unique(v_idcs)
        v = v[v_idcs, :]
        if is_collinear(v[-2, :], v[-1, :], v[0, :]):
            v = v[:-1]

        N = v.shape[0]
        for i in range(N):
            # Order v to have v[:, 0] as reflex vertex
            if is_reflex(i):
                v = np.vstack((v[i:, :], v[:i, :]))
                break

        INF_VAL = 300.
        # Initial step
        F1 = v[0, :] + INF_VAL * (v[0, :] - v[1, :]) / np.linalg.norm(v[0, :] - v[1, :])
        L1 = v[0, :] + INF_VAL * (v[0, :] - v[-1, :]) / np.linalg.norm(v[0, :] - v[-1, :])
        xk = np.vstack((F1, v[0, :], L1))
        F1_idx = 0  # Index of point F1 in xk
        L1_idx = 2  # Index of point L1 in xk
        xk_bounded = False

        if verbose:
            print("------")
            print(0)
            print("F1:", end=" ")
            print(F1)
            print("L1:", end=" ")
            print(L1)
        if debug:
            draw(xk, F1_idx, L1_idx, 0, xk_bounded)
            plt.show()

        for i in range(1, N):

            if verbose:
                print("------")
                print(i)
                print("F1 index: {}".format(F1_idx))
                print("L1 index: {}".format(L1_idx))

            L1 = xk[L1_idx, :]
            F1 = xk[F1_idx, :]
            vi = v[i, :]
            vi_1 = v[(i + 1) % N, :]

            # (1) vi is reflex
            if is_reflex(i):
                # (1.2) F1 lies to the left of line ->(vi->vi+1)->vi+1
                start_point = vi - INF_VAL * (vi_1 - vi) / np.linalg.norm(vi_1 - vi)
                if point_left_of_line(F1, start_point, vi_1):
                    case = 2
                    if debug:
                        draw(xk, F1_idx, L1_idx, i, xk_bounded)
                        plt.plot([start_point[0], vi_1[0]], [start_point[1], vi_1[1]], marker='*', color='y',
                                 label="->(v{}->v{})->v{}".format(i, i + 1, i + 1))
                        plt.title("F1 lies to the left of line ->(v{}->v{})->v{}".format(i, i + 1, i + 1))
                        plt.show()
                    if verbose:
                        print("(1.2) - F1 ({},{}) lies to the left of line ->(v{}->v{})->v{}".format(F1[0], F1[0], i,
                                                                                                     i + 1, i + 1))
                # (1.1) F1 lies on or to the right of line ->(vi->vi+1)->vi+1
                else:
                    case = 1
                    if verbose:
                        print(
                            "(1.1) - F1 ({},{}) lies on or to the right of line ->(v{}->v{})->v{}".format(F1[0], F1[0],
                                                                                                          i, i + 1,
                                                                                                          i + 1))
                        print("Scan xk ccw from F1 until we reach edge intersecting line ->(v{}->v{})->v{}".format(i,
                                                                                                                   i + 1,
                                                                                                                   i + 1))
                    # Scan xk ccw from F1 to L1 until we reach edge intersecting line ->(vi->vi+1)->vi+1
                    w1 = None
                    idx_offsets = range(1, xk.shape[0] + 1)
                    for off in idx_offsets:
                        t = (F1_idx + off) % xk.shape[0]
                        if not xk_bounded and t == 0:
                            break
                        # Get intersection w1 of current edge and line ->(vi->vi+1)->vi+1
                        wt_prev = xk[t - 1, :]
                        wt = xk[t, :]
                        w1 = get_intersection(wt_prev, wt, start_point, vi_1)
                        if debug:
                            draw(xk, F1_idx, L1_idx, i, xk_bounded)
                            col = 'r' if w1 is None else 'g'
                            plt.plot([wt_prev[0], wt[0]], [wt_prev[1], wt[1]], marker='*', color=col, label="edge")
                            plt.plot([start_point[0], vi_1[0]], [start_point[1], vi_1[1]], marker='*', color='y',
                                     label="->(v{}->v{})->v{}".format(i, i + 1, i + 1))
                            plt.title(
                                "Scan xk ccw from F1 until we reach edge intersecting line ->(v{}->v{})->v{}".format(i,
                                                                                                                     i + 1,
                                                                                                                     i + 1))
                        if w1 is not None:
                            if debug:
                                plt.plot(w1[0], w1[1], 's', label="w1", color='g')
                                plt.legend()
                                plt.show()
                            break
                        if debug:
                            plt.legend()
                            plt.show()

                        if t == L1_idx:
                            break
                    # If no intersecting line is reached no kernel exists
                    if w1 is None:
                        # if debug:
                        draw(xk, F1_idx, L1_idx, i, xk_bounded)
                        plt.title('No kernel found.. Polygon not starshaped.')
                        plt.legend()
                        plt.show()
                        return False
                    if verbose:
                        print("Found intersection ({},{}) at line ->(v{}->v{})->v{}".format(w1[0], w1[1], i, i + 1,
                                                                                            i + 1))
                        print("Scan xk cw from F1 until we reach edge intersecting line ->(v{}->v{})->v{}".format(i,
                                                                                                                  i + 1,
                                                                                                                  i + 1))
                    # Scan xk cw from F1 until we reach edge intersecting line ->(vi->vi+1)->vi+1
                    w2 = None
                    idcs_list = np.flip(np.roll(np.arange(xk.shape[0]), -F1_idx - 1)) if xk_bounded else range(F1_idx,
                                                                                                               0, -1)
                    # for s in range(F1_idx, 0, -1):
                    for s in idcs_list:
                        ws = xk[s, :]
                        ws_prev = xk[s - 1, :]
                        # Get intersection w2 of edge and line ->(vi->vi+1)->vi+1
                        w2 = get_intersection(ws_prev, ws, start_point, vi_1)

                        if debug:
                            draw(xk, F1_idx, L1_idx, i, xk_bounded)
                            plt.plot(w1[0], w1[1], 's', label="w1", color='g')
                            col = 'r' if w2 is None else 'g'
                            plt.plot([ws_prev[0], ws[0]], [ws_prev[1], ws[1]], marker='*', color=col, label="edge")
                            plt.plot([start_point[0], vi_1[0]], [start_point[1], vi_1[1]], marker='*', color='y',
                                     label="->(v{}->v{})->v{}".format(i, i + 1, i + 1))
                            plt.title(
                                "Scan xk cw from F1 until we reach edge intersecting line ->(v{}->v{})->v{}".format(i,
                                                                                                                    i + 1,
                                                                                                                    i + 1))

                        if w2 is not None:
                            if xk_bounded and s > t:
                                alpha = xk[xk.shape[0]:, :]  # Empty array
                                beta = xk[t:s, :]
                                L1_idx = L1_idx - t + 2
                            else:
                                alpha = xk[:s, :]
                                beta = xk[t:, :]
                                L1_idx = L1_idx - t + s + 2
                            if debug:
                                print("xk: ", end=" ")
                                print(xk)
                                print("alpha: ", end=" ")
                                print(alpha)
                                print("beta: ", end=" ")
                                print(beta)
                                print("w1: ", end=" ")
                                print(w1)
                                print("w2: ", end=" ")
                                print(w2)
                                plt.plot(w1[0], w1[1], 's', label="w1", color='k')
                                plt.plot(w2[0], w2[1], 's', label="w2", color='g')
                                plt.plot(alpha[:, 0], alpha[:, 1], 'r--', label="alpha")
                                plt.plot(beta[:, 0], beta[:, 1], 'k--', label="beta")
                                plt.legend()
                                plt.show()
                            xk = np.vstack((alpha, w2, w1, beta))
                            w1_idx = alpha.shape[0] + 1
                            w2_idx = alpha.shape[0]
                            ####### F1 index reassignment should not be necessary for this case
                            F1_idx = w2_idx
                            # L1_idx = L1_idx - t + s + 2

                            if verbose:
                                print("Update 1")
                                print("Found intersection ({},{}) at line ->(v{}->v{})->v{}".format(w2[0], w2[1], i,
                                                                                                    i + 1, i + 1))
                            break
                        if debug:
                            plt.legend()
                            plt.show()
                    # If no intersecting line is reached
                    if w2 is None:
                        # Test if xk+1 is bounded
                        # If slope ->(vi->vi+1)->vi+1 is comprised between the slopes of initial and final half lines of xk,
                        if (orientation_val(xk[-2, :], xk[-1, :], start_point) * orientation_val(vi_1, start_point,
                                                                                                 xk[0, :])) > 0:
                            beta = xk[t:, :]
                            if debug:
                                draw(xk, F1_idx, L1_idx, i, xk_bounded)
                                plt.plot(w1[0], w1[1], 's', label="w1", color='g')
                                plt.plot(xk[:2, 0], xk[:2, 1], '--c', label="initial half line")
                                plt.plot(xk[-2:, 0], xk[-2:, 1], '--m', label="final half line")
                                plt.plot([start_point[0], vi_1[0]], [start_point[1], vi_1[1]], marker='*', color='y',
                                         label="->(v{}->v{})->v{}".format(i, i + 1, i + 1))
                                plt.title(
                                    "->(v{}->v{})->v{} between initial and finial half lines of xk".format(
                                        i, i + 1, i + 1))
                                plt.plot(beta[:, 0], beta[:, 1], 'k--', label="beta")
                                plt.legend()
                                plt.show()
                            if verbose:
                                print("Update 2 - xk still unbounded")
                            # then xk+1= ->(vi->vi+1)->w1->beta is also unbounded.
                            xk = np.vstack((start_point, w1, beta))
                            w1_idx = 1
                            # xk_bounded = False
                            F1_idx = 0
                            L1_idx -= t - 1


                        else:
                            # otherwise scan xk cw from xk[-1,:] until we reach edge intersecting line ->(vi->vi+1)->vi+1
                            if verbose:
                                print(
                                    "Scan xk cw from end until we reach edge intersecting line ->(v{}->v{})->v{}".format(
                                        i, i + 1, i + 1))
                            w2 = None
                            if xk_bounded:
                                r = xk.shape[0]
                                w2 = get_intersection(xk[0, :], xk[r - 1, :], start_point, vi_1)
                                if debug:
                                    draw(xk, F1_idx, L1_idx, i, xk_bounded)
                                    plt.plot(w1[0], w1[1], 's', label="w1", color='g')
                                    col = 'r' if w2 is None else 'g'
                                    plt.plot([xk[r - 1, 0], xk[0, 0]], [xk[r - 1, 1], xk[0, 1]], marker='*', color=col,
                                             label="edge")
                                    plt.plot([start_point[0], vi_1[0]], [start_point[1], vi_1[1]], marker='*',
                                             color='y',
                                             label="->(v{}->v{})->v{}".format(i, i + 1, i + 1))
                                    plt.title(
                                        "Scan xk cw from xk[-1, :] until we reach edge intersecting line ->(v{}->v{})->v{}".format(
                                            i, i + 1, i + 1))
                                    if w2 is not None:
                                        delta = xk[t:r, :]
                                        if debug:
                                            print(t, r)
                                            plt.plot(delta[:, 0], delta[:, 1], 'k--', label="delta")
                                            plt.plot(w1[0], w1[1], 's', label="w1", color='k')
                                            plt.plot(w2[0], w2[1], 's', label="w2", color='g')
                                            plt.legend()
                                            plt.show()
                                    if debug:
                                        plt.legend()
                                        plt.show()
                            if w2 is None:
                                for r in range(xk.shape[0] - 1, 0, -1):
                                    # Get intersection w2 of edge and line ->(vi->vi+1)->vi+1
                                    w2 = get_intersection(xk[r, :], xk[r - 1, :], start_point, vi_1)
                                    if debug:
                                        draw(xk, F1_idx, L1_idx, i, xk_bounded)
                                        plt.plot(w1[0], w1[1], 's', label="w1", color='g')
                                        col = 'r' if w2 is None else 'g'
                                        plt.plot([xk[r - 1, 0], xk[r, 0]], [xk[r - 1, 1], xk[r, 1]], marker='*',
                                                 color=col, label="edge")
                                        plt.plot([start_point[0], vi_1[0]], [start_point[1], vi_1[1]], marker='*',
                                                 color='y', label="->(v{}->v{})->v{}".format(i, i + 1, i + 1))
                                        plt.title(
                                            "Scan xk cw from xk[-1, :] until we reach edge intersecting line ->(v{}->v{})->v{}".format(
                                                i, i + 1, i + 1))
                                    if w2 is not None:
                                        delta = xk[t:r, :]
                                        if debug:
                                            print(t, r)
                                            plt.plot(delta[:, 0], delta[:, 1], 'k--', label="delta")
                                            plt.plot(w1[0], w1[1], 's', label="w1", color='k')
                                            plt.plot(w2[0], w2[1], 's', label="w2", color='g')
                                            plt.legend()
                                            plt.show()
                                        break
                                    if debug:
                                        plt.legend()
                                        plt.show()
                            if verbose:
                                print("Update 3")
                                print("Found intersection ({},{}) at line ->(v{}->v{})->v{}".format(w2[0], w2[1], i,
                                                                                                    i + 1, i + 1))
                            # Set xk as delta-w2-w1
                            xk = np.vstack((delta, w2, w1))
                            w1_idx = delta.shape[0] + 1
                            w2_idx = delta.shape[0]
                            xk_bounded = True

                            F1_idx = 0
                            L1_idx = min(L1_idx - t, xk.shape[0] - 1)

                # F1 update
                if case == 1:
                    # If ->(vi->vi+1)->vi+1 has just one intersection with xk F1 = startpoint
                    if w2 is None:
                        F1_idx = 0
                    # Otherwise F1 = w2
                    else:
                        F1_idx = w2_idx
                if case == 2:
                    # Scan xk ccw from F1 until find vertex wt s.t. wt+1 lies to the
                    # right of vi+1->(vi+1->wt)->. Let F1 = wt.
                    idx_offsets = range(xk.shape[0])
                    for off in idx_offsets:
                        t = (F1_idx + off) % xk.shape[0]
                        w_next = xk[(t + 1) % xk.shape[0], :]
                        line_end_point = vi_1 + INF_VAL * (xk[t, :] - vi_1)
                        if point_right_of_line(w_next, vi_1, line_end_point):
                            F1_idx = t
                            break

                # Check update of previous L1 index for new xk
                # if not np.isclose(np.linalg.norm(L1 - xk[L1_idx, :]), 0):
                # print("BAD L1 update [{},{}] -> [{},{}]".format(xk[L1_idx, 0], xk[L1_idx, 1], L1[0], L1[1]))
                # plt.figure(2)
                # draw(xk, F1_idx, L1_idx, i, xk_bounded)
                # plt.show()

                # L1 update
                # scan xk ccw from L1 until find vertex wu s.t. wu+1 lies to the
                # left of vi+1->(vi+1->wu)->. Let L1 = wu.
                idx_offsets = range(xk.shape[0])
                for off in idx_offsets:
                    u = (L1_idx + off) % xk.shape[0]
                    w_next = xk[(u + 1) % xk.shape[0], :]
                    line_end_point = vi_1 + INF_VAL * (xk[u, :] - vi_1)
                    if point_left_of_line(w_next, vi_1, line_end_point):
                        L1_idx = u
                        break

            else:
                if verbose:
                    print("(2)")
                # Endpoint for line vi->(vi->vi+1)->
                end_point = vi + INF_VAL * (vi_1 - vi) / np.linalg.norm(vi_1 - vi)

                # (2.2) L1 lies to the left of line vi->(vi->vi+1)->
                if point_left_of_line(L1, vi, end_point):
                    case = 2
                    if verbose:
                        print("(2.2) - L1 ({},{}) lies to the left of line v{}->(v{}->v{})->".format(L1[0], L1[1], i, i,
                                                                                                     i + 1))
                    # xk stays the same

                    if debug:
                        draw(xk, F1_idx, L1_idx, i, xk_bounded)
                        plt.plot([end_point[0], vi[0]], [end_point[1], vi[1]], marker='*', color='y',
                                 label="v{}->(v{}->v{})->".format(i, i, i + 1))
                        plt.title("L1 lies to the left of line v{}->(v{}->v{})->".format(i, i, i + 1))
                        plt.show()

                # (2.1) L1 lies on or to the right of line vi->(vi->vi+1)->
                else:
                    case = 1
                    if verbose:
                        print(
                            "(2.1) - L1 ({},{}) lies on or to the right of line v{}->(v{}->v{})->".format(L1[0], L1[1],
                                                                                                          i, i, i + 1))
                        print(
                            "Scan xk cw from L1 until we reach F1 or an edge intersecting line v{}->(v{}->v{})->".format(
                                i, i, i + 1))
                    # Scan xk cw from L1 until we reach F1 or an edge intersecting line vi->(vi->vi+1)->
                    idx_offsets = range(xk.shape[0])
                    w1 = None
                    for off in idx_offsets:
                        t = L1_idx - off
                        # If circular
                        if t < 0:
                            t += xk.shape[0]
                        if t == F1_idx:
                            break
                        # Get intersection w1 of edge and line vi->(vi->vi+1)->
                        w1 = get_intersection(xk[t, :], xk[t - 1, :], vi, end_point)
                        if debug:
                            draw(xk, F1_idx, L1_idx, i, xk_bounded)
                            col = 'r' if w1 is None else 'g'
                            plt.plot([xk[t - 1, 0], xk[t, 0]], [xk[t - 1, 1], xk[t, 1]], marker='*', color=col,
                                     label="edge")
                            plt.plot([end_point[0], v[i, 0]], [end_point[1], v[i, 1]], marker='*', color='y',
                                     label="v{}->(v{}->v{})->".format(i, i, i + 1))
                            plt.title(
                                "Scan xk cw from L1 until we reach F1 or an edge intersecting line v{}->(v{}->v{})->".format(
                                    i,
                                    i,
                                    i + 1))
                        if w1 is not None:
                            if debug:
                                plt.plot(w1[0], w1[1], 's', label="w1", color='g')
                                plt.legend()
                                plt.show()
                            break
                        if debug:
                            plt.legend()
                            plt.show()
                    # If no intersecting line is reached no kernel exists
                    if w1 is None:
                        # if debug:
                        draw(xk, F1_idx, L1_idx, i, xk_bounded)
                        plt.title('No kernel found. Polygon not starshaped.')
                        plt.show()
                        return False

                    if verbose:
                        print("Found intersection ({},{}) at line v{}->(v{}->v{})-> for edge ({},{})-({},{})".format(
                            w1[0], w1[1], i, i, i + 1, xk[t - 1, 0], xk[t - 1, 1], xk[t, 0], xk[t, 1]))
                        print("Scan xk ccw from L1 until we reach edge intersecting line v{}->(v{}->v{})->".format(i,
                                                                                                                   i,
                                                                                                                   i + 1))
                    # Scan xk ccw from L1 until we reach edge w2 intersecting line vi->(vi->vi+1)->
                    w2 = None
                    idx_offsets = range(1, xk.shape[0] + 1)
                    for off in idx_offsets:
                        s = (L1_idx + off) % xk.shape[0]
                        if not xk_bounded and s == 0:
                            break
                        # Get intersection w2 of edge and line vi->(vi->vi+1)->
                        w2 = get_intersection(xk[s - 1, :], xk[s, :], vi, end_point)
                        if debug:
                            draw(xk, F1_idx, L1_idx, i, xk_bounded)
                            col = 'r' if w2 is None else 'g'
                            plt.plot([xk[s - 1, 0], xk[s, 0]], [xk[s - 1, 1], xk[s, 1]], marker='*', color=col,
                                     label="edge")
                            plt.plot([end_point[0], v[i, 0]], [end_point[1], v[i, 1]], marker='*', color='y',
                                     label="v{}->(v{}->v{})->".format(i, i, i + 1))
                            plt.title(
                                "Scan xk ccw from L1 until we reach edge intersecting line v{}->(v{}->v{})->".format(i,
                                                                                                                     i,
                                                                                                                     i + 1))
                        if w2 is not None:
                            if xk_bounded and t > s:
                                alpha = xk[xk.shape[0]:, :]  # Empty array
                                beta = xk[s:t, :]
                            else:
                                alpha = xk[:t, :]
                                beta = xk[s:, :]
                            if debug:
                                print("xk: ", end=" ")
                                print(xk)
                                print("alpha: ", end=" ")
                                print(alpha)
                                print("beta: ", end=" ")
                                print(beta)
                                print("w1: ", end=" ")
                                print(w1)
                                print("w2: ", end=" ")
                                print(w2)
                                plt.plot(w1[0], w1[1], 's', label="w1", color='k')
                                plt.plot(w2[0], w2[1], 's', label="w2", color='g')
                                plt.plot(alpha[:, 0], alpha[:, 1], 'r--', label="alpha")
                                plt.plot(beta[:, 0], beta[:, 1], 'k--', label="beta")
                                plt.legend()
                                plt.show()
                            if verbose:
                                print("Update 1")
                                print(
                                    "Found intersection ({},{}) at line v{}->(v{}->v{})-> for edge ({},{})-({},{})".format(
                                        w2[0], w2[1], i, i, i + 1, xk[s, 0], xk[s, 1], xk[(s + 1) % xk.shape[0], 0],
                                        xk[(s + 1) % xk.shape[0], 1]))

                            xk = np.vstack((alpha, w1, w2, beta))
                            w1_idx = alpha.shape[0]
                            w2_idx = alpha.shape[0] + 1
                            break
                        if debug:
                            plt.legend()
                            plt.show()
                    # If no intersecting line is reached
                    if w2 is None:
                        # Test if xk+1 is bounded
                        # If slope vi->(vi->vi+1)-> is comprised between the slopes of initial and final half lines of xk,
                        if not xk_bounded and ((orientation_val(xk[-2, :], xk[-1, :], end_point) * orientation_val(vi,
                                                                                                                   end_point,
                                                                                                                   xk[0,
                                                                                                                   :])) > 0):
                            alpha = xk[:t, :]
                            if debug:
                                draw(xk, F1_idx, L1_idx, i, xk_bounded)
                                plt.plot(w1[0], w1[1], 's', label="w1", color='g')
                                plt.plot(xk[:2, 0], xk[:2, 1], '--c', label="initial half line")
                                plt.plot(xk[-2:, 0], xk[-2:, 1], '--m', label="final half line")
                                plt.plot([end_point[0], v[i, 0]], [end_point[1], v[i, 1]], marker='*', color='y',
                                         label="v{}->(v{}->v{})->".format(i, i, i + 1))
                                plt.title(
                                    "v{}->(v{}->v{})-> between initial and final half lines of xk".format(
                                        i, i, i + 1))
                                plt.plot(alpha[:, 0], alpha[:, 1], 'r--', label="alpha")
                                plt.legend()
                                plt.show()
                            if verbose:
                                print("Update 2 - xk still unbounded")
                            # then xk+1= alpha->w1->(vi->vi+1)-> is also unbounded.
                            xk = np.vstack((alpha, w1, end_point))
                            w1_idx = alpha.shape[0]
                            L1_idx = xk.shape[0] - 1
                            F1_idx = 0
                        else:
                            if verbose:
                                print(
                                    "Scan xk ccw from start until we reach edge intersecting line v{}->(v{}->v{})->".format(
                                        i,
                                        i,
                                        i + 1))
                            # otherwise scan xk ccw from xk[0,:] until we reach edge intersecting line vi->(vi->vi+1)->
                            w2 = None
                            for r in range(1, xk.shape[0] + 1):
                                # Get intersection w2 of edge and line vi->(vi->vi+1)->
                                w2 = get_intersection(xk[r - 1, :], xk[r % xk.shape[0], :], vi, end_point)
                                if debug:
                                    draw(xk, F1_idx, L1_idx, i, xk_bounded)
                                    col = 'r' if w2 is None else 'g'
                                    plt.plot([xk[r - 1, 0], xk[r % xk.shape[0], 0]],
                                             [xk[r - 1, 1], xk[r % xk.shape[0], 1]], marker='*', color=col,
                                             label="edge")
                                    plt.plot([end_point[0], v[i, 0]], [end_point[1], v[i, 1]], marker='*', color='y',
                                             label="v{}->(v{}->v{})->".format(i, i, i + 1))
                                    plt.title(
                                        "Scan xk ccw from xk[0,:] until we reach edge intersecting line v{}->(v{}->v{})->".format(
                                            i,
                                            i,
                                            i + 1))
                                if w2 is not None:
                                    delta = xk[r:t, :]
                                    if debug:
                                        plt.plot(w2[0], w2[1], 's', label="w2", color='g')
                                        plt.legend()
                                        plt.show()
                                    break
                                if debug:
                                    plt.legend()
                                    plt.show()
                            if verbose:
                                print("Update 3")
                                print("Found intersection ({},{}) at line v{}->(v{}->v{})->".format(w2[0], w2[1], i, i,
                                                                                                    i + 1))
                            # Set xk as delta-w1-vi-vi+1-w2
                            xk = np.vstack((delta, w1, w2))
                            w1_idx = delta.shape[0]
                            w2_idx = delta.shape[0] + 1
                            xk_bounded = True
                            F1_idx = 0
                            L1_idx = min(L1_idx - t, xk.shape[0] - 1)

                # F1 update

                # If vi+1 in vi->(vi->vi+1)->w1,
                if case == 2 or is_between(vi, vi_1, w1):
                    # scan xk ccw from F1 until find vertex wt s.t. wt+1 lies to the
                    # right of vi+1->(vi+1->wt)->. Let F1 = wt.
                    idx_offsets = range(xk.shape[0])
                    for off in idx_offsets:
                        t = (F1_idx + off) % xk.shape[0]
                        w_next = xk[(t + 1) % xk.shape[0], :]
                        line_end_point = vi_1 + INF_VAL * (xk[t, :] - vi_1)
                        if debug:
                            draw(xk, F1_idx, L1_idx, i, xk_bounded)
                            plt.plot([vi_1[0], line_end_point[0]], [vi_1[1], line_end_point[1]], '--',
                                     label='v{}->(v{}->wt)'.format(i + 1, i + 1))
                            c = 'g' if point_right_of_line(w_next, vi_1, line_end_point) else 'r'
                            plt.plot(w_next[0], w_next[1], color=c, marker='s', label='w_t+1')
                            plt.title(
                                "Update F1\n scan xk ccw from F1 until find vertex wt s.t. wt+1 lies to the right of v{}->(v{}->wt)->.".format(
                                    i + 1, i + 1))
                            plt.legend()
                            plt.show()
                        if point_right_of_line(w_next, vi_1, line_end_point):
                            F1_idx = t
                            break
                else:
                    F1_idx = w1_idx

                # L1 update
                if case == 1:
                    if w2 is not None:
                        # If vi+1 in vi->(vi->vi+1)->w2
                        if is_between(vi, vi_1, w2):
                            L1_idx = w2_idx
                        else:
                            # scan xk ccw from w2 until find vertex wu s.t. wu+1 lies to the
                            # left of vi+1->(vi+1->wu)->. Let L1 = wu.
                            idx_offsets = range(xk.shape[0])
                            for off in idx_offsets:
                                u = (w2_idx + off) % xk.shape[0]
                                w_next = xk[(u + 1) % xk.shape[0], :]
                                line_end_point = vi_1 + INF_VAL * (xk[u, :] - vi_1)
                                if debug:
                                    draw(xk, F1_idx, L1_idx, i, xk_bounded)
                                    plt.plot([vi_1[0], line_end_point[0]], [vi_1[1], line_end_point[1]], '--',
                                             label='v{}->(v{}->wt)'.format(i + 1, i + 1))
                                    c = 'g' if point_left_of_line(w_next, vi_1, line_end_point) else 'r'
                                    plt.plot(w_next[0], w_next[1], color=c, marker='s', label='w_t+1')
                                    plt.title(
                                        "Update L1\n scan xk ccw from w2 until find vertex wu s.t. wu+1 lies to the left of v{}->(v{}->wt)->.".format(
                                            i + 1, i + 1))
                                    plt.legend()
                                    plt.show()
                                if point_left_of_line(w_next, vi_1, line_end_point):
                                    L1_idx = u
                                    break

                    # (2.1.2) line vi->(vi->vi+1)-> intersects xk just in w1
                    else:
                        L1_idx = xk.shape[0] - 1
                if case == 2:
                    # print("(2.1.2)")
                    if xk_bounded:
                        # scan xk ccw from w2 until find vertex wu s.t. wu+1 lies to the
                        # left of vi+1->(vi+1->wu)->. Let L1 = wu.
                        idcs_list = np.roll(np.arange(xk.shape[0]), -L1_idx) if xk_bounded else range(L1_idx,
                                                                                                      xk.shape[0] - 1)
                        for u in idcs_list:
                            w_next = xk[(u + 1) % xk.shape[0], :]
                            line_end_point = vi_1 + INF_VAL * (xk[u, :] - vi_1)
                            if debug:
                                draw(xk, F1_idx, L1_idx, i, xk_bounded)
                                plt.plot([vi_1[0], line_end_point[0]], [vi_1[1], line_end_point[1]], '--',
                                         label='v{}->(v{}->wt)'.format(i + 1, i + 1))
                                c = 'g' if point_left_of_line(w_next, vi_1, line_end_point) else 'r'
                                plt.plot(w_next[0], w_next[1], color=c, marker='s', label='w_t+1')
                                plt.title(
                                    "Update L1\n scan xk ccw from w2 until find vertex wu s.t. wu+1 lies to the left of v{}->(v{}->wt)->.".format(
                                        i + 1, i + 1))
                                plt.legend()
                                plt.show()
                            if point_left_of_line(w_next, vi_1, line_end_point):
                                L1_idx = u
                                break

            if verbose:
                print("F1:", end=" ")
                print(F1)
                print("L1:", end=" ")
                print(L1)
                print("xk:", end=" ")
                print(xk)
        if debug or verbose:
            draw(xk, F1_idx, L1_idx, 0, xk_bounded)
            plt.show()
        _, idx = np.unique(xk, axis=0, return_index=True)
        xk = xk[np.sort(idx), :]
        self._kernel = StarshapedPolygon(polygon=xk, motion_model=self._motion_model, id="temporary", is_convex=True, xr=self._xr)

    def init_plot(self, ax=None, fc='lightgrey', show_reference=True, show_name=False, **kwargs):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        if "show_reference" in kwargs:
            show_reference = kwargs["show_reference"]
        line_handles = []
        # Boundary
        line_handles += [patches.Polygon(xy=[[0,0], [0,1], [1,0]], fc=fc, **kwargs)]
        ax.add_patch(line_handles[-1])
        # Reference point
        line_handles += ax.plot(0, 0, '+', color='k') if show_reference else [None]
        # Name
        line_handles += [ax.text(0, 0, self._name)] if show_name else [None]
        return line_handles

    def update_plot(self, line_handles, state=None, frame=Frame.GLOBAL):
        polygon = self.polygon(frame, state=state)
        boundary = np.vstack((polygon.exterior.xy[0], polygon.exterior.xy[1])).T
        line_handles[0].set_xy(boundary)
        if line_handles[1] is not None:
            line_handles[1].set_data(*self.xr(frame, state))
        if line_handles[2] is not None:
            line_handles[2].update_positions(*self.pos(frame, state))

    def draw(self, ax=None, line_handles=None, state=None, fc='darkred', show_reference=True, show_name=False, frame=Frame.GLOBAL, **kwargs):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        polygon = self.polygon(frame, state=state)
        boundary = np.vstack((polygon.exterior.xy[0], polygon.exterior.xy[1])).T
        if line_handles:
            line_handles[0].set_xy(boundary)
            if show_name:
                line_handles[1].update_positions(*(self.pos(frame, state)+self.xr(frame, state)))
            if show_reference:
                line_handles[2].set_data(*self.xr(frame, state))
        else:
            line_handles = []
            # Boundary
            line_handles += [patches.Polygon(xy=boundary, fc=fc, **kwargs)]
            ax.add_patch(line_handles[-1])
            # ax.plot(polygon.centroid.x, polygon.centroid.y)
            # Name
            if show_name:
                line_handles += [ax.text(*(self.pos(frame, state)+self.xr(frame, state)), self._name)]
            if show_reference:
                line_handles += ax.plot(*self.xr(frame, state), '+', color='k')
        return ax, line_handles

        #
        # line_handle = []
        # # Boundary
        # polygon = self.polygon(frame, state)
        # boundary = np.vstack((polygon.exterior.xy[0], polygon.exterior.xy[1])).T
        # line_handle += [
        #     patches.Polygon(xy=boundary, fc=fc, **kwargs)]
        # ax.add_patch(line_handle[-1])
        # ax.plot(polygon.centroid.x, polygon.centroid.y)
        # # Reference point
        # if show_reference:
        #     line_handle += ax.plot(*self.xr(frame, state), '+', color='k')
        # # Name
        # if show_name:
        #     line_handle += [ax.text(*self.xr(frame, state), self._name)]
        # return ax, line_handle
