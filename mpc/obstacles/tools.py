import numpy as np
import shapely.geometry
import matplotlib.pyplot as plt
import matplotlib.patches as patches

DEFAULT_RAY_INFINITY_LENGTH = 100000.


def affine_transform(x, rotation=0, translation=None, inverse=False):
    t = np.array([0, 0]) if translation is None else np.array(translation)
    c, s = np.cos(rotation), np.sin(rotation)
    rotation_matrix = np.array([[c, -s],
                                [s, c]])
    if inverse:
        return rotation_matrix.T.dot((np.array(x) - t).T).T
    else:
        return rotation_matrix.dot(np.array(x).T).T + t


# Line segment from a to b, excluding a and b
def line(a, b):
    return shapely.geometry.LineString([a + .0001 * (b - a), a + .9999 * (b - a)])


def cone_from_rays(r1, r2):
    v = np.vstack((np.array(r1.coords), r2.coords[1]))
    b = (v[1, :] + v[2, :]) / 2.
    if is_cw(v[0, :], v[1, :], v[2, :]):
        v_extra = v[0, :] - DEFAULT_RAY_INFINITY_LENGTH * b
    else:
        v_extra = v[0, :] + DEFAULT_RAY_INFINITY_LENGTH * b
    v = np.vstack((v[:2, :],
                   v_extra,
                   v[2, :]))
    return shapely.geometry.Polygon(v)


# Random point on the triangle with vertices a, b and c
def point_in_triangle(a, b, c):
    """
    """
    x, y = np.random.rand(2)
    q = abs(x - y)
    s, t, u = q, 0.5 * (x + y - q), 1 - 0.5 * (q + x + y)
    return s * a[0] + t * b[0] + u * c[0], s * a[1] + t * b[1] + u * c[1]

# TODO: Dynamic length of ray
# Ray emanating from a in direction of b->c
def ray(a, b, c, ray_inf_length=DEFAULT_RAY_INFINITY_LENGTH):
    return shapely.geometry.LineString([a + .0001 * (c - b), a + ray_inf_length * (c - b)])


def orientation_val(a, b, c):
    return (b[1] - a[1]) * (c[0] - b[0]) - (b[0] - a[0]) * (c[1] - b[1])


def is_collinear(a, b, c):
    return np.isclose(orientation_val(a, b, c), 0)


def is_cw(a, b, c):
    o = orientation_val(a, b, c)
    return not np.isclose(o, 0) and o > 0


def is_ccw(a, b, c):
    o = orientation_val(a, b, c)
    return not np.isclose(o, 0) and o < 0


# Returns true if point b is between point and b
def is_between(a, b, c):
    return np.isclose(np.linalg.norm(a-b) + np.linalg.norm(c-b), np.linalg.norm(a-c), rtol=1e-7)


def intersect(a1, a2, b1, b2):
    return (is_ccw(a1, b1, b2) != is_ccw(a2, b1, b2) and is_ccw(a1, a2, b1) != is_ccw(a1, a2, b2))


# Get intersection of line a and line b
def get_intersection(a1, a2, b1, b2):
    if not intersect(a1, a2, b1, b2):
        if is_between(b1, a1, b2):
            return a1
        if is_between(b1, a2, b2):
            return a2
        if is_between(a1, b1, a2):
            return b1
        if is_between(a1, b2, a2):
            return b2
        return None
    da = a2 - a1
    db = b2 - b1
    dp = a1 - b1
    dap = np.array([-da[1], da[0]])
    denom = np.dot(dap, db)
    num = np.dot(dap, dp)
    return (num / denom.astype(float)) * db + b1


def equilateral_triangle(centroid, side_length, rot=0):
    triangle = np.array(centroid) + np.array([[0, 2 / 3 * side_length],
                                               [1 / 2 * side_length, -1 / 3 * side_length],
                                               [-1 / 2 * side_length, -1 / 3 * side_length]])
    rot_mat = np.array([[np.cos(rot), -np.sin(rot)], [np.sin(rot), np.cos(rot)]])
    return rot_mat.dot(triangle.T).T


def draw_shapely_polygon(pol, ax=None, xlim=None, ylim=None, **kwargs):
    if ax is None:
        _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
    pol_list = []
    if pol.geom_type == 'Polygon':
        pol_list += [pol]
    else:
        for p in pol.geoms:
            if p.geom_type == 'Polygon':
                pol_list += [p]
    for p in pol_list:
        if xlim is not None and ylim is not None:
            pol_plot = p.intersection(shapely.geometry.box(xlim[0] - 1, ylim[0] - 1, xlim[1] + 1, ylim[1] + 1))
        else:
            pol_plot = p
        ax.add_patch(patches.Polygon(xy=np.vstack((pol_plot.exterior.xy[0], pol_plot.exterior.xy[1])).T, **kwargs))
        # ax.add_patch(patches.Polygon(xy=np.vstack((pol.exterior.xy[0], pol.exterior.xy[1])).T, **kwargs))
    # else:
    #     for p in pol.geoms:
    #         if p.geom_type == 'Polygon':
    #             ax.add_patch(patches.Polygon(xy=np.vstack((p.exterior.xy[0], p.exterior.xy[1])).T, **kwargs))


def intersection_two_halfplanes(hps):
    if abs(np.cross(hps[1].dir, hps[0].dir)) < 1e-9:
        return hps[1].p
    alpha = np.cross((hps[0].p - hps[1].p), hps[0].dir) / np.cross(hps[1].dir, hps[0].dir)
    return hps[1].p + (hps[1].dir * alpha)


class RayCone:

    def __init__(self, apex, dir1, dir2):
        self.apex = np.array(apex)
        self.dir1 = np.array(dir1) / np.linalg.norm(dir1)
        self.dir2 = np.array(dir2) / np.linalg.norm(dir2)
        self.angle1 = np.arctan2(dir1[1], dir1[0]) # - pi to pi
        self.angle2 = np.arctan2(dir2[1], dir2[0]) # -pi to 3 pi
        if self.angle2 <= self.angle1:
            self.angle2 += 2 * np.pi

    def polygon(self):
        l_inf = 1e6
        # box_points = np.array([[-l_inf, -l_inf],
        #                        [l_inf, -l_inf],
        #                        [l_inf, l_inf],
        #                        [-l_inf, l_inf]])
        # r1 = self.apex + l_inf * self.dir1
        # r2 = self.apex + l_inf * self.dir2
        # v = [self.apex, r1]
        # for bp in box_points:
        #     if is_ccw(r1, self.apex + bp, r2):
        #         v += [self.apex + bp]
        # v += [r2]
        # return shapely.geometry.Polygon(v)

        box_points = {-3*np.pi/4: np.array([-l_inf, -l_inf]),
                      -np.pi/4: np.array([l_inf, -l_inf]),
                      np.pi/4: np.array([l_inf, l_inf]),
                      3*np.pi/4: np.array([-l_inf, l_inf]),
                      5*np.pi/4: np.array([-l_inf, -l_inf]),
                      7*np.pi/4: np.array([l_inf, -l_inf]),
                      9*np.pi/4: np.array([l_inf, l_inf]),
                      11*np.pi/4: np.array([-l_inf, l_inf])}

        ang2 = self.angle2 if self.angle1 < self.angle2 else self.angle2 + 2*np.pi
        v = [self.apex, self.apex + l_inf * self.dir1]
        for ang, vert in box_points.items():
            if self.angle1 < ang < ang2:
                v += [vert]
        v += [self.apex + l_inf * self.dir2]
        return shapely.geometry.Polygon(v)

    def draw(self, ax=None, **kwargs):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
            bounds = self.polygon().bounds
            xlim, ylim = [1.5*bounds[0], 1.5*bounds[2]], [1.5*bounds[1], 1.5*bounds[3]]
        else:
            xlim, ylim = ax.get_xlim(), ax.get_ylim()
        draw_shapely_polygon(self.polygon(), ax=ax, **kwargs)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        return ax, []

    def _intersection_same_apex(self, other):
        assert(np.array_equal(self.apex, other.apex))

        other_dir1_in_cone = is_ccw(self.dir1, other.dir1, self.dir2)
        other_dir2_in_cone = is_ccw(self.dir1, other.dir2, self.dir2)
        dir1_in_other_cone = is_ccw(other.dir1, self.dir1, other.dir2)

        if other_dir1_in_cone and other_dir2_in_cone:
            # Check several cones
            if dir1_in_other_cone:
                return [RayCone(self.apex, self.dir1, other.dir2),
                        RayCone(self.apex, other.dir1, self.dir2)]
            else:
                return [other]
        elif other_dir1_in_cone:
            return [RayCone(self.apex, other.dir1, self.dir2)]
        elif other_dir2_in_cone:
            return [RayCone(self.apex, self.dir1, other.dir2)]
        else:
            if dir1_in_other_cone:
                return [self]
            else:
                return []

    @staticmethod
    def intersection_same_apex(ray_cones, debug_ax=None):
        assert all([np.array_equal(ray_cones[0].apex, c.apex) for c in ray_cones])

        c_intersect = [ray_cones[0]]

        for c in ray_cones[1:]:
            c_intersect_new = []
            for ci in c_intersect:
                c_intersect_new += ci._intersection_same_apex(c)

            if debug_ax:
                fig, ax = plt.subplots()
                # ax = debug_ax
                ax.plot([c.apex[0]-1, c.apex[0]+1], [c.apex[1], c.apex[1]], 'k')
                ax.plot([c.apex[0], c.apex[0]], [c.apex[1]-1, c.apex[1]+1], 'k')
                c.draw(ax=ax, alpha=0.2, fc='y')
                ax.plot(*zip(c.apex, c.apex+c.dir1), 'y')
                dir1_bracket = np.array([-c.dir1[1], c.dir1[0]]) / 10
                ax.plot(*zip(c.apex+c.dir1, c.apex+c.dir1 + dir1_bracket), 'y')
                ax.plot(*zip(c.apex, c.apex+ c.dir2), 'y')
                dir2_bracket = np.array([c.dir2[1], -c.dir2[0]]) / 10
                ax.plot(*zip(c.apex+c.dir2, c.apex+c.dir2 + dir2_bracket), 'y')
                for ci in c_intersect:
                    ci.draw(ax=ax, alpha=0.2, fc='b')
                    ax.plot(*zip(ci.apex, ci.apex + ci.dir1), 'b')
                    dir1_bracket = np.array([-ci.dir1[1], ci.dir1[0]]) / 10
                    ax.plot(*zip(ci.apex + ci.dir1, ci.apex + ci.dir1 + dir1_bracket), 'b')
                    ax.plot(*zip(ci.apex, ci.apex + ci.dir2), 'b')
                    dir2_bracket = np.array([ci.dir2[1], -ci.dir2[0]]) / 10
                    ax.plot(*zip(ci.apex + ci.dir2, ci.apex + ci.dir2 + dir2_bracket), 'b')
                if c_intersect_new:
                    for ci in c_intersect_new:
                        ci.draw(ax=ax, alpha=1, fc='g')
                else:
                    ax.plot(0, 0, 'rx')
                plt.show()

            c_intersect = c_intersect_new

        return c_intersect

        # dir1s = np.array([[2, 1],
        #                   [-1, 2],
        #                   [-2, -1],
        #                   [1, -2]])
        # dir2s = np.array([[1, 2],
        #                   [-2, 1],
        #                   [-1, -2],
        #                   [2, -1]])
        # dir1s = (dir1s.T / np.linalg.norm(dir1s, axis=1)).T
        # dir2s = (dir2s.T / np.linalg.norm(dir2s, axis=1)).T
        #
        # c1 = [[RayCone([0, 0], dir1s[i], dir2s[j]) for j in range(4)] for i in range(4)]
        #
        # while True:
        #     fig, ax = plt.subplots(4, 4)
        #     c_dir1 = np.random.randn(2)
        #     c_dir1 /= np.linalg.norm(c_dir1)
        #     c_dir2 = np.random.randn(2)
        #     c_dir2 /= np.linalg.norm(c_dir2)
        #
        #
        #     for i in range(4):
        #         for j in range(4):
        #             ax[i, j].plot([-1, 1], [0, 0], 'k')
        #             ax[i, j].plot([0, 0], [-1, 1], 'k')
        #
        #             c2 = RayCone([0, 0], c_dir1, c_dir2)
        #
        #             c1[i][j].draw(ax=ax[i, j], alpha=0.2, fc='b')
        #             c2.draw(ax=ax[i, j], alpha=0.2, fc='y')
        #
        #             ax[i, j].plot(*zip(np.zeros(2), dir1s[i]), 'b')
        #             dir1_bracket = np.array([-dir1s[i][1], dir1s[i][0]]) / 10
        #             ax[i, j].plot(*zip(dir1s[i], dir1s[i] + dir1_bracket), 'b')
        #             ax[i, j].plot(*zip(np.zeros(2), dir2s[j]), 'b')
        #             dir2_bracket = np.array([dir2s[j][1], -dir2s[j][0]]) / 10
        #             ax[i, j].plot(*zip(dir2s[j], dir2s[j] + dir2_bracket), 'b')
        #
        #
        #             c_dir1_bracket = np.array([-c_dir1[1], c_dir1[0]]) / 10
        #             ax[i, j].plot(*zip(np.zeros(2), c_dir1), '--', color='y')
        #             ax[i, j].plot(*zip(c_dir1, c_dir1 + c_dir1_bracket), color='y')
        #             c_dir2_bracket = np.array([c_dir2[1], -c_dir2[0]]) / 10
        #             ax[i, j].plot(*zip(np.zeros(2), c_dir2), '--', color='y')
        #             ax[i, j].plot(*zip(c_dir2, c_dir2 + c_dir2_bracket), color='y')
        #
        #             c3 = c1[i][j].intersection_same_apex(c2)
        #
        #             if c3:
        #                 for c in c3:
        #                     c.draw(ax=ax[i, j], alpha=0.8, fc='g')
        #             else:
        #                 ax[i, j].plot(0, 0, 'rx')
        #
        #     plt.show()