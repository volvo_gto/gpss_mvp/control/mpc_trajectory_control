from mpc.obstacles import StarshapedObstacle, Frame
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from mpc.obstacles import is_ccw


class Ellipse(StarshapedObstacle):
    def __init__(self, a, n_pol=100, **kwargs):
        self._a = np.array(a, dtype=float)
        self._n_pol = n_pol
        # Generate polygon approximation
        t = np.linspace(0, 2 * np.pi, n_pol, endpoint=False)
        p_obstacle = np.vstack((self._a[0] * np.cos(t), self._a[1] * np.sin(t))).T
        super().__init__(polygon=p_obstacle, **kwargs)

    def extreme_points(self, frame=Frame.GLOBAL, state=None):
        return [self.transform([-self._a[0], 0], Frame.OBSTACLE, frame, state),
                self.transform([self._a[0], 0], Frame.OBSTACLE, frame, state),
                self.transform([0, -self._a[1]], Frame.OBSTACLE, frame, state),
                self.transform([0, self._a[1]], Frame.OBSTACLE, frame, state)]

    def _check_convexity(self):
        self._is_convex = True

    def _compute_kernel(self):
        self._kernel = self.copy(id='temporary')

    def dilated_obstacle(self, padding, id="new", name=None):
        cp = self.copy(id, name)
        cp._a += padding
        cp._polygon = cp._polygon.buffer(padding, cap_style=3, join_style=3)
        return cp

    def init_plot(self, ax=None, fc='lightgrey', show_reference=True, show_name=False, **kwargs):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        if "show_reference" in kwargs:
            show_reference = kwargs["show_reference"]
        line_handles = []
        # Boundary
        line_handles += [patches.Ellipse(xy=[0, 0], width=2*self._a[0], height=2*self._a[1], angle=0, fc=fc, **kwargs)]
        ax.add_patch(line_handles[-1])
        # Reference point
        line_handles += ax.plot(0, 0, '+', color='k') if show_reference else [None]
        # Name
        line_handles += [ax.text(0, 0, self._name)] if show_name else [None]
        return line_handles

    def update_plot(self, line_handles, state=None, frame=Frame.GLOBAL):
        pos, rot = self.pos(frame, state), self.rot(frame, state)
        line_handles[0].set_center(pos)
        line_handles[0].set_angle(np.rad2deg(rot))
        if line_handles[1] is not None:
            line_handles[1].set_data(*self.xr(frame, state))
        if line_handles[2] is not None:
            line_handles[2].update_positions(*self.xr(frame, state))

    def draw(self, ax=None, line_handles=None, state=None, fc='darkred', show_reference=True, show_name=False, frame=Frame.GLOBAL, **kwargs):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        pos, rot = self.pos(frame, state), self.rot(frame, state)
        if "show_reference" in kwargs:
            show_reference = kwargs["show_reference"]
        if line_handles:
            line_handles[0].set_center(pos)
            line_handles[0].set_angle(np.rad2deg(rot))
            if show_reference:
                line_handles[1].set_data(*self.xr(frame, state))
            if show_name:
                line_handles[2].update_positions(*self.xr(frame, state))
        else:
            line_handles = []
            # Boundary
            # line_handles += [patches.Ellipse(xy=pos, width=2*self._a[0], height=2*self._a[1], angle=np.rad2deg(rot), fc=fc, **kwargs)]
            line_handles += [patches.Ellipse(xy=pos, width=2*self._a[0], height=2*self._a[1], angle=np.rad2deg(rot), fc=fc, **kwargs)]
            ax.add_patch(line_handles[-1])
            # ax.plot(*self.xr(frame))
            # Reference point
            if show_reference:
                line_handles += ax.plot(*self.xr(frame, state), '+', color='k')
            # Name
            if show_name:
                line_handles += [ax.text(*self.xr(frame, state), self._name)]
        return ax, line_handles

    def radius(self, angle):
        return (self._a[0] * self._a[1]) / np.linalg.norm([self._a[1]*np.cos(angle), self._a[0]*np.sin(angle)])

    def boundary_point(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        xdir_obstacle = self.reference_direction(x, input_frame, Frame.OBSTACLE, state)
        angle = np.arctan2(xdir_obstacle[1], xdir_obstacle[0])
        b_obstacle = self.radius(angle) * xdir_obstacle
        return self.transform(b_obstacle, Frame.OBSTACLE, output_frame, state)

    def normal(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, x_is_boundary=False, state=None):
        b_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state) if x_is_boundary else self.boundary_point(x, input_frame, Frame.OBSTACLE, state)
        n_obstacle = np.array([self._a[1] ** 2 * b_obstacle[0], self._a[0] ** 2 * b_obstacle[1]])
        n_obstacle = n_obstacle / np.linalg.norm(n_obstacle)
        return self.rotate(n_obstacle, Frame.OBSTACLE, output_frame, state)

    def point_location(self, x, input_frame=Frame.GLOBAL, state=None):
        x_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state)
        return (x_obstacle[0] / self._a[0]) ** 2 + (x_obstacle[1] / self._a[1]) ** 2 - 1

    def line_intersection(self, line, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        # Transform line points to left/right points in obstacle ellipse coordinates
        l0_obstacle = self.transform(line[0], input_frame, Frame.OBSTACLE)
        l1_obstacle = self.transform(line[1], input_frame, Frame.OBSTACLE)
        l_left_obstacle = l0_obstacle if l0_obstacle[0] < l1_obstacle[0] else l1_obstacle
        l_right_obstacle = l1_obstacle if l0_obstacle[0] < l1_obstacle[0] else l0_obstacle

        # Special case with vertical line
        if np.isclose(l_right_obstacle[0],l_left_obstacle[0]):
            l_top_obstacle = l_right_obstacle if l_right_obstacle[1] > l_left_obstacle[1] else l_left_obstacle
            l_bottom_obstacle = l_left_obstacle if l_right_obstacle[1] > l_left_obstacle[1] else l_right_obstacle
            x_intersect_top_obstacle, x_intersect_bottom_obstacle = np.array([0, self._a[1]]), np.array([0, -self._a[1]])
            x_intersect_top = self.transform(x_intersect_top_obstacle, Frame.OBSTACLE, output_frame, state)
            x_intersect_bottom = self.transform(x_intersect_bottom_obstacle, Frame.OBSTACLE, output_frame, state)

            if l_top_obstacle[1] >= self._a[1] and l_bottom_obstacle[1] <= -self._a[1]:
                return [x_intersect_top, x_intersect_bottom]
            elif l_top_obstacle[1] >= self._a[1] and l_bottom_obstacle[1] <= self._a[1]:
                return [x_intersect_top]
            elif l_top_obstacle[1] >= -self._a[1] and l_bottom_obstacle[1] <= -self._a[1]:
                return [x_intersect_bottom]
            else:
                return []

        # Line parameters
        m = (l_right_obstacle[1] - l_left_obstacle[1]) / (l_right_obstacle[0] - l_left_obstacle[0])
        c = l_left_obstacle[1] - m * l_left_obstacle[0]

        # obstacle ellipse coefficients at intersection with line m*x+c
        kx2 = self._a[0]**2 * m**2 + self._a[1] ** 2
        kx = 2 * self._a[0]**2 * m * c
        k1 = self._a[0]**2 * (c**2 - self._a[1]**2)

        discriminant = self._a[0]**2 * m**2 + self._a[1] ** 2 - c**2
        if discriminant < 0:
            return []
        elif discriminant == 0:
            tmp_x = -kx / (2 * kx2)
            x_intersect_obstacle = np.array([tmp_x, m * tmp_x + c])
            return [self.transform(x_intersect_obstacle, Frame.OBSTACLE, output_frame, state)]
        else:
            tmp_x = -kx / (2 * kx2) - np.sqrt(kx**2 / (4 * kx2**2) - k1 / kx2)
            x_intersect_left_obstacle = np.array([tmp_x, m * tmp_x + c])
            tmp_x = -kx / (2 * kx2) + np.sqrt(kx**2 / (4 * kx2**2) - k1 / kx2)
            x_intersect_right_obstacle = np.array([tmp_x, m * tmp_x + c])
            x_intersect_left = self.transform(x_intersect_left_obstacle, Frame.OBSTACLE, output_frame, state)
            x_intersect_right = self.transform(x_intersect_right_obstacle, Frame.OBSTACLE, output_frame, state)

            if l_right_obstacle[0] < x_intersect_left_obstacle[0] or x_intersect_right_obstacle[0] < l_left_obstacle[0] or \
                    x_intersect_left_obstacle[0] < l_left_obstacle[0] < l_right_obstacle[0] < x_intersect_right_obstacle[0]:
                # print("No intersection")
                return []
            elif x_intersect_left_obstacle[0] < l_left_obstacle[0]:
                # print("Right")
                return [x_intersect_right]
            elif l_right_obstacle[0] < x_intersect_right_obstacle[0]:
                # print("Left")
                return [x_intersect_left]
            else:
                # print("Both")
                return [x_intersect_left, x_intersect_right]

    def tangent_points(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        # import time
        # t0 = time.time()
        x_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state)
        # print("Input transform:" + str((time.time() - t0) * 1000))

        # t0 = time.time()
        if not self.exterior_point(x_obstacle, Frame.OBSTACLE, state):
            return []
        # print("Internal check:" + str((time.time() - t0) * 1000))
        # t0 = time.time()

        px, py = x_obstacle

        # Special case with vertical tangent
        if self._a[0]**2 == px**2:
            m2 = (py**2-self._a[1]**2) / (2*px*py)
            x2 = (px * m2 ** 2 - py * m2) / (self._a[1] ** 2 / self._a[0] ** 2 + m2 ** 2)
            y2 = m2 * (x2 - px) + py
            tp1_obstacle = np.array([px, 0])
            tp2_obstacle = np.array([x2, y2])
        else:
            c1 = (px * py) / (px**2 - self._a[0]**2)
            c2 = (self._a[1]**2 - py**2) / (px**2 - self._a[0]**2)
            m1 = c1 + np.sqrt(c1**2+c2)
            m2 = c1 - np.sqrt(c1**2+c2)
            x1 = (px * m1**2 - py * m1) / (self._a[1]**2 / self._a[0]**2 + m1 ** 2)
            x2 = (px * m2**2 - py * m2) / (self._a[1]**2 / self._a[0]**2 + m2 ** 2)
            y1 = m1 * (x1 - px) + py
            y2 = m2 * (x2 - px) + py
            tp1_obstacle = np.array([x1, y1])
            tp2_obstacle = np.array([x2, y2])

        # print("tp calc:" + str((time.time() - t0) * 1000))
        # t0 = time.time()
        
        tp1 = self.transform(tp1_obstacle, Frame.OBSTACLE, output_frame, state)
        tp2 = self.transform(tp2_obstacle, Frame.OBSTACLE, output_frame, state)

        if is_ccw(x, tp1, tp2):
            tp1, tp2 = tp2, tp1
        # print("Output transform:" + str((time.time() - t0) * 1000))
        
        return [tp1, tp2]
