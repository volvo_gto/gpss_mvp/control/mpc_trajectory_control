from abc import abstractmethod
import numpy as np
from mpc.obstacles import Obstacle, Frame


class StarshapedObstacle(Obstacle):

    def __init__(self, polygon, xr=None, **kwargs):
        super().__init__(polygon=polygon, **kwargs)
        self._is_starshaped = True
        self._kernel = None
        self._xr = np.zeros(2) if xr is None else xr  # Reference point in local frame

    def xr(self, output_frame=Frame.LOCAL, state=None):
        return self.transform(self._xr, Frame.LOCAL, output_frame, state)

    def set_xr(self, xr, input_frame=Frame.LOCAL, safe_set=False, state=None):
        new_xr = self.transform(xr, input_frame, Frame.LOCAL, state)
        if safe_set:
            k = self.kernel()
            if not k.exterior_point(new_xr, Frame.LOCAL, state):
                self._xr = new_xr
        else:
            self._xr = new_xr

    def vel_intertial_frame(self, x):
        omega = self._motion_model.rot_vel()
        lin_vel_omega = np.cross(np.hstack(([0, 0], omega)), np.hstack((self.reference_direction(x), 0)))[:2]
        return self._motion_model.lin_vel() + lin_vel_omega

    def reference_direction(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        dir = self.transform(x, input_frame, output_frame, state) - self.transform(self._xr, Frame.LOCAL, output_frame, state)
        return dir / np.linalg.norm(dir, axis=x.ndim - 1)

    def distance_function(self, x, input_frame=Frame.GLOBAL, state=None):
        x_local = self.transform(x, input_frame, Frame.LOCAL, state)
        return (np.linalg.norm(x_local - self._xr, axis=x.ndim - 1) / (
            np.linalg.norm(self.boundary_point(x_local, input_frame=Frame.LOCAL, output_frame=Frame.LOCAL, state=state) - self._xr,
                           axis=x.ndim - 1))) ** 2

    def point_location(self, x, input_frame=Frame.GLOBAL, state=None):
        return np.sign(self.distance_function(x, input_frame, state)-1.)

    def kernel(self):
        # Check if kernel already has been computed
        if self._kernel is None:
            self._compute_kernel()
        return self._kernel

    def move(self, dt, forbidden_regions=None, dist=0):
        if self._kernel is not None:
            self._kernel.move(dt, forbidden_regions, dist)
        super(StarshapedObstacle, self).move(dt, forbidden_regions, dist)

    # ------------ Abstract methods ------------ #
    @abstractmethod
    def normal(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, x_is_boundary=False, state=None):
        pass

    @abstractmethod
    def boundary_point(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        pass

    @abstractmethod
    def _compute_kernel(self):
        pass