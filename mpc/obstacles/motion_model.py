import numpy as np
from abc import ABC, abstractmethod


class MotionModel(ABC):

    def __init__(self, pos=None, rot=0):
        self._pos = np.array([0., 0.]) if pos is None else np.array(pos)
        self._rot = rot
        self._t = 0.

    def move(self, obs_self, dt, forbidden_regions=None, dist=0):
        xy_vel = self.lin_vel()
        rot_vel = self.rot_vel()

        next_state = np.array([
            self._pos[0] + xy_vel[0] * dt,
            self._pos[1] + xy_vel[1] * dt,
            self._rot + rot_vel * dt
        ])
        # TODO: Nicer fix than hardcoded dilated obstacle
        move_ok = True
        if forbidden_regions is not None:
            tmp_obs = obs_self.dilated_obstacle(padding=dist, id="temp")
            if np.any([tmp_obs.interior_point(p, state=next_state) for p in forbidden_regions]):
                move_ok = False

        if move_ok:
            self._pos, self._rot = next_state[:2], next_state[2]
        self._t += dt

    def set_pos(self, pos):
        self._pos = pos

    def set_rot(self, rot):
        self._rot = rot

    def pos(self):
        return self._pos

    def rot(self):
        return self._rot

    @abstractmethod
    def lin_vel(self):
        pass

    @abstractmethod
    def rot_vel(self):
        pass


class Static(MotionModel):

    def lin_vel(self):
        return np.zeros(2)

    def rot_vel(self):
        return 0.

    # def move(self, obs_self, dt, forbidden_regions=None, dist=0):
    #     pass


class SinusVelocity(MotionModel):

    # If cartesian_coords: (x1,x2) vel are Cartesian (x,y) vel.
    # Else:                (x1,x2) vel are linear and rotational (lin,ang) vel.
    def __init__(self, pos=None, rot=0, cartesian_coords=True, x1_mag=0., x1_period=0, x2_mag=0., x2_period=0):
        super().__init__(pos, rot)
        self.cartesian_coords = cartesian_coords
        self.x1_vel_mag = x1_mag
        self.x1_vel_freq = 2 * np.pi / x1_period if x1_period else 0
        self.x2_vel_mag = x2_mag
        self.x2_vel_freq = 2 * np.pi / x2_period if x2_period else 0

    def lin_vel(self):
        if self.cartesian_coords:
            return np.array([self.x1_vel_mag * np.cos(self.x1_vel_freq * self._t),
                             self.x2_vel_mag * np.cos(self.x2_vel_freq * self._t)])
        else:
            rot_mat = np.array([[np.cos(self._rot), -np.sin(self._rot)], [np.sin(self._rot), np.cos(self._rot)]])
            return rot_mat.dot([self.x1_vel_mag * np.cos(self.x1_vel_freq * self._t), 0])

    def rot_vel(self):
        if self.cartesian_coords:
            return 0.
        else:
            return np.array(self.x2_vel_mag * np.cos(self.x2_vel_freq * self._t))

class Interval(MotionModel):

    def __init__(self, init_pos, time_pos):
        self.pos_point = np.array([init_pos] + [p for _, p in time_pos])
        self.time_point = np.cumsum([0] + [t for t, _ in time_pos])
        super().__init__(init_pos, 0)

    def lin_vel(self):
        if self._t > self.time_point[-1]:
            return np.zeros(2)
        idx = np.argmax(self._t < self.time_point)
        return (self.pos_point[idx] - self.pos_point[idx-1]) / (self.time_point[idx] - self.time_point[idx-1])

    def rot_vel(self):
        return 0.

    # def move(self, obs_self, dt, forbidden_regions=None, dist=0):
    #     # if self.cartesian_coords:
    #     #     xy_vel = np.array([self.x1_vel_mag * np.cos(self.x1_vel_freq * self.t),
    #     #                        self.x2_vel_mag * np.cos(self.x2_vel_freq * self.t)])
    #     #     rot_vel = 0
    #     # else:
    #     #     rot_mat = np.array([[np.cos(self._rot), -np.sin(self._rot)], [np.sin(self._rot), np.cos(self._rot)]])
    #     #     xy_vel = rot_mat.dot([self.x1_vel_mag * np.cos(self.x1_vel_freq * self.t), 0])
    #     #     rot_vel = self.x2_vel_mag * np.cos(self.x2_vel_freq * self.t)
    #
    #     xy_vel = self.lin_vel()
    #     rot_vel = self.rot_vel()
    #
    #     next_state = np.array([
    #         self._pos[0] + xy_vel[0] * dt,
    #         self._pos[1] + xy_vel[1] * dt,
    #         self._rot + rot_vel * dt
    #     ])
    #     # TODO: Nicer fix than hardcoded dilated obstacle
    #     move_ok = True
    #     if forbidden_regions is not None:
    #         tmp_obs = obs_self.dilated_obstacle(padding=dist, id="temp")
    #         if np.any([tmp_obs.interior_point(p, state=next_state) for p in forbidden_regions]):
    #             move_ok = False
    #
    #     if move_ok:
    #         self._pos, self._rot = next_state[:2], next_state[2]
    #     self.t += dt


# class AugmentedCoordinatedTurn:
#
#     def __init__(self, x=0., y=0., rot=0., vel_var=0., ang_vel_var=0., vel_max=0., ang_vel_max=0., rng_seed=None):
#         self.X = np.array([x, y, rot])
#         self.vel_sig = np.sqrt(vel_var)
#         self.ang_vel_sig = np.sqrt(ang_vel_var)
#         self.vel_max = vel_max
#         self.ang_vel_max = ang_vel_max
#         self.rng = np.random.default_rng(seed=rng_seed)
#
#     def move(self, obs_self, dt, forbidden_regions):
#         x, y, rot = self.X
#         vel = np.clip(self.rng.normal(0, self.vel_sig), -self.vel_max, self.vel_max)
#         ang_vel = np.clip(self.rng.normal(0, self.ang_vel_sig), -self.ang_vel_max, self.ang_vel_max)
#         X_next = np.array([
#                     x + np.cos(rot) * vel * dt,
#                     y + np.sin(rot) * vel * dt,
#                     rot + ang_vel * dt,
#                     ])
#         state = X_next
#         # pol = obs_self.polygon_approximation(state)
#         # if np.any([pol.intersects(r) for r in forbidden_regions]):
#         # TODO: Nicer fix than hardcoded dilated obstacle
#         tmp_obs = obs_self.dilated_obstacle(padding=0.5+0.2, obs_id=-1)
#         if np.any([tmp_obs.point_location(r, state=state) < 0 for r in forbidden_regions]):
#             pass
#         else:
#             self.X = X_next