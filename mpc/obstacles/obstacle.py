from abc import ABC, abstractmethod
import numpy as np
import mpc.obstacles.motion_model as mm
import shapely.geometry
import shapely.ops
from mpc.obstacles import affine_transform
from copy import deepcopy
from enum import Enum


class Frame(Enum):
    GLOBAL = 1
    LOCAL = 2
    OBSTACLE = 3

    class InvalidFrameError(Exception):
        pass


class Obstacle(ABC):
    """ Abstract base class of obstacles
    """
    id_counter = 0

    # obs_id <0: temp object, obs_id=0: new object, obs_id>0: existing object with id #obs_id
    def __init__(self, polygon, x0_local=None, rot_local=0, motion_model=None, is_convex=None, id='new', name=None):
        self._id = None
        self._is_convex = is_convex
        self._is_starshaped = False
        # Polygon in obstacle frame
        self._polygon = shapely.geometry.Polygon(polygon)
        # Pose of local frame in global frame
        self._motion_model = motion_model if motion_model is not None else mm.Static([0., 0.], 0.)
        # Pose of obstacle frame in local frame
        self._x0_local = np.array([0., 0.]) if x0_local is None else np.array(x0_local)
        self._rot_local = rot_local
        # Initialize id and name
        self._set_id_name(id, name)

    def _set_id_name(self, id, name=None):
        if id == 'new' or id == 'n':
            Obstacle.id_counter += 1
            self._id = Obstacle.id_counter
        elif id == 'temporary' or id == 'temp' or id == 't':
            self._id = None
        elif isinstance(id, int) and 0 < id <= Obstacle.id_counter:
            self._id = id
        else:
            print("Invalid id '" + str(id) + "' in set_id. Create temporary obstacle.")
            self._id = None
        self._name = name if name else str(self._id)

    def copy(self, id='temporary', name=None):
        ob = deepcopy(self)
        if not (id == 'duplicate' or id == 'd'):
            ob._set_id_name(id, name)
        return ob

    def __str__(self): return self._name

    def pos(self, output_frame=Frame.GLOBAL, state=None):
        if output_frame == Frame.OBSTACLE:
            return np.array([0., 0.])
        if output_frame == Frame.LOCAL:
            return self._x0_local
        if output_frame == Frame.GLOBAL:
            return self.transform(self._x0_local, Frame.LOCAL, Frame.GLOBAL, state)

    def rot(self, output_frame=Frame.GLOBAL, state=None):
        if output_frame == Frame.OBSTACLE:
            return 0.
        if output_frame == Frame.LOCAL:
            return self._rot_local
        if output_frame == Frame.GLOBAL:
            return self._rot_local + self._motion_model.rot()

    def interior_point(self, x, input_frame=Frame.GLOBAL, state=None): return True if self.point_location(x, input_frame, state) < 0 else False

    def exterior_point(self, x, input_frame=Frame.GLOBAL, state=None): return True if self.point_location(x, input_frame, state) > 0 else False

    def boundary_point(self, x, input_frame=Frame.GLOBAL, state=None): return True if self.point_location(x, input_frame, state) == 0 else False

    def move(self, dt, forbidden_regions=None, dist=0): self._motion_model.move(self, dt, forbidden_regions, dist)

    def id(self): return self._id

    def polygon(self, output_frame=Frame.GLOBAL, state=None):
        if output_frame == Frame.OBSTACLE:
            return shapely.geometry.Polygon(self._polygon)
        elif output_frame == Frame.LOCAL or output_frame == Frame.GLOBAL:
            c, s = np.cos(self._rot_local), np.sin(self._rot_local)
            trans_matrix = np.array([[c, -s, self._x0_local[0]], [s, c, self._x0_local[1]], [0, 0, 1]])
            if output_frame == Frame.GLOBAL:
                pos, rot = (self._motion_model.pos(), self._motion_model.rot()) if state is None else (state[:2], state[2])
                c, s = np.cos(rot), np.sin(rot)
                trans_matrix = np.array([[c, -s, pos[0]], [s, c, pos[1]], [0, 0, 1]]).dot(trans_matrix)
            affinity_matrix = [trans_matrix[0, 0], trans_matrix[0, 1], trans_matrix[1, 0], trans_matrix[1, 1], trans_matrix[0, 2], trans_matrix[1, 2]]
            return shapely.affinity.affine_transform(self._polygon, affinity_matrix)
        else:
            raise Frame.InvalidFrameError

    def intersects(self, other):
        return self.polygon().intersects(other.polygon())

    def transform(self, x, input_frame, output_frame, state=None):
        if input_frame == output_frame:
            return x
        if input_frame == Frame.OBSTACLE and output_frame == Frame.LOCAL:
            return self.transform_obstacle2local(x)
        if input_frame == Frame.OBSTACLE and output_frame == Frame.GLOBAL:
            return self.transform_obstacle2global(x, state)
        if input_frame == Frame.LOCAL and output_frame == Frame.OBSTACLE:
            return self.transform_local2obstacle(x)
        if input_frame == Frame.LOCAL and output_frame == Frame.GLOBAL:
            return self.transform_local2global(x, state)
        if input_frame == Frame.GLOBAL and output_frame == Frame.OBSTACLE:
            return self.transform_global2obstacle(x, state)
        if input_frame == Frame.GLOBAL and output_frame == Frame.LOCAL:
            return self.transform_global2local(x, state)

    def rotate(self, x, input_frame, output_frame, state=None):
        if input_frame == output_frame:
            return x
        if input_frame == Frame.OBSTACLE and output_frame == Frame.LOCAL:
            return self.rotate_obstacle2local(x)
        if input_frame == Frame.OBSTACLE and output_frame == Frame.GLOBAL:
            return self.rotate_obstacle2global(x, state)
        if input_frame == Frame.LOCAL and output_frame == Frame.OBSTACLE:
            return self.rotate_local2obstacle(x)
        if input_frame == Frame.LOCAL and output_frame == Frame.GLOBAL:
            return self.rotate_local2global(x, state)
        if input_frame == Frame.GLOBAL and output_frame == Frame.OBSTACLE:
            return self.rotate_global2obstacle(x, state)
        if input_frame == Frame.GLOBAL and output_frame == Frame.LOCAL:
            return self.rotate_global2local(x, state)

    def rotate_local2obstacle(self, x_local): return affine_transform(x_local, rotation=self._rot_local, inverse=True)

    def rotate_obstacle2local(self, x_obstacle): return affine_transform(x_obstacle, rotation=self._rot_local)

    def transform_local2obstacle(self, x_local): return affine_transform(x_local, rotation=self._rot_local, translation=self._x0_local, inverse=True)

    def transform_obstacle2local(self, x_obstacle): return affine_transform(x_obstacle, rotation=self._rot_local, translation=self._x0_local)

    def rotate_local2global(self, x_local, state=None):
        rot = self._motion_model.rot() if state is None else state[2]
        return affine_transform(x_local, rotation=rot)

    def rotate_global2local(self, x_global, state=None):
        rot = self._motion_model.rot() if state is None else state[2]
        return affine_transform(x_global, rotation=rot, inverse=True)

    def transform_local2global(self, x_local, state=None):
        pos, rot = (self._motion_model.pos(), self._motion_model.rot()) if state is None else (state[:2], state[2])
        return affine_transform(x_local, rotation=rot, translation=pos)

    def transform_global2local(self, x_global, state=None):
        pos, rot = (self._motion_model.pos(), self._motion_model.rot()) if state is None else (state[:2], state[2])
        return affine_transform(x_global, rotation=rot, translation=pos, inverse=True)

    def transform_global2obstacle(self, x_global, state=None):
        return self.transform_local2obstacle(self.transform_global2local(x_global, state))

    def rotate_global2obstacle(self, x_global, state=None):
        return self.rotate_local2obstacle(self.rotate_global2local(x_global, state))

    def transform_obstacle2global(self, x_obstacle, state=None):
        return self.transform_local2global(self.transform_obstacle2local(x_obstacle), state)

    def rotate_obstacle2global(self, x_obstacle, state=None):
        return self.rotate_local2global(self.rotate_obstacle2local(x_obstacle), state)

    def is_convex(self):
        # Check if convexity already has been computed
        if self._is_convex is None:
            self._check_convexity()
        return self._is_convex

    def is_starshaped(self):
        return self._is_starshaped

    # ------------ Abstract methods ------------ #
    @abstractmethod
    def draw(self):
        pass

    @abstractmethod
    def point_location(self, x, input_frame=Frame.GLOBAL, state=None):
        pass

    @abstractmethod
    def dilated_obstacle(self, padding, id="new", name=None):
        pass

    @abstractmethod
    def line_intersection(self, line, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        pass

    @abstractmethod
    def tangent_points(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        pass

    @abstractmethod
    def _check_convexity(self):
        pass
