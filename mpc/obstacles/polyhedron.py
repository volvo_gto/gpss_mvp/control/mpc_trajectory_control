from mpc.obstacles import Obstacle, Frame, StarshapedObstacle, is_cw, is_ccw, is_collinear, orientation_val, get_intersection, is_between
import shapely.geometry
import shapely.ops
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial import ConvexHull


class Polyhedron(Obstacle):

    def __init__(self, polyhedron, **kwargs):
        super().__init__(polygon=polyhedron, **kwargs)

    @classmethod
    def ellipsoid(cls, a, n_pol, **kwargs):
        s = np.linspace(0, 2*np.pi, n_pol, endpoint=False)
        t = np.linspace(0, 2 * np.pi, n_pol, endpoint=False)
        # p_obstacle = np.vstack((a[0] * np.sin(s) * np.cos(t), a[1] * np.sin(s) * np.sin(t), a[2] * np.cos(s))).T

        u, v = np.mgrid[0:2 * np.pi:n_pol*1j, 0:np.pi:n_pol*1j]
        # x = np.cos(u) * np.sin(v)
        # y = np.sin(u) * np.sin(v)
        # z = np.cos(v)
        p_obstacle = np.array([a[0], 0, 0])
        for ui in range(n_pol):
            for vi in range(n_pol):
                x = a[0] * np.cos(u[ui,vi]) * np.sin(v[ui,vi])
                y = a[1] * np.sin(u[ui,vi]) * np.sin(v[ui,vi])
                z = a[2] * np.cos(v[ui,vi])
                p_obstacle = np.vstack((p_obstacle, np.array([x, y, z])))

        return cls(polyhedron=p_obstacle, **kwargs)

    def draw(self, ax=None, state=None, fc='darkred', show_name=False, show_reference=False, frame=Frame.OBSTACLE, alpha=1.):
        if ax is None:
            fig = plt.figure()
            ax = Axes3D(fig, auto_add_to_figure=False)
            fig.add_axes(ax)
        line_handle = []
        # Boundary
        verts = np.array(self.polygon(frame, state=state).exterior.coords[:-1])
        hull = ConvexHull(verts)
        # draw the polygons of the convex hull
        for s in hull.simplices:
            tri = Poly3DCollection(verts[s])
            tri.set_color(fc)
            tri.set_alpha(alpha)
            ax.add_collection3d(tri)
        # Name
        if show_name:
            line_handle += [ax.text(*self.pos(frame, state), self._name)]
        return ax, line_handle

    def dilated_obstacle(self, padding, id="new", name=None):
        cp = self.copy(id, name)
        cp._polygon = cp._polygon.buffer(padding, cap_style=3, join_style=3)
        return cp

    def point_location(self, x, input_frame=Frame.GLOBAL, state=None):
        x_obstacle = self.transform(x, input_frame, state)
        x_sh = shapely.geometry.Point(x_obstacle)
        if self._polygon.contains(x_sh):
            return -1
        if self._polygon.exterior.contains(x_sh):
            return 0
        return 1

    # TODO: Add treatment of additional geom_type (e.g. multipoint+mulilinestring)
    def line_intersection(self, line, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        l0_obstacle = self.transform(line[0], input_frame, Frame.OBSTACLE, state)
        l1_obstacle = self.transform(line[1], input_frame, Frame.OBSTACLE, state)
        intersection_points_shapely = shapely.geometry.LineString([l0_obstacle, l1_obstacle]).intersection(self._polygon.exterior)
        if intersection_points_shapely.is_empty:
            return []
        if intersection_points_shapely.geom_type == 'Point':
            intersection_points_obstacle = [np.array([intersection_points_shapely.x, intersection_points_shapely.y])]
        elif intersection_points_shapely.geom_type == 'MultiPoint':
            intersection_points_obstacle = [np.array([p.x, p.y]) for p in intersection_points_shapely.geoms]
        elif intersection_points_shapely.geom_type == 'LineString':
            intersection_points_obstacle = [np.array([ip[0], ip[1]]) for ip in intersection_points_shapely.coords]
        else:
            print(intersection_points_shapely)
        return [self.transform(ip, Frame.OBSTACLE, output_frame, state) for ip in intersection_points_obstacle]

    def tangent_points(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        x_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state)
        vertices = np.asarray(self._polygon.exterior.coords)[:]
        tp = []
        for v in vertices:
            l1 = shapely.geometry.LineString([x_obstacle + .0001 * (v - x_obstacle), x_obstacle + 0.999 * (v - x_obstacle)])
            l2 = shapely.geometry.LineString([v + .0001 * (v - x_obstacle), v + 100 * (v - x_obstacle)])
            if (l1.disjoint(self._polygon) or l1.touches(self._polygon)) and l2.disjoint(self._polygon):
                tp += [self.transform(v, Frame.OBSTACLE, output_frame, state)]
        return tp

    def _check_convexity(self):
        pass

