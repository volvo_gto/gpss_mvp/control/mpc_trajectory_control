"""
The :mod:`obstacles` module implements various types of obstacles.
"""
# Various Obstacle Descriptions
from .tools import orientation_val, is_cw, is_ccw, is_collinear, get_intersection, is_between, line, ray, \
    affine_transform, equilateral_triangle, cone_from_rays, draw_shapely_polygon, RayCone, point_in_triangle
from .obstacle import Obstacle, Frame
from .motion_model import Static, SinusVelocity
from .starshaped_obstacle import StarshapedObstacle
from .ellipse import Ellipse
from .ellipsoid import Ellipsoid
from .polyhedron import Polyhedron
from .polygon import Polygon, StarshapedPolygon
from .starshaped_hull import StarshapedHullPrimitiveCombination, global_starshaped_hull, local_starshaped_hull, admissible_kernel_set, \
    get_intersection_clusters, cluster_and_starify, polygon_local_starshaped_hull,convex_local_starshaped_hull, \
    polygon_admissible_kernel_set, convex_admissible_kernel_set, ObstacleCluster, polygon_admissible_kernel, admissible_kernel

__all__ = ['Obstacle',
           'StarshapedObstacle',
           'Ellipse',
           'Static',
           'SinusVelocity',
           'StarshapedHullPrimitiveCombination',
           'admissible_kernel_set',
           'get_intersection_clusters',
           'cluster_and_starify'
           ]