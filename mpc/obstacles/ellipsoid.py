from mpc.obstacles import StarshapedObstacle, Frame
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from mpl_toolkits.mplot3d import Axes3D


class Ellipsoid(StarshapedObstacle):
    def __init__(self, a, n_pol=100, **kwargs):
        self._a = np.array(a)
        self._n_pol = n_pol
        # Generate polygon approximation
        u = np.linspace(0, 2 * np.pi, n_pol)
        v = np.linspace(0, np.pi, n_pol)
        x = self._a[0] * np.outer(np.cos(u), np.sin(v))
        y = self._a[1] * np.outer(np.sin(u), np.sin(v))
        z = self._a[2] * np.outer(np.ones(np.size(u)), np.cos(v))
        p_obstacle = np.vstack((x.flatten(), y.flatten(), z.flatten())).T

        # s = np.linspace(0, np.pi, n_pol, endpoint=False)
        # t = np.linspace(0, 2 * np.pi, n_pol, endpoint=False)
        # p_obstacle = np.vstack((self._a[0] * np.sin(s) * np.cos(t), self._a[1] * np.sin(s) * np.sin(t), self._a[2] * np.cos(s))).T
        super().__init__(polygon=p_obstacle, **kwargs)

    def _check_convexity(self):
        self._is_convex = True

    def _compute_kernel(self):
        self._kernel = self.copy(id='temporary')

    def dilated_obstacle(self, padding, id="new", name=None):
        cp = self.copy(id, name)
        cp._a += padding
        cp._polygon = cp._polygon.buffer(padding, cap_style=3, join_style=3)
        return cp

    def draw(self, ax=None, state=None, fc='darkred', show_reference=True, show_name=False, frame=Frame.GLOBAL, **kwargs):
        if ax is None:
            fig = plt.figure()
            ax = Axes3D(fig, auto_add_to_figure=False)
            fig.add_axes(ax)
            # fig = plt.figure(figsize=plt.figaspect(1))  # Square figure
            # ax = fig.add_subplot(111, projection='3d')

        pos, rot = self.pos(frame, state), self.rot(frame, state)
        line_handle = []
        # Boundary
        u = np.linspace(0, 2 * np.pi, 100)
        v = np.linspace(0, np.pi, 100)
        x = self._a[0] * np.outer(np.cos(u), np.sin(v))
        y = self._a[1] * np.outer(np.sin(u), np.sin(v))
        z = self._a[2] * np.outer(np.ones(np.size(u)), np.cos(v))
        line_handle += [ax.plot_surface(x, y, z, color=fc, **kwargs)]
        # line_handle += [patches.Ellipse(xy=pos, width=2*self._a[0], height=2*self._a[1], angle=np.rad2deg(rot), fc=fc, **kwargs)]
        # ax.add_patch(line_handle[-1])
        # Reference point
        if show_reference:
            line_handle += ax.plot(*self.xr(frame, state), '+', color='k')
        # Name
        if show_name:
            line_handle += [ax.text(*self.xr(frame, state), self._name)]
        return ax, line_handle

    def radius(self, angle):
        return (self._a[0] * self._a[1]) / np.linalg.norm([self._a[1]*np.cos(angle), self._a[0]*np.sin(angle)])

    def boundary_point(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        xdir_obstacle = self.reference_direction(x, input_frame, Frame.OBSTACLE, state)
        angle = np.arctan2(xdir_obstacle[1], xdir_obstacle[0])
        b_obstacle = self.radius(angle) * xdir_obstacle
        return self.transform(b_obstacle, Frame.OBSTACLE, output_frame, state)

    def normal(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, x_is_boundary=False, state=None):
        b_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state) if x_is_boundary else self.boundary_point(x, input_frame, Frame.OBSTACLE, state)
        n_obstacle = 2 * np.array([b_obstacle[0] / self._a[0], b_obstacle[1] / self._a[1], b_obstacle[2] / self._a[2]])
        n_obstacle = n_obstacle / np.linalg.norm(n_obstacle)
        return self.rotate(n_obstacle, Frame.OBSTACLE, output_frame, state)

    def point_location(self, x, input_frame=Frame.GLOBAL, state=None):
        x_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state)
        return (x_obstacle[0] / self._a[0]) ** 2 + (x_obstacle[1] / self._a[1]) ** 2 - 1

    def line_intersection(self, line, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        # Transform line points to left/right points in obstacle ellipse coordinates
        l0_obstacle = self.transform(line[0], input_frame, Frame.OBSTACLE)
        l1_obstacle = self.transform(line[1], input_frame, Frame.OBSTACLE)

        line(l0_obstacle, l1_obstacle).intersection(self.polygon())

        l_left_obstacle = l0_obstacle if l0_obstacle[0] < l1_obstacle[0] else l1_obstacle
        l_right_obstacle = l1_obstacle if l0_obstacle[0] < l1_obstacle[0] else l0_obstacle

        # Special case with vertical line
        if l_right_obstacle[0] - l_left_obstacle[0] == 0:
            l_top_obstacle = l_right_obstacle if l_right_obstacle[1] > l_left_obstacle[1] else l_left_obstacle
            l_bottom_obstacle = l_left_obstacle if l_right_obstacle[1] > l_left_obstacle[1] else l_right_obstacle
            x_intersect_top_obstacle, x_intersect_bottom_obstacle = np.array([0, self._a[1]]), np.array([0, -self._a[1]])
            x_intersect_top = self.transform(x_intersect_top_obstacle, Frame.OBSTACLE, output_frame, state)
            x_intersect_bottom = self.transform(x_intersect_bottom_obstacle, Frame.OBSTACLE, output_frame, state)

            if l_top_obstacle[1] >= self._a[1] and l_bottom_obstacle[1] <= -self._a[1]:
                return [x_intersect_top, x_intersect_bottom]
            elif l_top_obstacle[1] >= self._a[1] and l_bottom_obstacle[1] <= self._a[1]:
                return [x_intersect_top]
            elif l_top_obstacle[1] >= -self._a[1] and l_bottom_obstacle[1] <= -self._a[1]:
                return [x_intersect_bottom]
            else:
                return []

        # Line parameters
        m = (l_right_obstacle[1] - l_left_obstacle[1]) / (l_right_obstacle[0] - l_left_obstacle[0])
        c = l_left_obstacle[1] - m * l_left_obstacle[0]

        # obstacle ellipse coefficients at intersection with line m*x+c
        kx2 = self._a[0]**2 * m**2 + self._a[1] ** 2
        kx = 2 * self._a[0]**2 * m * c
        k1 = self._a[0]**2 * (c**2 - self._a[1]**2)

        discriminant = self._a[0]**2 * m**2 + self._a[1] ** 2 - c**2
        if discriminant < 0:
            return []
        elif discriminant == 0:
            tmp_x = -kx / (2 * kx2)
            x_intersect_obstacle = np.array([tmp_x, m * tmp_x + c])
            return [self.transform(x_intersect_obstacle, Frame.OBSTACLE, output_frame, state)]
        else:
            tmp_x = -kx / (2 * kx2) - np.sqrt(kx**2 / (4 * kx2**2) - k1 / kx2)
            x_intersect_left_obstacle = np.array([tmp_x, m * tmp_x + c])
            tmp_x = -kx / (2 * kx2) + np.sqrt(kx**2 / (4 * kx2**2) - k1 / kx2)
            x_intersect_right_obstacle = np.array([tmp_x, m * tmp_x + c])
            x_intersect_left = self.transform(x_intersect_left_obstacle, Frame.OBSTACLE, output_frame, state)
            x_intersect_right = self.transform(x_intersect_right_obstacle, Frame.OBSTACLE, output_frame, state)

            if l_right_obstacle[0] < x_intersect_left_obstacle[0] or x_intersect_right_obstacle[0] < l_left_obstacle[0] or \
                    x_intersect_left_obstacle[0] < l_left_obstacle[0] < l_right_obstacle[0] < x_intersect_right_obstacle[0]:
                # print("No intersection")
                return []
            elif x_intersect_left_obstacle[0] < l_left_obstacle[0]:
                # print("Right")
                return [x_intersect_right]
            elif l_right_obstacle[0] < x_intersect_right_obstacle[0]:
                # print("Left")
                return [x_intersect_left]
            else:
                # print("Both")
                return [x_intersect_left, x_intersect_right]

    def tangent_points(self, x, input_frame=Frame.GLOBAL, output_frame=Frame.GLOBAL, state=None):
        # if self.distance_function(x, input_frame, state) <= 1:
        #     return []

        x_obstacle = self.transform(x, input_frame, Frame.OBSTACLE, state)
        vertices = np.asarray(self._polygon.exterior.coords)[:]
        tp = []
        for v in vertices:
            n = self.normal(v, Frame.OBSTACLE, Frame.OBSTACLE, x_is_boundary=True)
            v_ray = v-x_obstacle
            v_ray /= np.linalg.norm(v_ray)
            # print(n.dot(v_ray))
            if abs(n.dot(v_ray)) < 0.1:
                tp += [self.transform(v, Frame.OBSTACLE, output_frame, state)]

        return tp
