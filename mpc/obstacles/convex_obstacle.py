from abc import ABC, abstractmethod
from mpc.obstacles import StarshapedObstacle


class ConvexObstacle(StarshapedObstacle, ABC):
    
    def __init__(self, motion_model=None, xr=None, name=None):
        super(ConvexObstacle, self).__init__(motion_model=motion_model, xr=xr, name=name)
    
