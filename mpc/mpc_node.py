"""This module defines a ros2 Node that detects aruco markers and publishes the result."""

import math
from typing import List, Optional
from mpc.RobotModelData import RobotModelData


# ROS module
import rclpy
from rclpy.node import Node
from rclpy.executors import MultiThreadedExecutor

from ament_index_python.packages import get_package_share_directory

from std_msgs.msg import (
    String,
)
from geometry_msgs.msg import TwistStamped
import rclpy.time
from mpc.mpc import MPCController
from mpc.use_cases import get_case_data





class MPCNode(Node):
    """Class for detecting tags and publishing to a topic."""

    mpc: MPCController

    def __init__(self):
        super().__init__("mpc_node")  # set name when launching

        robots, obstacles, sim_steps, r_model = get_case_data(14)
        self.mpc = MPCController(r_model)
        self.mpc.robots = robots
        self.mpc.obstacles = obstacles

        self.mpc.tick()


        self.publisher_ = self.create_publisher(
            TwistStamped, '/teleop_1', 10
        )

        self.timer = self.create_timer(0.1, self.timer_callback)


    def timer_callback(self) -> None:
        """Publish a message."""

        self.mpc.tick()
        new_state = self.mpc.robots[0]['State'].copy()
        x = new_state[0]
        y = new_state[1]
        a = new_state[2]
        v = new_state[3]
        w = new_state[4]

        msg = TwistStamped()
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = 'world'


        v_x = v * math.cos(a)
        v_y = v * math.sin(a)
        msg.twist.linear.x = v_x
        msg.twist.linear.y = v_y
        msg.twist.angular.z = w


        print("----------------------")
        print(F"x: {x}")
        print(F"y: {y}")
        print(F"a: {a}")
        print(F"v: {v}")
        print(F"w: {w}")
        print(F"v_x: {v_x}")
        print(F"v_y: {v_y}")
        print("----------------------")
    
        self.publisher_.publish(msg)





def main(args=None) -> None:
    """Run the TagPublisher node."""
    rclpy.init(args=args)
    runner = MPCNode()
    rclpy.spin(runner)
    runner.destroy_node()
    rclpy.shutdown()
    # try:
    #     pubber = MPCNode()

    #     executor = MultiThreadedExecutor()
    #     executor.add_node(pubber)
    #     # add more nodes if necessary

    #     try:
    #         executor.spin()
    #     finally:
    #         executor.shutdown()
    #         pubber.destroy_node()
    # finally:
    #     rclpy.shutdown()


if __name__ == "__main__":
    main()
