import matplotlib.pyplot as plt
from abc import ABC, abstractmethod
import numpy as np


class MobileRobot(ABC):

    def __init__(self, nu, nz, width, name, u_min=None, u_max=None, z_min=None, z_max=None):
        self.nz = nz
        self.nu = nu
        self.width = width
        self.name = name
        def valid_u_bound(bound): return bound is not None and len(bound) == self.nu
        def valid_z_bound(bound): return bound is not None and len(bound) == self.nz
        self.u_min = u_min if valid_u_bound(u_min) else [-np.inf] * self.nu
        self.u_max = u_max if valid_u_bound(u_max) else [np.inf] * self.nu
        self.z_min = z_min if valid_z_bound(z_min) else [-np.inf] * self.nz
        self.z_max = z_max if valid_z_bound(z_max) else [np.inf] * self.nz

    @abstractmethod
    def f(self, z, u):
        pass

    @abstractmethod
    def vel_min(self):
        pass

    @abstractmethod
    def vel_max(self):
        pass

    def move(self, z, u, dt):
        u_sat = np.clip(u, self.u_min, self.u_max)
        z_next = z + np.array(self.f(z, u_sat)) * dt
        z_next = np.clip(z_next, self.z_min, self.z_max)
        return z_next, u_sat

    def init_plot(self, ax=None, color='b', alpha=0.7):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        if self.width == 0:
            handle = plt.plot(0, 0, 'o', color=color, alpha=alpha, label=self.name)
        else:
            handle = [plt.Circle((0, 0), self.width / 2, color=color, alpha=alpha, label=self.name)]
        return handle

    def update_plot(self, z, handles):
        if self.width == 0:
            handles[0].set_data(*z[:2])
        else:
            handles[0].set(center=z[:2])

    def draw(self, z, ax=None, handle=None, color='b'):
        if ax is None:
            _, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        if handle is not None:
            handle.remove()
        handle = plt.Circle((z[0], z[1]), self.width / 2, color=color, alpha=0.7, label=self.name)
        ax.add_artist(handle)
        return ax, handle
