"""
The :mod:`robot` module implements mobile robots for obstacle avoidance.
"""
from .mobile_robot import MobileRobot
from .unicycle import Unicycle
from .omnidirectional import Omnidirectional