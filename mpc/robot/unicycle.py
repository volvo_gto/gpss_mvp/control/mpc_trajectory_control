import numpy as np
from mpc.robot import MobileRobot


class Unicycle(MobileRobot):

    def __init__(self, width, vel_min=None, vel_max=None, name='robot'):
        super().__init__(nu=2, nz=3, width=width, name=name, u_min=vel_min, u_max=vel_max)

    def f(self, z, u):
        return [u[0] * np.cos(z[2]),
                u[0] * np.sin(z[2]),
                u[1]]

    def vel_min(self):
        return self.u_min

    def vel_max(self):
        return self.u_max

    def init_plot(self, ax=None, color='b', alpha=0.7):
        h = super(Unicycle, self).init_plot(ax=ax, color=color, alpha=alpha)
        h += [ax.quiver(0, 0, 1, 1, color=color)]
        return h

    def update_plot(self, z, handles):
        super(Unicycle, self).update_plot(z, handles)
        # if self.width == 0:
        #     handles[0].set_data(*z[:2])
        # else:
        #     handles[0].set(center=z[:2])
        handles[1].set_offsets(z[:2])
        handles[1].set_UVC(np.cos(z[2]), np.sin(z[2]))

    def draw(self, z, ax=None, handle=None, color='b'):
        if handle:
            for h in handle:
                h.remove()
        ax, h1 = super(Unicycle, self).draw(z, ax=ax, color=color)
        h2 = ax.quiver(z[0], z[1], np.cos(z[2]), np.sin(z[2]), color=color)
        handle = [h1, h2]
        return ax, handle
