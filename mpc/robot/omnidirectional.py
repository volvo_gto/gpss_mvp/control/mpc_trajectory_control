import numpy as np
from mpc.robot import MobileRobot


class Omnidirectional(MobileRobot):

    def __init__(self, width, vel_min=None, vel_max=None, name='robot'):
        super().__init__(nu=2, nz=2, width=width, name=name, u_min=vel_min, u_max=vel_max)

    def f(self, z, u):
        return [u[0],
                u[1]]

    def vel_min(self):
        return self.u_min

    def vel_max(self):
        return self.u_max
